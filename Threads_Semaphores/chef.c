/*Yusuf Can Kan */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include <pthread.h>
#include "chef.h"

/*Macros*/
#define ONE 1
#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define PLUSONE 1
#define TRUE 1
#define NUMBER_OF_CHILD 4
#define READ 0
#define WRITE 1
#define CONTAINER_SIZE 256



/*Main struct for all variables to pass from main thread to others.*/
struct chef_struct;

/*This function will invoke as printf if
    something unexpected happens.*/
void print_error (char *message){   
    write (STDERR_FILENO, message, strlen(message));
}

int print_screen (char *message){  
    char error_screen[CONTAINER_SIZE]; 
    if(write (STDOUT_FILENO, message, strlen(message))==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_wait operations");                
        print_error(error_screen);
        return RETURN_FAIL;
    }
    return RETURN_SUCCESS;
}

void io_files(int argc, char* argv[], char** file){
	int opt;
	char print_screen_string[CONTAINER_SIZE]; /*For printing string.*/
	while((opt = getopt(argc, argv, "i:")) !=-1 ){
		switch(opt){
			case 'i':
				*file=optarg;
				break;
			default:
                sprintf(print_screen_string,"Please provide proper input: ./161044007 -i filePath\n");
                print_screen(print_screen_string);
                exit(EXIT_FAILURE);
		}
	}
	if(argc != 3){
        sprintf(print_screen_string,"Please provide proper input: ./161044007 -i filePath\n");
        print_screen(print_screen_string);
        exit(EXIT_FAILURE);
    }
}

void *chef_fuct(void *arg){

    int i;
    int needed_ingredients[2];
    char *ingredient1, *ingredient2;
    int chef_id,
        flag=0; /*This flag will using for synchronizing chefs. When chef checks incoming
                ingredients, it he/she doesn't need it it will set this flag for blocking itself.*/
    char error_screen[CONTAINER_SIZE]; /*For printing error.*/
    char print_screen_string[CONTAINER_SIZE]; /*For printing string.*/
    struct chef_struct *chef_struct;
    chef_struct=(struct chef_struct *)(arg);

    srand(time(0)); 

    /*This two variable will help while print operations.*/
    ingredient1=(char*)malloc(10*sizeof(char));
    ingredient2=(char*)malloc(10*sizeof(char));

    /*Save with mutex*/
    if(sem_wait(chef_struct->chose_mutex)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_wait operations");                
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }

    /*This code blocks sets needed ingredient to check if wholesaler delivered it.
        which_meterial=0 => Milk - Flour
        which_meterial=1 => Milk - Walnuts
        which_meterial=2 => Milk - Sugar
        which_meterial=3 => Flour - Walnuts
        which_meterial=4 => Flour - Sugar
        which_meterial=5 => Walnuts - Sugar

        and

        Milk = 1
        Flour = 2
        Walnuts = 3
        Sugar = 4
    */
    if(chef_struct->which_material==0){
        needed_ingredients[0]= 1;
        needed_ingredients[1]= 2;
        sprintf(ingredient1,"milk");
        sprintf(ingredient2,"flour");
    }
    else if(chef_struct->which_material==1){
        needed_ingredients[0]= 1;
        needed_ingredients[1]= 3;
        sprintf(ingredient1,"milk");
        sprintf(ingredient2,"walnuts");
    }
    else if(chef_struct->which_material==2){
        needed_ingredients[0]= 1;
        needed_ingredients[1]= 4;
        sprintf(ingredient1,"milk");
        sprintf(ingredient2,"sugar");
    }
    else if(chef_struct->which_material==3){
        needed_ingredients[0]= 2;
        needed_ingredients[1]= 3;
        sprintf(ingredient1,"flour");
        sprintf(ingredient2,"walnuts");
    }
    else if(chef_struct->which_material==4){
        needed_ingredients[0]= 2;
        needed_ingredients[1]= 4;
        sprintf(ingredient1,"flour");
        sprintf(ingredient2,"sugar");
    }
    else if(chef_struct->which_material==5){
        needed_ingredients[0]= 3;
        needed_ingredients[1]= 4;
        sprintf(ingredient1,"walnuts");
        sprintf(ingredient2,"sugar");
    }

    (chef_struct->which_material)++;
    chef_id=(chef_struct->which_material);
        /*Save the print.*/
    if(sem_post(chef_struct->chose_mutex)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_wait operations");                
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }
    
    while(TRUE){

        if(flag==0){
            /*Save print with mutex*/
            if(sem_wait(chef_struct->print_mutex)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operations");                
                print_error(print_screen_string);
                free(ingredient1);
                free(ingredient2);
                exit(EXIT_FAILURE);
            }

            sprintf(print_screen_string,"chef%d is waiting for %s and %s\n",chef_id,ingredient1,ingredient2);
            if(print_screen(print_screen_string)==ERROR_CONTROL){
                free(ingredient1);
                free(ingredient2);
                exit(EXIT_FAILURE);
            }

                /*Save print with mutex*/
            if(sem_post(chef_struct->print_mutex)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_post operations");                
                print_error(print_screen_string);
                free(ingredient1);
                free(ingredient2);
                exit(EXIT_FAILURE);
            }
        }
        else{ /*If it doesn't need the ingredients which has came it will wait
                for other chefs check.*/
            flag=0;
            
            if(sem_post(chef_struct->lock_chefs_mutex)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operations");                
                print_error(print_screen_string);
                free(ingredient1);
                free(ingredient2);
                exit(EXIT_FAILURE);
            }

            if(sem_wait(chef_struct->other_chefs)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operations");                
                print_error(print_screen_string);
                free(ingredient1);
                free(ingredient2);
                exit(EXIT_FAILURE);
            }

            /*Save waiting_chefs variable.*/
            if(sem_wait(chef_struct->protect_waiting_chefs)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operations");                
                print_error(print_screen_string);
                exit(EXIT_FAILURE);
            }

            (chef_struct->waiting_chefs)--;
            if((chef_struct->waiting_chefs)==0){
                /*Let cooker chef continue delivering.*/
                if(sem_post(chef_struct->clear_wait_chefs)==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while sem_wait operations");                
                    print_error(print_screen_string);
                    exit(EXIT_FAILURE);
                }
            }

            if(sem_post(chef_struct->protect_waiting_chefs)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_post operations");                
                print_error(print_screen_string);
                exit(EXIT_FAILURE);
            }
        }
        /*This mutex makes wait all chefs until wholesaler delivers
            ingredient.*/  

        if(sem_wait(chef_struct->lock_chefs_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operations");                
            print_error(error_screen);
            free(ingredient1);
            free(ingredient2);
            exit(EXIT_FAILURE);
        }
        if((chef_struct->ingredients[0]) == -1 && (chef_struct->ingredients[1]) == -1){
            break;
        }

        /*If cgef needs both ingredients.*/
        if( ((chef_struct->ingredients[0]) == needed_ingredients[0] || 
              (chef_struct->ingredients[0]) == needed_ingredients[1] )
            &&
            ((chef_struct->ingredients[1]) == needed_ingredients[0] || 
              (chef_struct->ingredients[1]) == needed_ingredients[1] ) ){
            
            /*Take the ingredients.*/

            /*Save print with mutex*/
            if(sem_wait(chef_struct->print_mutex)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operations");                
                print_error(print_screen_string);
                free(ingredient1);
                free(ingredient2);
                exit(EXIT_FAILURE);
            }
            
            sprintf(print_screen_string,"chef%d has taken the %s\n",chef_id,ingredient1);
            if(print_screen(print_screen_string)==ERROR_CONTROL){
                free(ingredient1);
                free(ingredient2);
                exit(EXIT_FAILURE);
            }
            
            if(sem_post(chef_struct->print_mutex)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_post operations");                
                print_error(print_screen_string);
                free(ingredient1);
                free(ingredient2);
                exit(EXIT_FAILURE);
            }

            chef_struct->ingredients[0]=0;

            /*Save print with mutex*/
            if(sem_wait(chef_struct->print_mutex)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operations");                
                print_error(print_screen_string);
                free(ingredient1);
                free(ingredient2);
                exit(EXIT_FAILURE);
            }
            
            sprintf(print_screen_string,"chef%d has taken the %s\n",chef_id,ingredient2);
            if(print_screen(print_screen_string)==ERROR_CONTROL){
                free(ingredient1);
                free(ingredient2);
                exit(EXIT_FAILURE);
            }
            
            if(sem_post(chef_struct->print_mutex)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_post operations");                
                print_error(print_screen_string);
                free(ingredient1);
                free(ingredient2);
                exit(EXIT_FAILURE);
            }
            
            chef_struct->ingredients[1]=0;

            /*Simulation for dessert preparation.*/
            sleep(((rand()%5)+1));

            /*Save print with mutex*/
            if(sem_wait(chef_struct->print_mutex)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operations");                
                print_error(print_screen_string);
                free(ingredient1);
                free(ingredient2);
                exit(EXIT_FAILURE);
            }

            sprintf(print_screen_string,"chef%d has delivered the dessert to the wholesaler\n",chef_id);
            if(print_screen(print_screen_string)==ERROR_CONTROL){
                free(ingredient1);
                free(ingredient2);
                exit(EXIT_FAILURE);
            }

            if(sem_post(chef_struct->print_mutex)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_post operations");                
                print_error(print_screen_string);
                free(ingredient1);
                free(ingredient2);
                exit(EXIT_FAILURE);
            }

            if((chef_struct->waiting_chefs)!=0){
                /*Protect the waiting_chefs value so when other waiting chefs unlocked
                    can't spoil the value.*/
                if(sem_wait(chef_struct->protect_waiting_chefs)==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while sem_wait operations");                
                    print_error(print_screen_string);
                    exit(EXIT_FAILURE);
                }

                for(i=ZERO;i<(chef_struct->waiting_chefs);i++){
                    if(sem_post(chef_struct->other_chefs)==ERROR_CONTROL){
                        sprintf(error_screen,"\nError while sem_post operations");                
                        print_error(print_screen_string);
                        free(ingredient1);
                        free(ingredient2);
                        exit(EXIT_FAILURE);
                    }
                }

                if(sem_post(chef_struct->protect_waiting_chefs)==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while sem_post operations");                
                    print_error(print_screen_string);
                    exit(EXIT_FAILURE);
                }

                /*Lock process until all other chefs goes to starting position so noone waits
                    critical parts.*/
                if(sem_wait(chef_struct->clear_wait_chefs)==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while sem_wait operations");                
                    print_error(print_screen_string);
                    exit(EXIT_FAILURE);
                }

            }

            /*Let wholesaler continue its operaitons.*/
            if(sem_post(chef_struct->wait_dessert_mutex)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_post operations");                
                print_error(print_screen_string);
                exit(EXIT_FAILURE);
            }

            flag=0;
        }
        else if(chef_struct->ingredients[0]==-1 && chef_struct->ingredients[1]==-1){
            break;
        }
        else{ /*If chef doesn't need this ingredients lets other chefs take it.*/
                        /*Save print with mutex*/
            flag=1;
            (chef_struct->waiting_chefs)++;
        }
    }

    if(sem_post(chef_struct->lock_chefs_mutex)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_post operations");                
        print_error(print_screen_string);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }

    if(sem_wait(chef_struct->print_mutex)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_wait operations");                
        print_error(print_screen_string);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }   

    sprintf(print_screen_string,"chef%d finished its job. GOODBYE!!\n",chef_id);
    if(print_screen(print_screen_string)==ERROR_CONTROL){
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }

    if(sem_post(chef_struct->print_mutex)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_post operations");                
        print_error(print_screen_string);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }   
    

    free(ingredient1);
    free(ingredient2);
    return RETURN_SUCCESS;
}
