/*Yusuf Can Kan*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include <pthread.h>
#include "chef.h"

/*Macros*/
#define ONE 1
#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define PLUSONE 1
#define TRUE 1
#define NUMBER_OF_CHILD 4
#define READ 0
#define WRITE 1
#define CONTAINER_SIZE 256

int main(int argc,char *argv[]){
    char error_screen[CONTAINER_SIZE];
    char print_screen_string[CONTAINER_SIZE]; /*For printing string.*/
    int sh_ingredients; /*Shared memory variables.*/
    mode_t perms=(S_IRUSR | S_IWUSR);
    char buffer[3];
    pthread_t chef1,chef2,chef3,chef4,chef5,chef6;
    char *ingredient1, *ingredient2;
    struct chef_struct *chefs;
    chefs=(struct chef_struct *)malloc(1*sizeof(struct chef_struct)); /*Allocated memory from heap.*/
    int error=0;
    int fd;
    char *file;

    io_files(argc,argv,&file);
    
    fd=open(file,O_RDONLY);

    if(fd==ERROR_CONTROL){
        sprintf(error_screen,"\nerror while opening file.\n");                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    
    /*This two variable will help while print operations.*/
    ingredient1=(char*)malloc(10*sizeof(char));
    ingredient2=(char*)malloc(10*sizeof(char));

    chefs->which_material=0;
    chefs->ingredients[0]=0;
    chefs->ingredients[1]=0;

    chefs->chose_mutex=(sem_t*)malloc(ONE*sizeof(sem_t));
    chefs->lock_chefs_mutex=(sem_t*)malloc(ONE*sizeof(sem_t));
    chefs->wait_dessert_mutex=(sem_t*)malloc(ONE*sizeof(sem_t));
    chefs->print_mutex=(sem_t*)malloc(ONE*sizeof(sem_t));
    chefs->other_chefs=(sem_t*)malloc(ONE*sizeof(sem_t));
    chefs->clear_wait_chefs=(sem_t*)malloc(ONE*sizeof(sem_t));
    chefs->protect_waiting_chefs=(sem_t*)malloc(ONE*sizeof(sem_t));

    if(sem_init(chefs->chose_mutex,0,1)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }
    if(sem_init(chefs->lock_chefs_mutex,0,0)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }
    if(sem_init(chefs->wait_dessert_mutex,0,0)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }
    if(sem_init(chefs->print_mutex,0,1)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }
    if(sem_init(chefs->other_chefs,0,0)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }
    if(sem_init(chefs->clear_wait_chefs,0,0)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }
    if(sem_init(chefs->protect_waiting_chefs,0,1)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }

    if((error=pthread_create(&chef1,NULL,chef_fuct,chefs)) != 0){
        sprintf (error_screen,"\nThread error: %s \n",strerror (error));         
        print_error(print_screen_string);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }

    if((error=pthread_create(&chef2,NULL,chef_fuct,chefs)) != 0){
        sprintf (error_screen,"\nThread error: %s \n",strerror (error));         
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }

    if((error=pthread_create(&chef3,NULL,chef_fuct,chefs)) != 0){
        sprintf (error_screen,"\nThread error: %s \n",strerror (error));         
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }

    if((error=pthread_create(&chef4,NULL,chef_fuct,chefs)) != 0){
        sprintf (error_screen,"\nThread error: %s \n",strerror (error));         
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }

    if((error=pthread_create(&chef5,NULL,chef_fuct,chefs)) != 0){
        sprintf (error_screen,"\nThread error: %s \n",strerror (error));         
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }

    if((error=pthread_create(&chef6,NULL,chef_fuct,chefs)) != 0){
        sprintf (error_screen,"\nThread error: %s \n",strerror (error));         
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }

    while(read(fd,&buffer,3)!=0){
        if(buffer[0]=='M'){
            chefs->ingredients[0]=1;
            sprintf(ingredient1,"milk");
        }
        else if(buffer[0]=='F'){
            chefs->ingredients[0]=2;
            sprintf(ingredient1,"flour");
        } 
        else if(buffer[0]=='W'){
            chefs->ingredients[0]=3;
            sprintf(ingredient1,"walnuts");
        } 
        else if(buffer[0]=='S'){
            chefs->ingredients[0]=4;
            sprintf(ingredient1,"sugar");
        } 
        else break;
        if(buffer[1]=='M'){
            chefs->ingredients[1]=1;
            sprintf(ingredient2,"milk");
        } 
        else if(buffer[1]=='F'){
            chefs->ingredients[1]=2;
            sprintf(ingredient2,"flour");
        } 
        else if(buffer[1]=='W'){
            chefs->ingredients[1]=3;
            sprintf(ingredient2,"walnuts");
        } 
        else if(buffer[1]=='S'){
            chefs->ingredients[1]=4;
            sprintf(ingredient2,"sugar");
        } 
        else break;

        if(sem_wait(chefs->print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operations");                
            print_error(print_screen_string);
            free(ingredient1);
            free(ingredient2);
            exit(EXIT_FAILURE);
        }   

        sprintf(print_screen_string,"the wholesaler delivers %s and %s\n",ingredient1,ingredient2);
        if(print_screen(print_screen_string)==ERROR_CONTROL){
            free(ingredient1);
            free(ingredient2);
            exit(EXIT_FAILURE);
        }

        if(sem_post(chefs->print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operations");                
            print_error(print_screen_string);
            free(ingredient1);
            free(ingredient2);
            exit(EXIT_FAILURE);
        }   

        if(sem_post(chefs->lock_chefs_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operations");                
            print_error(print_screen_string);
            free(ingredient1);
            free(ingredient2);
            exit(EXIT_FAILURE);
        }   

        if(sem_wait(chefs->print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operations");                
            print_error(print_screen_string);
            free(ingredient1);
            free(ingredient2);
            exit(EXIT_FAILURE);
        }
        
        sprintf(print_screen_string,"the wholesaler is waiting for the dessert\n");
        if(print_screen(print_screen_string)==ERROR_CONTROL){
            free(ingredient1);
            free(ingredient2);
            exit(EXIT_FAILURE);
        }

        if(sem_post(chefs->print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operations");                
            print_error(print_screen_string);
            free(ingredient1);
            free(ingredient2);
            exit(EXIT_FAILURE);
        }   

        if(sem_wait(chefs->wait_dessert_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operations");                
            print_error(print_screen_string);
            free(ingredient1);
            free(ingredient2);
            exit(EXIT_FAILURE);
        }

        if(sem_wait(chefs->print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operations");                
            print_error(print_screen_string);
            free(ingredient1);
            free(ingredient2);
            exit(EXIT_FAILURE);
        }

        sprintf(print_screen_string,"the wholesaler has obtained the dessert and left to sell it\n");
        if(print_screen(print_screen_string)==ERROR_CONTROL){
            free(ingredient1);
            free(ingredient2);
            exit(EXIT_FAILURE);
        }

        if(sem_post(chefs->print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operations");                
            print_error(print_screen_string);
            free(ingredient1);
            free(ingredient2);
            exit(EXIT_FAILURE);
        }   
    }

    chefs->ingredients[0]=-1;
    chefs->ingredients[1]=-1;

    if(sem_post(chefs->lock_chefs_mutex)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_post operations");                
        print_error(print_screen_string);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }


    if(sem_wait(chefs->print_mutex)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_wait operations");                
        print_error(print_screen_string);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }

    sprintf(print_screen_string,"the wholesaler finished its job. GOODBYE!!\n");
    if(print_screen(print_screen_string)==ERROR_CONTROL){
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }

    if(sem_post(chefs->print_mutex)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_post operations");                
        print_error(print_screen_string);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }   


    if((error=pthread_join(chef1,NULL)) != 0){
        sprintf (error_screen,"\nThread join error: %s \n",strerror (error));         
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }
    if((error=pthread_join(chef2,NULL)) != 0){
        sprintf (error_screen,"\nThread join error: %s \n",strerror (error));         
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }
    if((error=pthread_join(chef3,NULL)) != 0){
        sprintf (error_screen,"\nThread join error: %s \n",strerror (error));         
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }
    if((error=pthread_join(chef4,NULL)) != 0){
        sprintf (error_screen,"\nThread join error: %s \n",strerror (error));         
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }
    if((error=pthread_join(chef5,NULL)) != 0){
        sprintf (error_screen,"\nThread join error: %s \n",strerror (error));         
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }
    if((error=pthread_join(chef6,NULL)) != 0){
        sprintf (error_screen,"\nThread join error: %s \n",strerror (error));         
        print_error(error_screen);
        free(ingredient1);
        free(ingredient2);
        exit(EXIT_FAILURE);
    }

    free(ingredient1);
    free(ingredient2);
    return 0;
}




