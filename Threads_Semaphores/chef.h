/*Yusuf Can Kan*/

#ifndef CHEF_H
#define CHEF_H


/*Main struct for all variables to pass from main thread to others.*/
struct chef_struct{
    int which_material; /*When all threads start for different
                            chefs, they will chose its needed igredients with
                            respect to this variable and after they choose they 
                            will increase 1 for other chef.*/
    
    int ingredients[2]; /*This variable will be used by wholesaler for
                            delivering the ingredients. 0 is (M Milk),
                            1 is (F Flour), 2 is (W Walnuts), 3 is (S sugar).*/
    
    sem_t *chose_mutex;       /*This is the mutex using while chefts chosing 
                                their needed ingredients.*/
    sem_t *lock_chefs_mutex;  /*This mutex locks all chefs until wholesaler
                                delivers its ingredients.*/

    sem_t *wait_dessert_mutex; /*This variable makes wholesaler to wait until 
                                 dessert will be ready. */

    sem_t *print_mutex;         /*It saves the print operations.*/

    sem_t *other_chefs;          /*This mutex helps for chefs controls if they need the delivered
                                    ingredients or not. If they not they suspend itselfs with using
                                    this semaphore.*/

    sem_t *clear_wait_chefs;    /*Before the delivering dessert the current cooker chef waits until all 
                                    chefs comes to initial position.*/

    sem_t *protect_waiting_chefs; /* This mutex. protects the waiting_chefs value.*/

    int waiting_chefs;           /*This variable will be using for synchronizing chefs.*/
};

/*Prints error*/
void print_error (char *message); 

/*Print screen*/
int print_screen (char *message);

/*For command line arguments.*/
void io_files(int argc, char* argv[], char** file);

/*Chef functions*/ 
void *chef_fuct(void *arg);


#endif
