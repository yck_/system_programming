/*Yusuf Can Kan*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include <pthread.h>
#include "florist.h"
#include "order_queue.h"


/*Macros*/
#define ONE 1
#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define PLUSONE 1
#define TRUE 1
#define NUMBER_OF_CHILD 4
#define READ 0
#define WRITE 1
#define CONTAINER_SIZE 512
#define LINE_SIZE 1024





/*This conditional variables will be using for synronization closing shops
    and last prints.*/
pthread_cond_t c_florist=PTHREAD_COND_INITIALIZER; /*Florists waits for main to finishes its 
                                                        print with this variable. */
pthread_cond_t c_main=PTHREAD_COND_INITIALIZER; /*Main waits all florist finishes their 
                                                    delivery with this variable.*/

int isOrdersDone=0; /*For controlling if main process is done or not.*/
int florist_lock=0; /*For handling CTRL+C*/


/*STRUCTS*/



/*Linked list struct for threads.*/
struct thread_linked{
    pthread_t *thread;
    struct thread_linked *next;
};

/*This struct will be used only when CTRL+C happened.*/
struct clean_backup{
    struct thread_linked *threads;
    struct florist_linked *florists;
    int fd;
};



/*GLOBAL VARIABLES*/


pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER; /*Mutex for synchronization of delivering.*/


int main_lock=0; /*For handling CTRL+C*/

struct clean_backup backup;


/*SIGINT Handler.*/
void sigint_handler(int signal_number);

/*Parses command line arg with using getopt.*/
void parse_arg(int argc, char* argv[],char** file);

/*This function reads file and creates florists.*/
int read_file(int *fd,struct florist_linked **head);

/*It reads the orders from file and and delivers
    to the florists. */
int read_orders(int *fd,struct florist_linked *head);

/*Free queue*/
void free_queue(struct order_Queue *queue);

/*Free florists*/
void free_florists(struct florist_linked *head);


int main(int argc, char*argv[]){

    int i,fd,error=0;
    char *file;
    char line[LINE_SIZE];
    int florist_count=0; /*Holds created florists count.*/
    struct thread_linked *threads=NULL;
    struct thread_linked *node_thread=NULL;

    char *return_values; /*This value stores the returns of florsits.*/

    /*Create florist linkedlist.*/
    struct florist_linked *head=NULL;
    struct florist_linked *node_florist=NULL;
    
    struct sigaction sigint_action;

    /*Parse command line arguments.*/
    parse_arg(argc,argv,&file);

    backup.florists=NULL;
    backup.threads=NULL;
    (backup.fd)=-1;
    threads=(struct thread_linked*)malloc(sizeof(struct thread_linked));
    threads->next=NULL;
    threads->thread=NULL;
    backup.threads=threads;


    /*Set SIGINT handler for CTRL+C.*/
    memset(&sigint_action,0,sizeof(sigint_action));
	sigint_action.sa_handler = &sigint_handler;
	if(sigaction(SIGINT,&sigint_action,NULL)==ERROR_CONTROL){
        sprintf(line,"\nError while sigaction operation.\n");                
        print_error(line);
        exit(EXIT_FAILURE);
    }



    species=(struct flover_species*)malloc(sizeof(struct flover_species));
    /*Create flover array size of 512. If program needs more space it will 
        malloc another 512.*/
    species->flower_species=(char**)malloc(CONTAINER_SIZE*sizeof(char*));
    species->capacity=CONTAINER_SIZE;
    species->size=0;

    fd=open(file,O_RDONLY);
	if(fd==ERROR_CONTROL){
        free(threads);
        sprintf(line,"\nFile couldn't open.\n");                
        print_error(line);
        free_florists(head);
        /*Free flower species.*/
        for(i=0;i<species->size;i++){
            free(species->flower_species[i]);
        }
        free(species->flower_species);
        free(species);
        exit(EXIT_FAILURE);
    }
    
    backup.fd=fd;
    sprintf(line,"Florist application initializing from file: %s\n",file);
    print_screen(line);
    
    /*Read florist and flower informations and set.*/
    if((florist_count=read_file(&fd,&head))==ERROR_CONTROL){

        sprintf(line,"\nSomething happened while reading file..\n");                
        print_error(line);

        /*Free flower species.*/
        for(i=0;i<species->size;i++){
            free(species->flower_species[i]);
        }
        free(species->flower_species);
        free(species);
        free_florists(head);
        if(close(fd)==ERROR_CONTROL){
            exit(EXIT_FAILURE);
        }
        exit(EXIT_FAILURE);
    }
    node_florist=head;
    node_thread=threads;

    /*Create threads.*/
    if(node_florist!=NULL){
        node_thread->thread=(pthread_t*)malloc(sizeof(pthread_t));
        error=pthread_create(node_thread->thread,NULL,florists,node_florist->florist);
        if(error!=0){
            sprintf (line, "Error while creating thread.\n");
            print_error(line);
            return RETURN_FAIL;
        }

        node_thread->next=(struct thread_linked*)malloc(sizeof(struct thread_linked));
        node_thread->next->next=NULL;
        node_thread->next->thread=NULL;
        node_florist=node_florist->next;
    }
    
    /*Create threads.*/
    while(node_florist!=NULL){  
        (node_thread->next->thread)=(pthread_t*)malloc(sizeof(pthread_t));
        error=pthread_create(node_thread->next->thread,NULL,florists,node_florist->florist);
        if(error!=0){
            sprintf (line, "Error while creating thread.\n");
            print_error(line);
            return RETURN_FAIL;
        }

        node_florist=node_florist->next;
        node_thread->next->next=(struct thread_linked*)malloc(sizeof(struct thread_linked));
        node_thread=node_thread->next;
        node_thread->next->next=NULL;
        node_thread->next->thread=NULL;
    }
    free(node_thread->next);
    node_thread->next=NULL;
    
    sprintf(line,"%d florists have been created.\n",florist_count);
    print_screen(line);

    if(read_orders(&fd,head)==ERROR_CONTROL){
        /*Free allocated memory.*/
        for(i=0;i<species->size;i++){
            free(species->flower_species[i]);
        }
        free(species->flower_species);
        free(species);
        free_florists(head);
        if(close(fd)==ERROR_CONTROL){
            exit(EXIT_FAILURE);
        }
        exit(EXIT_FAILURE);
    }

    error=pthread_mutex_lock(&m);
    main_lock++;
    if(error!=0){
        sprintf (line, "Failed while locking mutex.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }
    isOrdersDone++;
    node_florist=head;
    while(node_florist!=NULL){
        error=pthread_cond_signal(&(node_florist->florist->c));
        if(error!=0){
            sprintf (line, "Error while signalling conditional variable.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }
        node_florist=node_florist->next;
    }
    
    while((*(head->florist->finished_florist)) != florist_count){
        error=pthread_cond_wait(&c_main,&m);
        if(error!=0){
            sprintf (line, "Failed while waiting conditional variable.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }
    }

    sprintf(line,"All requests processed.\n");
    print_screen(line);

    error=pthread_cond_broadcast(&c_florist);
    if(error!=0){
        sprintf (line, "Failed while broadcasting cond. variable.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }

    error=pthread_cond_wait(&c_main,&m);
    if(error!=0){
        sprintf (line, "Failed while waiting cond variable..\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }
    

    node_florist=head;
    sprintf (line, "Sale statistic for today:\n");
    print_screen(line);
    sprintf (line, "-------------------------------------------------\n");
    print_screen(line);
    sprintf (line, "Florist         # of sales         Total time\n");
    print_screen(line);
    sprintf (line, "-------------------------------------------------\n");
    print_screen(line);
      
    error=pthread_mutex_unlock(&m);
    main_lock--;
    if(error!=0){
        sprintf (line, "Failed while unlocking mutex.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }
      
    node_thread=threads;
    while(node_thread!=NULL){
        error=pthread_join((*(node_thread->thread)),(void**)(&return_values));
        print_screen(return_values);
        if(error!=0){
            sprintf (line, "Error while joining thread.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }
        node_thread=node_thread->next;
    }

    node_thread=threads;
    while(node_thread!=NULL){
        if(node_thread->thread!=NULL)
            free(node_thread->thread);
        threads=node_thread;
        node_thread=node_thread->next;
        free(threads);
    }
    free(head->florist->finished_florist);
    free_florists(head);
    
    for(i=0;i<species->size;i++){
        free(species->flower_species[i]);
    }
    free(species->flower_species);
    free(species);
    if(close(fd)==ERROR_CONTROL){
        exit(EXIT_FAILURE);
    }

    return 0;
}

/*SIGINT Handler.*/
void sigint_handler(int signal_number){
    int err=0;
    char line[1024];
    int i;

    struct thread_linked *node_thread;
    struct florist_linked *node_florist;

    if(main_lock==1){
        err=pthread_mutex_unlock(&m);
        if(err!=0){
            sprintf(line,"%s",strerror(err));
            print_error(line);
            exit(EXIT_FAILURE);
        } 
    }

    node_thread=backup.threads;
    while(node_thread!=NULL){
        if((node_thread->thread)!=NULL)
            pthread_cancel((*(node_thread->thread)));
        node_thread=node_thread->next;
    }

    node_thread=backup.threads;
    while(node_thread!=NULL){
        if((node_thread->thread)!=NULL){
            pthread_join((*(node_thread->thread)),NULL);
        }  
        node_thread=node_thread->next;
    }
    node_thread=backup.threads;
    while(node_thread!=NULL){
        if(node_thread->thread!=NULL){
            free(node_thread->thread);
        }
            
        backup.threads=node_thread;
        node_thread=node_thread->next;
        free(backup.threads);
    }

    node_florist=backup.florists;
    if(node_florist!=NULL){
        free(node_florist->florist->finished_florist);
        free_florists(node_florist);
    }
    
    if(species!=NULL){
        for(i=0;i<species->size;i++){
            if(species->flower_species[i]!=NULL)
                free(species->flower_species[i]);
        }
        if(species->flower_species!=NULL)
            free(species->flower_species);
        free(species);
    }


    if((backup.fd)!=-1){
        if(close(backup.fd)==ERROR_CONTROL){
            exit(EXIT_FAILURE);
        }
    }

    sprintf(line,"\nProgram exited gracefully.\n");
    print_screen(line);
 
    exit(EXIT_SUCCESS);
}


/*Parses command line arg with using getopt.*/
void parse_arg(int argc, char* argv[],char** file){
	int opt;
    char line[256];
	while((opt = getopt(argc, argv, "i:")) !=-1 ){
		switch(opt){
			case 'i':
				*file=optarg;
				break;
			default:
                sprintf(line,"\nPlease provide proper command line arguement!\n ./161044007 -i inputFile\n");                
                print_error(line);
				exit(EXIT_FAILURE);
		}
	}
	if(argc!=3){
        sprintf(line,"\nPlease provide proper command line arguement!\n ./161044007 -i inputFile\n");                
        print_error(line);
		exit(EXIT_FAILURE);
    }
}


/*This function reads file and creates florists.*/
int read_file(int *fd,struct florist_linked **head){
    int readed_byte,str_size=0,counter=0; /*counter helps to identify words or 
                                            numbers in the line.*/
    int species_no=0;/*This variable holds species no.*/
    double store_double=0; /*This variable stores the speed value.*/
    char buffer[2];
    char line[LINE_SIZE];
    int counter_2=0;
    int error=0;
    struct florist *new_florist;

    /*This variable will be using for tracking of how mant 
        florist finishes its all delivery. Variable will be increase
        after main delivers all the orders.*/
    int *finished_florist=(int*)malloc(sizeof(int));
    /*Initialize variables.*/
    (*finished_florist)=0;

    /*Checks if file is empty or not.*/
    readed_byte=read((*fd),&buffer,1);
    if(readed_byte==ERROR_CONTROL){
		sprintf (line, "Failed to read file.\n");
        print_error(line);
		return RETURN_FAIL;
    }
    else if(readed_byte==0){
        sprintf (line, "File is empty please provide proper file.\n");
        print_error(line);
		return RETURN_FAIL;
    }
   
    buffer[1]='\0';
    strncpy(line,buffer,2);

    while(buffer[0]!='\n'){

        new_florist=create_new_florist(head);
        if(counter_2==0)
             backup.florists=(*head);
        new_florist->finished_florist=finished_florist; /*Assign every florist same address.*/
        error=pthread_cond_init(&(new_florist->c),NULL);
        if(error!=0){
            sprintf (line, "Error while initializing condition variable.\n");
            print_error(line);
            return RETURN_FAIL;
        }

        counter_2++;
        while(buffer[0]!='\n'){
            readed_byte=read((*fd),&buffer,1);
            if(readed_byte==ERROR_CONTROL){
                sprintf (line, "Failed to read file.\n");
                print_error(line);
                return RETURN_FAIL;
            }
            /*Sets the name.*/
            if(buffer[0]=='('){
                str_size=strlen(line)+1;
                new_florist->name=(char*)malloc(str_size*sizeof(char));
                strcpy(new_florist->name,line);
                line[0]='\0';
            }
            else if(buffer[0]==','){
                if(counter!=0){
                    species_no=add_species(line);
                    add_species_to_florist(new_florist->flover_size,&(new_florist->flovers),species_no);
                    new_florist->flover_size++;
                    line[0]='\0';
                }
                else{
                    store_double=atof(line);
                    counter++;
                    line[0]='\0';
                    new_florist->coor_x=store_double;
                }
            }
            else if(buffer[0]==';'){
                    store_double=atof(line);
                    line[0]='\0';
                    new_florist->coor_y=store_double;
            }
            else if(buffer[0]==')'){
                store_double=atof(line);
                line[0]='\0';
                new_florist->speed=store_double;
            }
            else if(buffer[0]=='\n'){
                species_no=add_species(line);
                add_species_to_florist(new_florist->flover_size,&(new_florist->flovers),species_no);
                new_florist->flover_size++;
                line[0]='\0';
            }
            else if(buffer[0]!=' ' && buffer[0]!=':'){
                strncat(line,buffer,1);
            }
        }
        counter=0; /*Reset counter for new line.*/
        readed_byte=read((*fd),&buffer,1);
        if(readed_byte==ERROR_CONTROL){
            sprintf (line, "Failed to read file.\n");
            print_error(line);
            return RETURN_FAIL;
        }
        if(buffer[0]=='\n') break;
        strcpy(line,buffer);
    }
    return counter_2;
}

/*It reads the orders from file and and delivers
    to the florists. */
int read_orders(int *fd,struct florist_linked *head){
    char buffer[2];
    char line[LINE_SIZE];
    int i,error=0;
    int clientNo=0,flover_id=0; /*Order informations.*/
    double coor_x=0,coor_y=0;
    struct florist *close_florist=NULL;

    buffer[1]='\0';
    line[0]='\0';
 
    while((error=read((*fd),&buffer,1))!=0){
        if(error==ERROR_CONTROL){
            sprintf (line, "Failed to read file.\n");
            print_error(line);
            return RETURN_FAIL;
        }
      
        if(buffer[0]=='('){
            int x=strlen(line);
            /*GIVE ORDER TO FLORIST.*/
            i=0;
            while(line[i+6]!='\0'){
               line[i]=line[i+6];
                i++;
            }
            line[i]='\0';
            clientNo=atoi(line);
            line[0]='\0';
        }
        else if(buffer[0]=='\n'){
            if(line[0]!='\0'){
              
                flover_id=return_flower_id(line);
                close_florist=find_closest_florist(coor_x,coor_y,flover_id,head);
                
                if(close_florist==NULL){
                    sprintf (line, "There is no florist sells that flower.\n");
                    print_screen(line);
                }
                else{
                    error=pthread_mutex_lock(&m);
                    main_lock++;
                    if(error!=0){
                        sprintf (line, "Failed while locking mutex.\n");
                        print_error(line);
                        return RETURN_FAIL;
                    }
                    
                    enQueue(close_florist->order_q,clientNo,coor_x,coor_y,flover_id);
                    
                    error=pthread_cond_signal(&(close_florist->c));
                    if(error!=0){
                        sprintf (line, "Error while signalling conditional variable.\n");
                        print_error(line);
                        return RETURN_FAIL;
                    }
                    error=pthread_mutex_unlock(&m);
                    main_lock--;
                    if(error!=0){
                        sprintf (line, "Failed while unlocking mutex.\n");
                        print_error(line);
                        return RETURN_FAIL;
                    }  
                }   
            }
            line[0]='\0';
        }
        else if(buffer[0]==','){
            coor_x=atof(line);
            line[0]='\0';
        }
        else if(buffer[0]==')'){
            coor_y=atof(line);
            line[0]='\0';
        }
        else if(buffer[0]!=' ' && buffer[0]!=':'){
            strcat(line,buffer);
        }     
    }

    /*If there is no '\n' character end of the file,
        this code segment will work.*/
    if(line[0]!='\0'){
        flover_id=return_flower_id(line);
        close_florist=find_closest_florist(coor_x,coor_y,flover_id,head);
        if(close_florist==NULL){
            sprintf (line, "There is no florist sells that flower.\n");
            print_screen(line);
        }
        else{          
            error=pthread_mutex_lock(&m);
            main_lock++;
            if(error!=0){
                sprintf (line, "Failed while locking mutex.\n");
                print_error(line);
                return RETURN_FAIL;
            }
            enQueue(close_florist->order_q,clientNo,coor_x,coor_y,flover_id);
            error=pthread_cond_signal(&(close_florist->c));
            if(error!=0){
                sprintf (line, "Error while signalling conditional variable..\n");
                print_error(line);
                return RETURN_FAIL;
            }
            error=pthread_mutex_unlock(&m);
            main_lock--;
            if(error!=0){
                sprintf (line, "Failed while unlocking mutex.\n");
                print_error(line);
                return RETURN_FAIL;
            }  
        } 
    }
    return RETURN_SUCCESS;
}

/*Free queue*/
void free_queue(struct order_Queue *queue){
    struct orders *temp;
    while(queue->front != NULL){
        temp = queue->front;
        queue->front=queue->front->next;
        free(temp);
    }
    free(queue);
}

/*Free florists*/
void free_florists(struct florist_linked *head){
    struct florist_linked *temp;
    int error=0;
    char line[62];
    while(head!=NULL){
        temp=head;
        head=head->next;
        if(temp->florist->order_q != NULL)
            free_queue(temp->florist->order_q);
        if(temp->florist->name!=NULL)
            free(temp->florist->name);
        if(temp->florist->flovers!=NULL)
            free(temp->florist->flovers);
        if(temp->florist->return_value!=NULL)
            free(temp->florist->return_value);
        error=pthread_cond_broadcast(&(temp->florist->c));
        if(error!=0){
            sprintf (line, "Failed while broadcasting.\n");
            print_error(line);
		    exit(EXIT_FAILURE);
        }
        error=pthread_cond_destroy(&(temp->florist->c));
        if(error!=0){
            sprintf (line,"Failed while destroying cond. variable.\n");
            print_error(line);
		    exit(EXIT_FAILURE);
        }
        free(temp->florist);
        free(temp);
    }
}


