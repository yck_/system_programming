/*Yusuf Can Kan*/

#ifndef FLORIST_H
#define FLORIST_H

/*This conditional variables will be using for synronization closing shops
    and last prints.*/
extern pthread_cond_t c_florist; /*Florists waits for main to finishes its 
                                                        print with this variable. */
extern pthread_cond_t c_main; /*Main waits all florist finishes their 
                                                    delivery with this variable.*/

extern int isOrdersDone; /*For controlling if main process is done or not.*/
extern int florist_lock; /*For handling CTRL+C*/
extern pthread_mutex_t m;

extern struct flover_species *species;



/*This structs stores different information 
    for every florist.*/
struct florist{
    char *name;
    double coor_x;
    double coor_y;
    double speed;
    int *flovers;
    int flover_size;
    struct order_Queue *order_q; /*This queue holds the orders. 
                                    Florist will dequeue orders, main process enqueue.*/

    pthread_cond_t c; /*Conditional variable for synchronization.*/

    int *finished_florist; /*This variable will be using for tracking of how many 
                            florist finishes its all delivery. Variable will be increase
                            after main process delivers all the orders.*/
    char *return_value; /*Stores return value.*/
};

/*This struct holds a string for every kind of flower species.
    In this way we give every species to id number(index),
    and we can use this index.*/
struct flover_species{
    char **flower_species;
    int size; /* Current size of string array.*/
    int capacity; /*Capacity of string array.*/
};


/*Linked list struct for florist.*/
struct florist_linked{
    struct florist *florist;
    struct florist_linked *next;
};


/*When program reads species of flower if there is no id added that
    species, this function gives new species.*/
int add_species(char *flower);

/*This function adds new node to florist linkled list and 
    returns the unsetted florist struct.*/
struct florist* create_new_florist(struct florist_linked **head);

/*This function finds closest florist with using  chebshevand distance returns it.*/
struct florist* find_closest_florist(double coor_x,double coor_y,int flower_id,struct florist_linked *head);


/*Florist function for every thread.*/
void *florists(void *arg);

/*This function gives readed species to 
    florist.*/
void add_species_to_florist(int flower_size,int **flowers,int new_flower);

/*Check if flower species is in string array,
    if it is returns index number, if not
    returns -1.*/
int return_flower_id(char *flower);

/*Returns 1 if given character is vovel.*/
int vowel_check(char c);

/*This functions using instead of printf. */
void print_screen(char *message);


/*Calculate chebshev distance.*/
double calculate_chebyshev(double x1,double y1,double x2,double y2);


/*This function using as printf if
    something unexpected happens.*/
void print_error(char *message);

void cleanupHandler(void *arg);


#endif
