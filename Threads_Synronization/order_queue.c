/*Yusuf Can Kan*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include <pthread.h>
#include "order_queue.h"

/*Macros*/
#define ONE 1
#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define PLUSONE 1
#define TRUE 1
#define NUMBER_OF_CHILD 4
#define READ 0
#define WRITE 1
#define CONTAINER_SIZE 512
#define LINE_SIZE 1024


/*Holds order information for queue.*/
struct orders;

/*Queue for orders.*/
struct order_Queue;

/*Create queue for orders*/
struct order_Queue* createQueue(){
    struct order_Queue* q = (struct order_Queue*)malloc(sizeof(struct order_Queue)); 
    q->front = q->rear = NULL; 
    return q; 
}

/*Adds element to rear.*/
void enQueue(struct order_Queue *q,int c_no,double x,double y,int f_no){
    /*Create new node.*/
    struct orders *temp = (struct orders*)malloc(sizeof(struct orders));
    temp->client_no=c_no;
    temp->coor_x=x;
    temp->coor_y=y;
    temp->flover_no=f_no;
    temp->next=NULL;

    /*If queue is empty.*/
    if (q->rear==NULL){
        q->front = q->rear = temp;
        return;
    }

    q->rear->next=temp;
    q->rear=temp;
}

/*Removes element in the front.*/
int deQueue(struct order_Queue *q){
    /*If queue is empty return fail.*/
    if(q->front == NULL)
        return RETURN_FAIL;
    
    struct orders *temp = q->front;
    q->front = q->front->next;
    if(q->front == NULL){
        q->rear=NULL;
    }
    free(temp);
    return RETURN_SUCCESS;
}












