/*Yusuf Can Kan*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include <pthread.h>
#include "florist.h"
#include "order_queue.h"

/*Macros*/
#define ONE 1
#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define PLUSONE 1
#define TRUE 1
#define NUMBER_OF_CHILD 4
#define READ 0
#define WRITE 1
#define CONTAINER_SIZE 512
#define LINE_SIZE 1024



/*This structs stores different information 
    for every florist.*/
struct florist;

/*This struct holds a string for every kind of flower species.
    In this way we give every species to id number(index),
    and we can use this index.*/
struct flover_species;

/*Linked list struct for florist.*/
struct florist_linked;


struct flover_species *species=NULL; /*Initializing this struct global, 
                                    allows other threads reach this easily.*/



/*When program reads species of flower if there is no id added that
    species, this function gives new species.*/
int add_species(char *flower){
    int i,f_id; /*Flower id*/

    f_id=return_flower_id(flower); /*Control if flower exist in list.*/
    if(f_id != RETURN_FAIL) return f_id;

    /*If there is no place to put new species, make double capacity.*/
    if((species->size)==((species->capacity))){
        char **temp=(char**)malloc(2*(species->capacity)*sizeof(char*));
        for(i=0;i<(species->size);i++){
            temp[i]=(char*)malloc(strlen(species->flower_species[i])*sizeof(char));
            strcpy(temp[i],(species->flower_species[i]));
        }
        for(i=0;i<(species->size);i++){
            free(species->flower_species[i]);
        }
        free(species->flower_species);
        species->flower_species=temp;
        species->capacity*=2;
    }
    species->flower_species[species->size]=(char*)malloc((strlen(flower)+1)*sizeof(char));
    strcpy(species->flower_species[species->size],flower);
    (species->size)++;

    return (species->size)-1;
}


/*This function adds new node to florist linkled list and 
    returns the unsetted florist struct.*/
struct florist* create_new_florist(struct florist_linked **head){

    struct florist_linked *node;
    node=(*head);
   if(node != NULL){
        while(node->next!=NULL){
            node=node->next;
        }

        node->next=(struct florist_linked *)malloc(sizeof(struct florist_linked));
        node=node->next;

    }
    else{
        node=(struct florist_linked *)malloc(sizeof(struct florist_linked));
        (*head)=node;
    }

    node->next=NULL;
    node->florist=(struct florist*)malloc(sizeof(struct florist));

    node->florist->name=NULL;
    node->florist->order_q=createQueue();
    node->florist->flovers=NULL;
    node->florist->flover_size=0;
    node->florist->finished_florist=NULL;
    node->florist->return_value=NULL;
    return node->florist;
}

/*This function finds closest florist with using  chebshevand distance returns it.*/
struct florist* find_closest_florist(double coor_x,double coor_y,int flower_id,struct florist_linked *head){
    struct florist_linked *node;
    struct florist *temp=NULL;
    double chebyshev_distance=-1;
    int i;
    double distance=0;
    
    node=head;

    if (node==NULL) return NULL; /*If there is no florist.*/
    else if(node->next==NULL){ /*If there is only 1 element in struct.*/
        for(i=0;i<(node->florist->flover_size);i++){
            if((node->florist->flovers[i])==flower_id) return node->florist;
        }
        return NULL;
    }

    while(node!=NULL){
       for(i=0;i<(node->florist->flover_size);i++){
            if((node->florist->flovers[i])==flower_id){
                distance=calculate_chebyshev(coor_x,coor_y,(node->florist->coor_x),(node->florist->coor_y));
                if(distance < chebyshev_distance || chebyshev_distance==-1){
                    chebyshev_distance=distance;
                    temp=node->florist;
                }
            } 
        }
        node=node->next;
    }
    return temp;
}

/*Florist function for every thread.*/
void *florists(void *arg){
    struct florist *f;
    int error;
    int t_preparetion=0;
    int t_delivery=0;
    char line[LINE_SIZE];
    int return_value[2];/*index 0 for counter of total element, 
                             1 for total time.*/
    return_value[0]=0;
    return_value[1]=0;
    char *return_informations=NULL;
    char vowel=' ';
    
    int *isUnlock=(int*)malloc(sizeof(int));
    (*isUnlock)=0;

    pthread_cleanup_push(cleanupHandler, isUnlock);

    srand(time(0));
    f=(struct florist*)(arg);

    error=pthread_mutex_lock(&m);
    florist_lock++;
    if(error!=0){
        sprintf (line, "Failed while locking mutex.\n");
        print_error(line);
		pthread_exit(NULL);
    }
    while(TRUE){
        if(f->order_q->front!=NULL){ 
            /*While processing current */
            error=pthread_mutex_unlock(&m);
            florist_lock--;
            (*isUnlock)++;
            if(error!=0){
                sprintf (line, "Failed while unlocking mutex.\n");
                print_error(line);
                pthread_exit(NULL);
            }

            t_preparetion=((rand()%250)+1);
            t_delivery=sqrt(pow(((f->coor_x)-(f->order_q->front->coor_x)),2)+
                            pow(((f->coor_y)-(f->order_q->front->coor_y)),2))/(f->speed);
            
            usleep((1000*(t_delivery+t_preparetion)));
            return_value[1]+=(t_delivery+t_preparetion);

            vowel=(species->flower_species[f->order_q->front->flover_no][0]);
            
            if(vowel_check(vowel) == 1){
                sprintf(line,"Florist %s has delivered an %s to client%d in %dms\n",f->name,
                    species->flower_species[f->order_q->front->flover_no],
                    f->order_q->front->client_no,t_preparetion+t_delivery);
            }
            else{
                sprintf(line,"Florist %s has delivered a %s to client%d in %dms\n",f->name,
                    species->flower_species[f->order_q->front->flover_no],
                    f->order_q->front->client_no,t_preparetion+t_delivery);
            }
         
            print_screen(line);
            return_value[0]++;   
            error=deQueue(f->order_q);
            if(error==ERROR_CONTROL){
                sprintf (line,"Error while dequeue queue.\n");
                print_error(line);
                pthread_exit(NULL);
            } 

            error=pthread_mutex_lock(&m);
            florist_lock++;
            (*isUnlock)--;
            if(error!=0){
                sprintf (line, "Failed while locking mutex.\n");
                print_error(line);
                pthread_exit(NULL);
            }
        }
        else{
            if(isOrdersDone==1) break; 
            error=pthread_cond_wait(&(f->c),&(m));
            if(error!=0){
                sprintf(line,"Error while waiting cond variable.\n");
                print_error(line);
                pthread_exit(NULL);
            }
            if(f->order_q->front==NULL) break;
        }
    }

    error=pthread_cond_signal(&c_main);
    if(error!=0){
        sprintf(line,"Error while signalling cond. variable.\n");
        print_error(line);
        pthread_exit(NULL);
    }

    (*(f->finished_florist))++;

    error=pthread_cond_wait(&c_florist,&m);
    if(error!=0){
        sprintf(line,"Error while waiting cond. variable.\n");
        print_error(line);
        pthread_exit(NULL);
    }

    sprintf(line,"%s closing shop.\n",f->name);
    print_screen(line);

    sprintf(line,"%s              %d          %dms\n",f->name,return_value[0],return_value[1]);
    return_informations=(char*)malloc((strlen(line)+1)*sizeof(char));
    strcpy(return_informations,line);
    f->return_value=return_informations; /*Stores adress if sudden CTRL+C comes.*/

    (*(f->finished_florist))--;
    if((*(f->finished_florist))==0){
        error=pthread_cond_signal(&c_main);
    
        if(error!=0){
            sprintf(line,"Error while signalling cond variable.\n");
            print_error(line);
            pthread_exit(NULL);
        }
    }

    error=pthread_mutex_unlock(&m);
    florist_lock--;
    if(error!=0){
        sprintf(line,"Error while unlocking mutex.\n");
        print_error(line);
        pthread_exit(NULL);
    }
    pthread_cleanup_pop(0);

    free(isUnlock);
    return return_informations;
}

/*This function gives readed species to 
    florist.*/
void add_species_to_florist(int flower_size,int **flowers,int new_flower){
      int size=0,i,*temp;
    if((*flowers)==NULL){
        (*flowers)=(int*)malloc(1*sizeof(int));
        (*flowers)[0]=new_flower;
    }
    else{
        temp=(int*)malloc((flower_size+1)*sizeof(int));
        for(i=0;i<flower_size;i++){
            temp[i]=(*flowers)[i];
        }
        temp[flower_size]=new_flower;
        free(*flowers);
        (*flowers)=temp;
    }
}

/*Check if flower species is in string array,
    if it is returns index number, if not
    returns -1.*/
int return_flower_id(char *flower){
    int i;
    for(i=0;i<species->size;i++){
        if(strcmp(flower,species->flower_species[i])==ZERO){
            return i;
        }
    }
    return RETURN_FAIL;
}


/*Returns 1 if given character is vowel.*/
int vowel_check(char c){

    if( (c=='a') || (c=='e') || (c=='i') || (c=='o') || (c=='u') ||
        (c=='A') || (c=='E') || (c=='I') || (c=='O') || (c=='U') ) return 1;
    
    return 0;
}

/*This function using instead of printf.*/
void print_screen(char *message){
    int error=0;
    char error_line[64];
    
    error=write (STDOUT_FILENO, message, strlen(message));
    if(error==ERROR_CONTROL){
        sprintf(error_line,"Error while write operation:%s\n",strerror(error));
        print_error(error_line);
        exit(EXIT_FAILURE);
    }
}

/*Calculate chebshev distance.*/
double calculate_chebyshev(double x1,double y1,double x2,double y2){

    double x=0,y=0;

    x=x1-x2;
    y=y1-y2;

    if(x<0) x*=-1;
    if(y<0) y*=-1;

    if(x>y) return x;
    else return y;
}


/*This function using as printf if
    something unexpected happens.*/
void print_error(char *message){   
    write (STDERR_FILENO, message, strlen(message));
}


/*Clean handler for florists*/
void cleanupHandler(void *arg){
    int error=0;
    char line[40];
    int *isUnlock;
    isUnlock=(int*)arg;

    if(florist_lock!=0 && (*isUnlock)!=1){
        florist_lock--;
        error=pthread_mutex_unlock(&m);
        if(error!=0){
            sprintf (line, "Failed while unlocking mutex.\n");
            print_error(line);
		    exit(EXIT_FAILURE);
        }
    }
    free(isUnlock);
}




