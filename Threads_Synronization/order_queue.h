#ifndef ORDER_QUEUE_H
#define ORDER_QUEUE_H


/*Holds order information for queue.*/
struct orders{
    int client_no;
    double coor_x;
    double coor_y;
    int flover_no;
    struct orders *next;
};

/*Queue for orders.*/
struct order_Queue{
    struct orders *front;
    struct orders *rear;
};


/*Create queue for orders*/
struct order_Queue* createQueue();

/*Adds element to rear.*/
void enQueue(struct order_Queue *q,int c_no,double x,double y,int f_no);

/*Removes element in the front.*/
int deQueue(struct order_Queue *q);


#endif
