#ifndef PROGRAM1_H
#define PROGRAM1_H


/*Takes the input bytes and b0,b1 for line equation (b1x+b0) and fills 
	the line string with this for filling the temporary file.*/
void fill_line(const unsigned char *bytes,char **line,double b0,double b1);

/*Calculates the least square method, finds ax+b equation
	and fills the line string with following  form
 	(x_1, y_1),....,(x_10, y_10), b0x+b1  */
void least_squares_method(unsigned char *bytes,char **line);

/*Writes string to the file with using given fd.*/
void writefile(int fd,char *line);


#endif
