/*Yusuf Can Kan*/

/*Program 2 functions.*/

#ifndef PROGRAM2_H
#define PROGRAM2_H


/*This function reads the line from give fd and 
	stores the coordinates in coordinates array,
	b0 index in b0, b1 index in b1.
	It is using in program2 function.*/
int readline(int fd,int *coordinates,double *b0,double *b1,char *line);

/*It removes the first.
	This function is using while removing 
	the line in the temporary file.
	
	If file is empty it return -1 , 0 otherwise.*/
void remove_line(int fd,char *tempFile);

/*MAE*/
double meanAbsoluteError(int coordinates[],double b0,double b1);

/*MSE*/
double meanSquaredError(int coordinates[],double b0,double b1);

/*RMSE*/
double rootMeanSquaredError(int coordinates[],double b0,double b1);

/*It takes the errors array and adds new 
	error values end of the array.*/
void add_errors(double ***errors,int size,double x1,double x2, double x3);


/*It takes the errors array, calculates 
	standard and mean deviation for each error
	and prints it.*/
void print_deviations(double **errors,int size);


#endif
