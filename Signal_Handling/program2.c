/*Yusuf Can Kan*/

/*Program 2 functions.*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#define COORDINATES_SIZE 20 /*Every single x,y coordinates size.*/
#define POINTS_SIZE 10.0 /*(x,y) points size.*/
#define LINES_SIZE 255
#define SINGLE_COORDINATE_LENGTH 10
#define ONE 1
#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define PLUSONE 1
#define TRUE 1


/*This function reads the line from give fd and 
	stores the coordinates in coordinates array,
	b0 index in b0, b1 index in b1.
	It is using in program2 function.*/
int readline(int fd,int *coordinates,double *b0,double *b1,char *line){
	int coor_index=ZERO, /*Index counter for coordinates*/
		line_index=ZERO, /*Index counter for lines.*/
		sing_coor_index=ZERO, /*Index for single_coordinate string.*/
		error=ZERO;	/*Error control.*/
	char buffer=0;	/*Stores evert character one by one.*/
	char *single_coordinate; /*Stores numbers inside the line for converting int.*/
	single_coordinate=(char*)malloc(SINGLE_COORDINATE_LENGTH*sizeof(char));

	if(lseek(fd,0,SEEK_SET) == RETURN_FAIL){
		free(single_coordinate);
		fprintf (stderr, "Failded to moving cursor inside file.");	
		exit(EXIT_FAILURE);
	}
	while(coor_index!=20){
		error=read(fd,&buffer,1); /*Reads file 1 by 1.*/
		if(error == ERROR_CONTROL){
			free(single_coordinate);
			fprintf (stderr, "\nFailed to read file.\n");
			exit(EXIT_FAILURE);
		}
		/*If the file is empty returns fail.*/
		if(error==0){
			free(single_coordinate);
			return RETURN_FAIL;
		}

		line[line_index++]=buffer; /*Stores the line for writing output file.*/
		if(buffer==',' && coor_index%2==0){
			single_coordinate[sing_coor_index]='\0';
			coordinates[coor_index++]=atoi(single_coordinate);
			sing_coor_index=ZERO;
		}
		else if(buffer==')'){
			single_coordinate[sing_coor_index]='\0';
			coordinates[coor_index++]=atoi(single_coordinate);
			error=read(fd,&buffer,ONE); /*Reads the comma between coordinates (a b) , (c d)*/
			line[line_index++]=buffer;
			if(error == ERROR_CONTROL){
				free(single_coordinate);
				fprintf (stderr, "\nFailed to read file.\n");
				exit(EXIT_FAILURE);
			}
			sing_coor_index=ZERO;
		}
		else if(buffer=='(' || buffer==',' || buffer==' '){
			continue;
		}
		else if(buffer>='0' && buffer <='9')
			single_coordinate[sing_coor_index++]=buffer;
	}
	/*This part takes b0 and b1 for b1x+b0*/
	while(buffer!='\n'){
		error=read(fd,&buffer,1);
		if(error == ERROR_CONTROL){
			free(single_coordinate);
			fprintf (stderr, "\nFailed to read file.\n");
			exit(EXIT_FAILURE);
		}
		line[line_index++]=buffer;
		if(buffer>='0' && buffer <='9'||buffer=='.'||buffer=='-')
			single_coordinate[sing_coor_index++]=buffer;
		else if(buffer=='x'){
			single_coordinate[sing_coor_index]='\0';
			*b1=atof(single_coordinate);
			sing_coor_index=ZERO;
		}
		else if(buffer=='\n'){
			single_coordinate[sing_coor_index]='\0';
			*b0=atof(single_coordinate);
			sing_coor_index=ZERO;
		}
	}
	line[line_index]='\0';
	free(single_coordinate);
	return RETURN_SUCCESS;
}

/*It removes the first.
	This function is using while removing 
	the line in the temporary file.
	
	If file is empty it return -1 , 0 otherwise.*/
void remove_line(int fd,char *tempFile){
	int counter=ZERO,
		ch_del_count=ZERO, /*Deleted character counter.*/
		error=0,
		isfileempty=ZERO;
	char buffer=0;

	/*It stores the file size.*/
	struct stat st;
	stat(tempFile, &st);
	int file_size = st.st_size;

	if(lseek(fd,0,SEEK_SET) == RETURN_FAIL){
		fprintf (stderr,"\nFailded to moving cursor inside file.\n");	
		exit(EXIT_FAILURE);
	}

	while(buffer!='\n'){
		counter++;
		error=read(fd,&buffer,1);
		if(error==ERROR_CONTROL){
			fprintf (stderr,"\nFailded to read input file!\n");	
			exit(EXIT_FAILURE);
		}
		if(error==0){
			isfileempty++;
			break;
		} 
	} 
	if(isfileempty==0){
		error=read(fd,&buffer,1);
		if(error==ERROR_CONTROL){
			fprintf (stderr,"\nFailded to read input file!\n");	
			exit(EXIT_FAILURE);
		}
		while(error!=0){
			lseek(fd,-counter-1,SEEK_CUR);
			write(fd,&buffer,1);
			lseek(fd,counter,SEEK_CUR);
			error=read(fd,&buffer,1);
			if(error==ERROR_CONTROL){
				fprintf (stderr,"\nFailded to read input file!\n");	
				exit(EXIT_FAILURE);
			}
		}
		truncate(tempFile,file_size-counter);
	}
}

/*MAE*/
double meanAbsoluteError(int coordinates[],double b0,double b1){
	int i;
	double result=ZERO;

	for(i=0;i<COORDINATES_SIZE;i+=2){
		result += abs((double)coordinates[i+1] - ( (b1*((double)coordinates[i])) + b0));
	}
	return (result/POINTS_SIZE);
}

/*MSE*/
double meanSquaredError(int coordinates[],double b0,double b1){
	int i;
	double error=ZERO,result=ZERO;
	for(i=0;i<COORDINATES_SIZE;i+=2){
		error = ( (b1*((double)coordinates[i])) + b0) - ((double)(coordinates[i+1]));
		result+=error*error;
	}
	return (result/POINTS_SIZE);
}

/*RMSE*/
double rootMeanSquaredError(int coordinates[],double b0,double b1){
	return sqrt(meanSquaredError(coordinates,b0,b1));
}

/*It takes the errors array and adds new 
	error values end of the array.*/
void add_errors(double ***errors,int size,double x1,double x2, double x3){
	double **errors_new;
	int i=0;
	errors_new=(double**)malloc((size+1)*sizeof(double*));
	for(i=0;i<size+1;i++){
		errors_new[i]=(double*)malloc(3*sizeof(double));
		if(i!=size){
			errors_new[i][0]=(*errors)[i][0];
			errors_new[i][1]=(*errors)[i][1];
			errors_new[i][2]=(*errors)[i][2];
		}
	}
	errors_new[size][0]=x1;
	errors_new[size][1]=x2;
	errors_new[size][2]=x3;
	for(i=0;i<size;i++){
		free((*errors)[i]);
	}
	free(*errors);
	*errors=errors_new;
}

/*It takes the errors array, calculates 
	standard and mean deviation for each error
	and prints it.*/
void print_deviations(double **errors,int size){

	int i;
	double average_MAE=ZERO, average_MSE=ZERO,average_RMSE=ZERO,
		mean_MAE=ZERO,mean_MSE=ZERO,mean_RMSE=ZERO,
		standard_MAE=ZERO,standard_MSE=ZERO,standard_RMSE=ZERO;

	for(i=0;i<size;i++){
		average_MAE+=errors[i][0];
		average_MSE+=errors[i][1];
		average_RMSE+=errors[i][2];
	}

	average_MAE/=((double)size);
	average_MSE/=((double)size);
	average_RMSE/=((double)size);

	for(int i=0;i<size;i++){
		mean_MAE+=abs(average_MAE-errors[i][0]);
		mean_MSE+=abs(average_MSE-errors[i][1]);
		mean_RMSE+=abs(average_RMSE-errors[i][2]);

		standard_MAE+= pow( (average_MAE-errors[i][0]) ,2.0);
		standard_MSE+= pow( (average_MSE-errors[i][1]) ,2.0);
		standard_RMSE+= pow( (average_RMSE-errors[i][2]) ,2.0);
	}

	mean_MAE/=((double)size);
	mean_MSE/=((double)size);
	mean_RMSE/=((double)size);

	standard_MAE/=((double)size);
	standard_MSE/=((double)size);
	standard_RMSE/=((double)size);

	printf("\nMean:");
	printf("\nMAE:  %.3f",average_MAE);
	printf("\nMSE:  %.3f",average_MSE);
	printf("\nRMSE: %.3f\n",average_RMSE);

	printf("\nMean deviations:");
	printf("\nMAE:  %.3f",mean_MAE);
	printf("\nMSE:  %.3f",mean_MSE);
	printf("\nRMSE: %.3f",mean_RMSE);

	printf("\n\nStandard deviations:");
	printf("\nMAE:  %.3f",sqrt(standard_MAE));
	printf("\nMSE:  %.3f",sqrt(standard_MSE));
	printf("\nRMSE: %.3f",sqrt(standard_RMSE));
}

