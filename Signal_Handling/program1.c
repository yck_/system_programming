/*Yusuf Can Kan*/

/*Program 1 functions.*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#define COORDINATES_SIZE 20 /*Every single x,y coordinates size.*/
#define POINTS_SIZE 10.0 /*(x,y) points size.*/
#define LINES_SIZE 255
#define SINGLE_COORDINATE_LENGTH 10
#define ONE 1
#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define PLUSONE 1
#define TRUE 1


/*Takes the input bytes and b0,b1 for line equation (b1x+b0) and fills 
	the line string with this for filling the temporary file.*/
void fill_line(const unsigned char *bytes,char **line,double b0,double b1){

	int i;
	(*line) = (char*)malloc(LINES_SIZE*sizeof(char));
	
	for(i=START;i<COORDINATES_SIZE;i=i+2){
		sprintf( &((*line)[strlen(*line)]),"(%d, %d),",(unsigned int)bytes[i],(unsigned int)bytes[i+PLUSONE] );
	}
	if(b0 >= 0)
		sprintf( &((*line)[strlen(*line)])," %.3fx+%.3f",b1,b0);
	else 
		sprintf( &((*line)[strlen(*line)])," %.3fx%.3f",b1,b0);
}

/*Calculates the least square method, finds ax+b equation
	and fills the line string with following  form
 	(x_1, y_1),....,(x_10, y_10), b0x+b1  */
void least_squares_method(unsigned char *bytes,char **line){
	
	int i,
		temp_x=ZERO,temp_y=ZERO; 
	double average_x=ZERO,average_y=ZERO, /*Average of x and y coordinates.*/
		x_1=ZERO,y_1=ZERO,/*It holds x1 - x(average) any y1 - y(average).... */
		b1_nom=ZERO,b1_denom=ZERO,/*Nominator and denominator for b1.*/
		b1=ZERO,b0=ZERO; /*b0 and b1 parameters for b1x+b0.*/

	for(i=START;i<COORDINATES_SIZE;i=i+2){
		temp_x+=((int)bytes[i]);
		temp_y+=((int)(bytes[i+PLUSONE]));
	}

	average_x=((double)temp_x)/(double)POINTS_SIZE ;
	average_y=((double)temp_y)/(double)POINTS_SIZE ;
	
	for(i=START;i<COORDINATES_SIZE;i+=2){
		temp_x=((int)bytes[i]);
		temp_y=((int)(bytes[i+PLUSONE]));
		x_1 = ( (double)temp_x - average_x);
		y_1 = ( (double)temp_y - average_y);
		b1_denom+=pow(x_1,2.0);
		b1_nom += (x_1 * y_1);
	}
	b1=(b1_nom/b1_denom);
	b0= average_y - ( b1 * (average_x) );

	fill_line(bytes,line,b0,b1);
}

/*Writes string to the file with using given fd.*/
void writefile(int fd,char *line){
	unsigned char newline='\n';
	int size=strlen(line);
	int error=0;
	error=write(fd,line,size);
	if(error == ERROR_CONTROL){
		fprintf (stderr, "\nFailed to write file.");
		exit(EXIT_FAILURE);
	}
	error=write(fd,&newline,1);
	if(error == ERROR_CONTROL){
		fprintf (stderr, "\nFailed to write file.");
		exit(EXIT_FAILURE);
	}
}
