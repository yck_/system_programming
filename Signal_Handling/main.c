/*Yusuf Can Kan*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "program1.h"
#include "program2.h"

#define COORDINATES_SIZE 20 /*Every single x,y coordinates size.*/
#define POINTS_SIZE 10.0 /*(x,y) points size.*/
#define LINES_SIZE 255
#define SINGLE_COORDINATE_LENGTH 10
#define ONE 1
#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define PLUSONE 1
#define TRUE 1

/*Signal variables.*/
sig_atomic_t isSIGINTsent = ZERO;
sig_atomic_t isSIGUSR1sent = ZERO;
sig_atomic_t isSIGUSR1sentP1 = ZERO;
sig_atomic_t isSIGUSR2sent = ZERO;

/*Fork return variable.*/
pid_t fork_return; 
/*Files*/
char *file1;
char *file2;
char template[] = "tempfileXXXXXX";

/*SIGINT handler for program 1.*/
void SIGINT_handler(int signo){
	isSIGUSR1sentP1++;
}
/*Signal handler for SIGTERM.*/
void SIGTERM_handler(int signo){
	unlink(file1);
	unlink(template);

	if(fork_return==0){
		kill(getppid(),SIGKILL);
	}else{
		kill(fork_return,SIGKILL);
	}
	
	exit(EXIT_FAILURE);
}

/*Signal handler for SIGUSR1 and SIGUSR2.*/
void handler(int signo){
	if(signo == SIGUSR1) ++isSIGUSR1sent;
	else if(signo == SIGUSR2) ++isSIGUSR2sent;
	else if(signo==SIGINT) ++isSIGINTsent;	
}

int io_files(int argc, char* argv[], char** file1, char** file2){
	int opt,file_flag=0;
	
	while((opt = getopt(argc, argv, "i:o:")) !=-1 ){
		switch(opt){
			case 'i':
					*file1=optarg;
					file_flag++;
					break;
			case 'o':
					*file2=optarg;
					file_flag++;
					break;
            default:
					fprintf (stderr, "\nPlease provide proper input: ./program -i inputPath -o outputPath\n");
					exit(EXIT_FAILURE);
		}
	}
	if(file_flag<2){
		fprintf (stderr, "\nPlease provide proper input: ./program -i inputPath -o outputPath\n");
		exit(EXIT_FAILURE);
	}
	return 0;
}


void program1(const char *inputFile,int tempfd,pid_t childpid,int fd){
	unsigned char bytes[COORDINATES_SIZE];
	size_t bytes_read;
	unsigned int coordinates[COORDINATES_SIZE]; /*Holds the converted coordinates.*/
	char *line;
	sigset_t mask;
	struct sigaction act,actold;
	int bytes_counter=ZERO;
	/*For output file.*/
	struct flock lock;
	memset(&lock,0,sizeof(lock));

	/*For input file.*/
	struct flock lock_input;
	memset(&lock_input,0,sizeof(lock_input));

	/*Locks the input file until end of the Program 1.*/
	lock_input.l_type=F_RDLCK;
	fcntl(fd,F_SETLKW,&lock_input);

	/*Sets the signal masks.*/
	if( (sigemptyset(&mask))==-1 || (sigaddset(&mask,SIGINT))==-1 || (sigaddset(&mask,SIGSTOP))==-1 ){
		
		/*Opens the input file lock.*/
		lock_input.l_type=F_UNLCK;
		fcntl(fd,F_SETLKW,&lock_input);
		
		if(close(fd)==ERROR_CONTROL){
			fprintf (stderr, "\nInput file couldn't close.\nerrno = %d: %s \n", 
				errno, strerror (errno));
			exit(EXIT_FAILURE);
		}

		kill(childpid,SIGUSR1); /*Send child SIGUSR1 because P1 is exiting.*/
		fprintf (stderr, "\nFailed to initialize the signal mask.\nerrno = %d: %s \n", 
			errno, strerror (errno));
		exit(EXIT_FAILURE);
	}

	
	/*FOR SIGINT in critical section.*/
	memset(&act,0,sizeof(act));
	memset(&actold,0,sizeof(actold));
	/*Stores the old action.*/
	sigaction(SIGINT,NULL,&actold);
	act.sa_handler = &SIGINT_handler;


	/*First reads the file and looks if there is enough character or not.*/
	bytes_read=read(fd,bytes,sizeof(bytes));
	if(bytes_read==ERROR_CONTROL || bytes_read!=COORDINATES_SIZE){
		
		/*Opens the input file lock.*/
		lock_input.l_type=F_UNLCK;
		fcntl(fd,F_SETLKW,&lock_input);
		
		if(close(fd)==ERROR_CONTROL){
			fprintf (stderr, "\nInput file couldn't close.\nerrno = %d: %s \n", 
				errno, strerror (errno));
			exit(EXIT_FAILURE);
		}
	
		kill(childpid,SIGUSR1); /*Send child SIGUSR1 because P1 is exiting.*/
		fprintf (stderr, "\nNot enough character in the input file.\n");
		exit(EXIT_FAILURE);
	}

	do{
		sigaction(SIGINT,&act,NULL);
		least_squares_method(bytes,&line);  /*CRITICAL SECTION*/
		sigaction(SIGINT,&actold,NULL);
		/*Reads another 20 bytes.*/
		bytes_read=read(fd,bytes,sizeof(bytes));
		if(bytes_read==ERROR_CONTROL){
			
			/*Opens the input file lock.*/
			lock_input.l_type=F_UNLCK;
			fcntl(fd,F_SETLKW,&lock_input);
		

			if(close(fd)==ERROR_CONTROL){
				fprintf (stderr, "\nInput file couldn't close.\nerrno = %d: %s \n", 
					errno, strerror (errno));
				exit(EXIT_FAILURE);
			}
			fprintf (stderr, "\nReading error.\n");
			free(line);
			exit(EXIT_FAILURE);
		}
		
		/*Locks the temp file.*/
		lock.l_type=F_WRLCK;
		fcntl(tempfd,F_SETLKW,&lock);
		/*Goes the end of file.*/
		lseek(tempfd,0,SEEK_END);

		
		/*Writes it to the temp. file.*/
		writefile(tempfd,line);

		/*Unlocks the temp file.*/
		lock.l_type=F_UNLCK;
		fcntl(tempfd,F_SETLKW,&lock);
		
		/*Free the line string.*/
		free(line);
		bytes_counter+=COORDINATES_SIZE;

		/*Sent signal to P2 for if it is suspended.*/
		kill(childpid,SIGUSR2);
	}while(bytes_read==sizeof(bytes));

	kill(childpid,SIGUSR1);

	/*Unlocks the input file.*/
	lock_input.l_type=F_UNLCK;
	fcntl(fd,F_SETLKW,&lock_input);

	printf("\nReaded bytes as input: %d",bytes_counter);
	printf("\nLine Equation count: %d",bytes_counter/20);
	printf("\nSended signals while Program1 in the critical section:");
	if(isSIGUSR1sentP1 != 0) printf(" SIGINT");
	printf("\n\n");
}

void program2(char *outputFile,char *tempFile,int temp_fd,int fd){
	int i,
		size_errors=ZERO; /*Size for errors array.*/
	double b0=ZERO,b1=ZERO, /*b0 and b1 coordinates for b0x+b1*/
	MAE=ZERO,MSE=ZERO,RMSE=ZERO; /*Mean Absolute Error, Mean Squared Error, Root Mean Squared Error*/
	int coordinates[COORDINATES_SIZE];
	double **errors=NULL; /*It holds the error for derivation calculations.*/
	char *line; /*Line for output file.*/
	struct sigaction act,actold; /*For critical section.*/
	sigset_t new_mask; /*for sigsuspend*/
	struct flock lock;
	memset(&lock,0,sizeof(lock));

	struct flock output_lock;
	memset(&output_lock,0,sizeof(output_lock));

	line = (char*)malloc(LINES_SIZE*sizeof(char));
	/*Locks the output file.*/
	output_lock.l_type=F_WRLCK;
	fcntl(fd,F_SETLKW,&output_lock);

	/*Sets sigsuspend parameter for empty file.*/
	sigfillset(&new_mask);
	sigdelset(&new_mask,SIGUSR1);
	sigdelset(&new_mask,SIGUSR2);

	/*FOR SIGINT in critical section.*/
	memset(&act,0,sizeof(act));
	memset(&actold,0,sizeof(actold));
	/*Stores the old action.*/
	sigaction(SIGINT,NULL,&actold);
	act.sa_handler = &handler;

	/*This loop reads the line and calculates the error. After that it 
		fills the output file.*/
	while( TRUE ){  
		/*Locks the temp file*/
	
		lock.l_type=F_WRLCK;
		fcntl(temp_fd,F_SETLKW,&lock);
		/*Reading part.*/
		if((readline(temp_fd,coordinates,&b0,&b1,line)) == RETURN_FAIL){
			
			lock.l_type=F_UNLCK;
			fcntl(temp_fd,F_SETLKW,&lock);
			if(isSIGUSR1sent == 0){ /*If Program1 hasn't finished yet.*/

			/*If program 1 hasn't finished yet, it suspends the process 
				until SIGUSR1 or SIGUSR2 comes. */
				sigsuspend(&new_mask);
				
				lock.l_type=F_WRLCK;
				fcntl(temp_fd,F_SETLKW,&lock);
				if((readline(temp_fd,coordinates,&b0,&b1,line)) != RETURN_SUCCESS && 	
						isSIGUSR1sent!=0){
							lock.l_type=F_UNLCK;
							fcntl(temp_fd,F_SETLKW,&lock);
							break;
						}
			}
			else{
				break;
			}
		}
		/*Removes the line from file*/
		remove_line(temp_fd,tempFile);
		
		lock.l_type=F_UNLCK;
		fcntl(temp_fd,F_SETLKW,&lock);

		/*Ignores the SIGINT signal for critical section.*/
		sigaction(SIGINT,&act,NULL);
		/*Critical Section!*/

		MAE=meanAbsoluteError(coordinates,b0,b1);
		MSE=meanSquaredError(coordinates,b0,b1);
		RMSE=rootMeanSquaredError(coordinates,b0,b1); 

		/*Sets SIGINT .*/
		sigaction(SIGINT,&actold,NULL);

		/*Adds errors to array for 
			derivation calculation.*/
		add_errors(&errors,size_errors,MAE,MSE,RMSE);
		size_errors++;
		
		sprintf( &((line)[(strlen(line)-1)]),", %.3f, %.3f, %.3f",MAE,MSE,RMSE);
		/*Write to output file.*/

		writefile(fd,line);
	}

	if(MAE != 0 || MSE !=0 || RMSE != 0){
		printf("\n\nEach error metric:");
		printf("\nMAE - MSE - RMSE");
		for(i=0;i<size_errors;i++){
			printf("\n%.3f - %.3f - %.3f",errors[i][0],errors[i][1],errors[i][2]);
		}
		printf("\n\n");

		print_deviations(errors,size_errors);
		printf("\n");
	}
		
	/*Unlocks the output file.*/
	output_lock.l_type=F_UNLCK;
	fcntl(fd,F_SETLKW,&output_lock);

	for(i=0;i<size_errors;i++){
		free(errors[i]);
	}
	free(errors);
	free(line);
}

int main(int argc, char* argv[]){
    int tempfd,temp_fd2, /*File descriptors for temporary file.*/
		inputfd,outputfd; 
    /*Takes command line arguments and makes assignments.*/
	if((io_files(argc,argv, &file1, &file2))==ERROR_CONTROL){
		fprintf (stderr, "\nCommand line agument problem.\n");
		exit(EXIT_FAILURE);
	}

	/*Opens input file.*/
	inputfd=open(file1,O_RDONLY);
	if(inputfd==ERROR_CONTROL){
		fprintf (stderr,"\nerrno = %d: %s \n",errno,strerror (errno));
		exit(EXIT_FAILURE);
	}

	/*Opens output file.*/
	outputfd=open(file2,O_WRONLY);
	if(outputfd==ERROR_CONTROL){
		fprintf (stderr,"\nFile couldn't open\nerrno = %d: %s \n",errno,strerror (errno));
		exit(EXIT_FAILURE);
	}
	
	/*Creates the temp file.*/
	tempfd=mkstemp(template);
	if(tempfd==ERROR_CONTROL){
		fprintf (stderr,"\nTemp. file couldn't open\nerrno = %d: %s \n",errno,strerror (errno));
		exit(EXIT_FAILURE);
	}

	if(signal(SIGUSR1,handler)==SIG_ERR){
		fprintf (stderr,"\nerrno = %d: %s \n",errno,strerror (errno));
		exit(EXIT_FAILURE);
	}
	if(signal(SIGUSR2,handler)==SIG_ERR){
		fprintf (stderr,"\nerrno = %d: %s \n",errno,strerror (errno));
		exit(EXIT_FAILURE);
	}
	if(signal(SIGTERM,SIGTERM_handler)==SIG_ERR){
		fprintf (stderr,"\nerrno = %d: %s \n",errno,strerror (errno));
		exit(EXIT_FAILURE);
	}

	fork_return=fork();

	if(fork_return!=0){
		program1(file1,tempfd,fork_return,inputfd); 
			/*Closing the input file.*/
		if(close(inputfd)==ERROR_CONTROL){
			fprintf (stderr,"\nerrno = %d: %s \n",errno,strerror (errno));
			exit(EXIT_FAILURE);
		}
	}
	else{	
		temp_fd2=open(template,O_RDWR);	
		if(temp_fd2==ERROR_CONTROL){
			fprintf (stderr,"\nerrno = %d: %s \n",errno,strerror (errno));
			exit(EXIT_FAILURE);
		}

		program2(file2,template,temp_fd2,outputfd);

		if(close(outputfd)==ERROR_CONTROL){
			fprintf (stderr,"\nerrno = %d: %s \n",errno,strerror (errno));
			exit(EXIT_FAILURE);
		}
		if(close(temp_fd2)==ERROR_CONTROL){
			fprintf (stderr,"\nerrno = %d: %s \n",errno,strerror (errno));
			exit(EXIT_FAILURE);
		}
		unlink(template);
		unlink(file1);
	}
	return 0;
}
