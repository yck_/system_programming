/*Yusuf Can Kan */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include "student.h"
#include "print_handler.h"
#include "kitchen.h"

#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define TRUE 1
#define SEM_COUNT 17 /*Semaphore count*/
#define COOK_FINISH 1
#define CONTINUE_TAKING_PLATE 0
#define READ 0
#define WRITE 1
#define CONTAINER_SIZE 1024

/*FUNCTION SIGNITURES*/

/*Takes command line arguments with getopt*/
int command_line(int argc, char* argv[]);

/*Delivers plate to the kitchen. */
int supplier(sem_t *plate,sem_t *kitchen,sem_t *lock,sem_t *soup_deliv,
                sem_t *main_deliv,sem_t *dessert_deliv,sem_t *print_mutex,int *k_count);

/*It creates supplier process.*/
int create_supplier(sem_t *plate,sem_t *kitchen,sem_t *lock,sem_t *soup_deliv,
                    sem_t *main_deliv,sem_t *dessert_deliv,sem_t *print_mutex,int *k_count);


int main(int argc, char* argv[]){

     /*Parameters.*/
    int i;

    int sh_semap,sh_kitchen_count,sh_counter_count,sh_table; /*Shared memory variables.*/
    mode_t perms=(S_IRUSR | S_IWUSR);
    struct sigaction sigchld_action,sigint_action; /*This variables for handling sigchld and sigint.*/
	char dummy;
    
    /*Assignes the command line arguments.*/
    if((command_line(argc,argv))==ERROR_CONTROL){
        /*Print screen error .*/
        sprintf(error_screen,"\nCommand line agument problem.\n");                
        print_error(error_screen);

		exit(EXIT_FAILURE);
	}

    /*Main waits all the childs process with using
        this pipe. After all the child process finished
        their job they closes write side of this pipe 
        so main returns 0 and ends */
    if(pipe(waitpipe)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while creating pipe.\n");                
        print_error(error_screen);
		exit(EXIT_FAILURE);
    }

    /*Sets the signal handler for SIGCHLD.*/
	memset(&sigchld_action,0,sizeof(sigchld_action));
	sigchld_action.sa_handler = &sigchld_handler;
	sigchld_action.sa_flags=SA_RESTART;
	if(sigaction(SIGCHLD,&sigchld_action,NULL)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sigaction operation.\n");                
        print_error(error_screen);
    }

    /*Sets the signal handler for SIGINT.*/
	memset(&sigint_action,0,sizeof(sigint_action));
	sigint_action.sa_handler = &sigint_handler_main;
	if(sigaction(SIGINT,&sigint_action,NULL)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sigaction operation.\n");                
        print_error(error_screen);
    }

    kitchen_size=2*times*students+1;

    /* --- SEMAPHORES AND SHARED MEMORY INITIALIZATION. --- */

    /*This shm for creating semaphores.*/
    /*Create shared memory object for semaphores.*/
    sh_semap=shm_open("sharedFd",O_RDWR | O_CREAT,perms);
    if(sh_semap == ERROR_CONTROL){
        sprintf(error_screen,"\nError creating shared memory object: %s!\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    /*Set shared memory size.*/
    if(ftruncate(sh_semap,SEM_COUNT*sizeof(sem_t)) == -1){
        sprintf(error_screen,"\nError while ftruncate opetation: %s!\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    sems=mmap(NULL,SEM_COUNT*sizeof(sem_t),PROT_READ | PROT_WRITE, MAP_SHARED, sh_semap, 0);
    if (sems == MAP_FAILED){
        sprintf(error_screen,"\nError while mmap operation: %s!\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }

    /*This SHM only for storing the plates in kitchen.*/
    /*Create shared memort object for kitchen.*/
    sh_kitchen_count=shm_open("sharedFd1",O_RDWR | O_CREAT,perms);
    if(sh_kitchen_count == ERROR_CONTROL){
        sprintf(error_screen,"\nError creating shared memory object: %s!\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    /*Set shared memory size.*/
    if(ftruncate(sh_kitchen_count,4*sizeof(int)) == -1){
        sprintf(error_screen,"\nError while ftruncate operations: %s!\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    kitchen_count=mmap(NULL,4*sizeof(int),PROT_READ | PROT_WRITE, MAP_SHARED, sh_kitchen_count, 0);
    if (kitchen_count == MAP_FAILED){
        sprintf(error_screen,"\nError while mmap operations: %s!\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    

    /*This SHM only for storing the plates in counter.*/
    /*Create shared memort object for kitchen.*/
    sh_counter_count=shm_open("sharedFd2",O_RDWR | O_CREAT,perms);
    if(sh_counter_count == ERROR_CONTROL){
        sprintf(error_screen,"\nError creating shared memory object: %s!\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    /*Set shared memory size.*/
    if(ftruncate(sh_counter_count,6*sizeof(int)) == -1){
        sprintf(error_screen,"\nError while ftruncate operations: %s!\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    counter_count=mmap(NULL,6*sizeof(int),PROT_READ | PROT_WRITE, MAP_SHARED, sh_counter_count, 0);
    if (counter_count == MAP_FAILED){
        sprintf(error_screen,"\nError while mmap operations: %s!\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    
    
    /*This array hols each table as in array. Last element
        of this array is empty table count.*/
    sh_table=shm_open("sharedFd3",O_RDWR | O_CREAT,perms);
    if(sh_table == ERROR_CONTROL){
        sprintf(error_screen,"\nError whilecreating shared memort object: %s!\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    /*Set shared memory size.*/
    if(ftruncate(sh_table,(tables+1)*sizeof(int)) == -1){
        sprintf(error_screen,"\nError while ftruncate operations: %s!\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    empty_table=mmap(NULL,(tables+1)*sizeof(int),PROT_READ | PROT_WRITE, MAP_SHARED, sh_table, 0);
    if (empty_table == MAP_FAILED){
        sprintf(error_screen,"\nError while mmap operations: %s!\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }

    /* --- SEMAPHORES AND SHARED MEMORY INITIALIZATION END. --- */

    /*Empty all the tables.*/
    for(i=ZERO;i<tables;i++){
         empty_table[i]=0;
    }
    /*Set last element to empty table size.*/
    empty_table[tables]=tables;


    /*Sets soup,main course and dessert to 0.*/
    kitchen_count[0]=0;
    kitchen_count[1]=0;
    kitchen_count[2]=0;
    kitchen_count[3]=0;

    counter_count[0]=0;
    counter_count[1]=0;
    counter_count[2]=0;
    counter_count[3]=0;/*For counter line*/
    counter_count[4]=0;/*For count of undergraduate students.*/
    counter_count[5]=0;/*When all the foods in kitchen is done one of the cooks sets  this to 1 
                            so other cooks doesn't try to take plate from kitchen and put plate to counter.*/
     /*This semaphores synronies all the processes. They deals with
    supplier,counter,cook,students and table. This part sets every semaphore to its value.*/
    if(sem_init(&(sems[0]),1,0)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[1]),1,kitchen_size)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[2]),1,1)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[3]),1,counter_size)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[4]),1,0)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[5]),1,1)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[6]),1,0)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[7]),1,0)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[8]),1,0)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[9]),1,0)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[10]),1,1)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[11]),1,1)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[12]),1,1)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[13]),1,1)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[14]),1,tables)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(sem_init(&(sems[15]),1,1)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    /*Mutex for saving print screen operations.*/
    if(sem_init(&(sems[16]),1,1)==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }

        /*Fork all the graduate and undergraduate students.*/
    if(create_studets(counter_count,(&(sems[3])),(&(sems[4])),(&(sems[5])),(&(sems[6])),(&(sems[10])),
        (&(sems[11])),(&(sems[12])),(&(sems[13])),(&(sems[14])),(&(sems[15])),&(sems[16]),empty_table)==ERROR_CONTROL){
        
        clear_main();
        exit(EXIT_FAILURE);
    }

    if(create_cooks(cooks,&(sems[0]),&(sems[1]),&(sems[2]),kitchen_count,&(sems[3]),&(sems[4]), 
                        &(sems[5]),&(sems[6]),&(sems[7]),&(sems[8]),&(sems[9]),&(sems[16]),counter_count)==ERROR_CONTROL){     
        clear_main();
        exit(EXIT_FAILURE);
    }
    
    if(create_supplier((&(sems[0])),&(sems[1]),&(sems[2]),&(sems[7]),&(sems[8]),&(sems[9]),&(sems[16]),kitchen_count)==ERROR_CONTROL){
        clear_main();
        exit(EXIT_FAILURE);
    }

    /*After all the pipe operations close write side of pipe
        and start listen read side of pipe.*/
    if(close(waitpipe[WRITE])==ERROR_CONTROL){
        clear_main();
        sprintf(error_screen,"\nError while closing pipe: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }

    /*It blocks the main process until all childs 
        finish their jobs.*/
	if((read(waitpipe[READ],&dummy,1)) != 0){
        clear_main();
        sprintf(error_screen,"\nErrror while initializing semaphore: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }

    /*If ctrl+c comes while cleaning process it may create problem so after here
        process ignores ctrl+c.*/
	memset(&sigint_action,0,sizeof(sigint_action));
	sigint_action.sa_handler = SIG_IGN;
	if(sigaction(SIGINT,&sigint_action,NULL)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sigaction operation.\n");                
        print_error(error_screen);
    }

    clear_main();
    return 0; 
}


/*This function only for clearing main process,
    it runs at the end of the main and
    before program exits if something unexpected happens.*/
void clear_main(){
    int i;
        /*Free and unmap all resources and finish main.*/
    for(i=START;i<SEM_COUNT;i++){
        if(sem_destroy(&(sems[i]))==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_destroy operation: %s\n",strerror(errno));                
            print_error(error_screen);
            exit(EXIT_FAILURE);
        }
    }

    clear_memories();
   
    if(shm_unlink("sharedFd")==ERROR_CONTROL){
        sprintf(error_screen,"\nError while unlink shared memory: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(shm_unlink("sharedFd1")==ERROR_CONTROL){
        sprintf(error_screen,"\nError while unlink shared memory: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(shm_unlink("sharedFd2")==ERROR_CONTROL){
        sprintf(error_screen,"\nError while unlink shared memory: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(shm_unlink("sharedFd3")==ERROR_CONTROL){
        sprintf(error_screen,"\nError while unlink shared memory: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    
}

/*Delivers plate to the kitchen. */
int supplier(sem_t *plate,sem_t *kitchen,sem_t *lock,sem_t *soup_deliv,
                sem_t *main_deliv,sem_t *dessert_deliv,sem_t *print_mutex,int *k_count){
    int i,soup,main_course,dessert,bytes_read;
    char meal;
    int fd;

    fd=open(filePath,O_RDONLY);
	if(fd==ERROR_CONTROL){
        sprintf(error_screen,"\nFile couldn't open\nerrno = %d: %s \n",errno,strerror (errno));                
        print_error(error_screen);
        return RETURN_FAIL;
	}
  
    for(i=ZERO;i<3*times*students;i++){
        
        bytes_read=read(fd,&meal,1);
		if(bytes_read==ERROR_CONTROL){
            sprintf(error_screen,"\nError while reading file");                
            print_error(error_screen);
            return RETURN_FAIL;
		}
        
        if(meal=='P') meal=0;
        else if(meal=='C') meal=1;
        else if(meal=='D') meal=2;

        /*Save the print.*/
        if(sem_wait(lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operations");                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*This mutex saves the print.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operations");                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        /*Print proper output to screen*/
        if(meal == 0){ 
            sprintf(print_screen,
                    "The supplier is going to the kitchen to deliver soup: kitchen items P:%d,C:%d,D:%d=%d\n",
                    k_count[0],k_count[1],k_count[2],k_count[0]+k_count[1]+k_count[2]);
            
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        }
        else if(meal == 1){
            sprintf(print_screen,
                "The supplier is going to the kitchen to deliver main course: kitchen items P:%d,C:%d,D:%d=%d\n",
                k_count[0],k_count[1],k_count[2],k_count[0]+k_count[1]+k_count[2]);
            
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        }
        else{
            sprintf(print_screen,
                "The supplier is going to the kitchen to deliver dessert: kitchen items P:%d,C:%d,D:%d=%d\n",
                k_count[0],k_count[1],k_count[2],k_count[0]+k_count[1]+k_count[2]);
            
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        }
        
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /* Savely sending plate.*/
        if(sem_post(lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        


        /*Decreases empty places.*/
        if(sem_wait(kitchen)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Locks for critical section.*/
        if(sem_wait(lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        if(i==3*times*students-1)  k_count[3]--;

        k_count[meal]++;

        /*This mutex saves the print.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        if(meal == 0){

            /*Print screen.*/
            sprintf(print_screen,
                "The supplier delivered soup – after delivery: kitchen items P:%d,C:%d,D:%d=%d\n",
                k_count[0],k_count[1],k_count[2],k_count[0]+k_count[1]+k_count[2]);
            
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }            
            

            /*Increase soup is delivered so cook don't have to wait.*/
            if(sem_post(soup_deliv)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        }
        else if(meal == 1){

            /*Print screen.*/
            sprintf(print_screen,
                "The supplier delivered main course – after delivery: kitchen items P:%d,C:%d,D:%d=%d\n",
                k_count[0],k_count[1],k_count[2],k_count[0]+k_count[1]+k_count[2]);
            
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }        

            /*Increase main course is delivered so cook don't have to wait for this meal.*/
            if(sem_post(main_deliv)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        }
        else{
            
            /*Print screen.*/
            sprintf(print_screen,
                "The supplier delivered dessert – after delivery: kitchen items P:%d,C:%d,D:%d=%d\n",
                k_count[0],k_count[1],k_count[2],k_count[0]+k_count[1]+k_count[2]);
            
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }            
                            
            /*Increase dessert is delivered so cook don't have to wait for this meal.*/
            if(sem_post(dessert_deliv)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        }

        /*Unlock print mutex*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        /*Unlocks critical section.*/
        if(sem_post(lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Increases plate 1 so cook can take one plate.*/
        if(sem_post(plate)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
    }
    
    /*This mutex saves the print.*/
    if(sem_wait(print_mutex)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
        print_error(error_screen);
        return RETURN_FAIL;
    }
    
    /*Print screen.*/
    sprintf(print_screen,"\nThe supplier finished supplying – GOODBYE!\n");   
    if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
        sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
        print_error(error_screen);
        return RETURN_FAIL;
    }       

    /*Unlock print mutex.*/
    if(sem_post(print_mutex)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
        print_error(error_screen);
        return RETURN_FAIL;
    }

    return RETURN_SUCCESS;
}

/*It creates supplier process.*/
int create_supplier(sem_t *plate,sem_t *kitchen,sem_t *lock,sem_t *soup_deliv,
                    sem_t *main_deliv,sem_t *dessert_deliv,sem_t *print_mutex,int *k_count){
    struct sigaction sigint_action;

    switch (fork()){
		case -1:
			    sprintf(error_screen,"\nError while fork operations\n");                
                print_error(error_screen);
                clear_memories();
                exit(EXIT_SUCCESS);
		case 0:

            /*Sets the signal handler for SIGINT.*/
            memset(&sigint_action,0,sizeof(sigint_action));
            sigint_action.sa_handler = &sigint_handler_childs;
            if(sigaction(SIGINT,&sigint_action,NULL)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sigaction operation: %s",strerror (errno));                
                print_error(error_screen);
                clear_memories();
                exit(EXIT_SUCCESS);
            }

                /*It closes read side of the pipe.*/
            if(close(waitpipe[READ])==ERROR_CONTROL){
                sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                print_error(error_screen);
                clear_memories();
                exit(EXIT_SUCCESS);
            }

            if(supplier(plate,kitchen,lock,soup_deliv,main_deliv,dessert_deliv,print_mutex,k_count)==ERROR_CONTROL){
                clear_memories();         
                exit(EXIT_SUCCESS);
            }

            /*If ctrl+c comes while cleaning process it may create problem so after here
                    process ignores ctrl+c.*/
            memset(&sigint_action,0,sizeof(sigint_action));
            sigint_action.sa_handler = SIG_IGN;
            if(sigaction(SIGINT,&sigint_action,NULL)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sigaction operation: %s",strerror (errno));                
                print_error(error_screen);
                clear_memories();
                exit(EXIT_SUCCESS);
            }
            
            clear_memories();

            /*It closes write side of the pipe so main 
                process can continue.*/
            if(close(waitpipe[WRITE])==ERROR_CONTROL){
                sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                print_error(error_screen);
                clear_memories();
                exit(EXIT_SUCCESS);
            }

			exit(EXIT_SUCCESS);
		default:
			break;
	}
    return RETURN_SUCCESS;
}

