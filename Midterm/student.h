/*Yusuf Can Kan*/

#ifndef STUDENT_H
#define STUDENT_H


/*Below 3 function runs only graduate and undergraduate case.*/

/*This function represent only student case. It is also be used in graduate and 
    undergraduate functions for taking food from counter.*/
int take(int number,int *c_count,sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock,
            sem_t *three_meal,char isGraduate,int round,sem_t* mutex_table,sem_t *print_mutex);

/*Graduate students*/
int graduate_f(int number,int *c_count,sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock,
            sem_t *three_meal,sem_t* try_take,sem_t* secure_take, sem_t *wait_take,
            sem_t *lock_graduate,sem_t *table,sem_t *mutex_table,sem_t *print_mutex, int *empty_table);

/*Undergraduate students*/
int undergraduate_f(int number,int *c_count,sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock,
            sem_t *three_meal,sem_t* try_take,sem_t* secure_take, sem_t *wait_take,
            sem_t *table,sem_t *mutex_table,sem_t *print_mutex, int *empty_table);

/*This function runs only if user provides -M from command line.*/
int students_f(int number,int *c_count,sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock,
            sem_t *three_meal,sem_t *table,sem_t *mutex_table,sem_t *print_mutex, int *empty_table);

/*This function creates both graduate and undergraduate students.*/
int create_studets(int *c_count,sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock,
                    sem_t *three_meal,sem_t* try_take,sem_t* secure_take,
                    sem_t *wait_take,sem_t *lock_graduate,sem_t *table,sem_t *mutex_table,
                    sem_t *print_mutex, int *empty_table);


#endif
