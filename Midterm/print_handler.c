/*Yusuf Can Kan */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include "print_handler.h"

#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define TRUE 1
#define SEM_COUNT 17 /*Semaphore count*/
#define COOK_FINISH 1
#define CONTINUE_TAKING_PLATE 0
#define READ 0
#define WRITE 1
#define CONTAINER_SIZE 1024

int tables=0;
int times=0;
int students=0,graduate=0,undergraduate=0;

/*This variable stores the printed string and after that
    it print screen with write() adn STDOUT_FILENO variable.*/
char print_screen[CONTAINER_SIZE];

/*If something unexpected happens this
    variables sets and print screen to
    the error before exit.*/
char error_screen[CONTAINER_SIZE];

/*This variable will be mmaped. */
int *empty_table;


/*This pipe makes main process waits until all its childrens finished.*/
int waitpipe[2];

sem_t *sems; /*All the semaphores stores in this sem_t pointer so in case 
            of CTRL + C or something signal handler can reach this global 
            variable with ease. */

int cooks=0,counter_size=0,
        kitchen_size=0; /*Command Line parameters.*/


	char *filePath;

/*This variables will be mmaped. */
int *kitchen_count, /*Stores elements in the kitchen.*/
    *counter_count; /*Stores elements in the counter and 
                        stores information related counter.*/




/*This function using as printf if
    something unexpected happens.*/
void print_error (char *message){   
    write (STDERR_FILENO, message, strlen(message));
}

/*In case of CTRL + C, this handler invoke for main and free all 
	the resources and exits gracefully.*/
void sigint_handler_main(int signal_number){
    int i;
    for(i=START;i<SEM_COUNT;i++){
        if(sem_destroy(&(sems[i]))==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_destryo operation: %s\n",strerror(errno));                
            print_error(error_screen);
            exit(EXIT_FAILURE);
        }
    }

    clear_memories();
   
    if(shm_unlink("sharedFd")==ERROR_CONTROL){
        sprintf(error_screen,"\nError while unlink shared memory: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(shm_unlink("sharedFd1")==ERROR_CONTROL){
        sprintf(error_screen,"\nError while unlink shared memory: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(shm_unlink("sharedFd2")==ERROR_CONTROL){
        sprintf(error_screen,"\nError while unlink shared memory: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    if(shm_unlink("sharedFd3")==ERROR_CONTROL){
        sprintf(error_screen,"\nError while unlink shared memory: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }

    sprintf(error_screen,"Process %d: exited gracefully \n",getpid());                
    print_error(error_screen);
    exit(EXIT_FAILURE);
}

/*In case of CTRL + C, this handler invoke for childs and free all 
	the resources and exits gracefully.*/
void sigint_handler_childs(int signal_number){
    
    clear_memories();

    sprintf(error_screen,"Process %d: exited gracefully \n",getpid());                
    print_error(error_screen);
	exit(EXIT_SUCCESS);
}

/*This handler handles the SIGCHLD signal synchronously.*/
void sigchld_handler(int signal_number){
	int error;
	/*This loops executes wait to ecah child until there is no child exist. 
		In this way when main program recieves SIGCHILD first time whis process 
		will start. */
	while((error=waitpid(-1,NULL,WNOHANG))>0)
		if(error==ERROR_CONTROL){
            sprintf(error_screen,"Error while waitpid operations.\n");                
            print_error(error_screen);
            exit(EXIT_FAILURE);
        }
}



/*Takes command line arguments with getopt*/
int command_line(int argc, char* argv[]){
	int opt,counter_flag=0,M;
	
    if(argc==15){

        while((opt = getopt(argc, argv, "N:T:S:L:U:G:F:")) !=-1 ){
            switch(opt){
                case 'N':
                        cooks=atoi(optarg);
                        break;
                case 'T':
                        tables=atoi(optarg);
                        break;
                case 'S':
                        counter_size=atoi(optarg);
                        break;
                case 'L':
                        times=atoi(optarg);
                        break;
                case 'U':
                        undergraduate=atoi(optarg);
                        break;
                case 'G':
                        graduate=atoi(optarg);

                        break;
                case 'F':
                        filePath=optarg;
                        break;
                default:
                        
                        sprintf(error_screen,"Please provide proper input:\n ./program -N cooks -M students -T tables -S counterSize -L times -F filePathor \
\n ./program -N cooks -T tables -S counterSize -L times -U undergraduate_students -G graduate_students -F filePath\n");                
                        print_error(error_screen);
                        exit(EXIT_FAILURE);
            }
	    }
        students=graduate+undergraduate;

        if(students <= 3){
            sprintf(error_screen,"\nG + U must be bigger than 3 .\n");                
            print_error(error_screen);
            exit(EXIT_FAILURE);
        }
        else if(undergraduate<=graduate||graduate<1){
            sprintf(error_screen,"\nG must be bigger than 0 and U must be bigger than G .\n");                
            print_error(error_screen);
            exit(EXIT_FAILURE);
        }
    }
    else if(argc==13){
        while((opt = getopt(argc, argv, "N:M:T:S:L:F:")) !=-1 ){
            switch(opt){
                case 'N':
                        cooks=atoi(optarg);
                        break;
                case 'M':
                        students=atoi(optarg);
                        break;
                case 'T':
                        tables=atoi(optarg);
                        break;
                case 'S':
                        counter_size=atoi(optarg);
                        break;
                case 'L':
                        times=atoi(optarg);
                        break;
                case 'F':
                        filePath=optarg;
                        break;
                default:
                        sprintf(error_screen,"Please provide proper input:\n ./program -N cooks -M students -T tables -S counterSize -L times -F filePathor \
\n ./program -N cooks -T tables -S counterSize -L times -U undergraduate_students -G graduate_students -F filePath\n");                
                        print_error(error_screen);
                        exit(EXIT_FAILURE);
            }
	    }
    }
    else{
        sprintf(error_screen,"Please provide proper input:\n ./program -N cooks -M students -T tables -S counterSize -L times -F filePathor \
\n ./program -N cooks -T tables -S counterSize -L times -U undergraduate_students -G graduate_students -F filePath\n");                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }


    if(students<=3){
        sprintf(error_screen,"\nM or U+G must be bigger than 3.\n");                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    else if((students)<=(cooks)){
        sprintf(error_screen,"\nnN must be smaller than M.\n");                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    else if(cooks<=2){
        sprintf(error_screen,"\nnN must be bigger than 2.\n");                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    else if((counter_size)<=3){
        sprintf(error_screen,"\nS must be bigger than 3.\n");                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    else if((students) <= (tables) || (tables) < 1){
        sprintf(error_screen,"\nM or U+G  must be bigger than T and T must be bigger than 0.\n");                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    else if((times)<3){
        sprintf(error_screen,"\nL must be bigger than 2.\n");                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }

	return 0;
}

/*It unmaps all the memories.*/
void clear_memories(){
            /*FREE ALL RESOURCES.*/
    
    if(munmap(sems,SEM_COUNT*sizeof(sem_t)) == ERROR_CONTROL){
        sprintf(error_screen,"\nError while munmap operation: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }

    if(munmap(kitchen_count,4*sizeof(int)) == ERROR_CONTROL){
        sprintf(error_screen,"\nError while munmap operation: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }

    if(munmap(counter_count,5*sizeof(int)) == ERROR_CONTROL){
        sprintf(error_screen,"\nError while munmap operation: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
    
    if(munmap(empty_table,((tables+1)*sizeof(int))) == ERROR_CONTROL){
        sprintf(error_screen,"\nError while munmap operation: %s\n",strerror(errno));                
        print_error(error_screen);
        exit(EXIT_FAILURE);
    }
}


