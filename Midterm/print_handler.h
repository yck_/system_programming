#ifndef PRINT_HANDLER_H
#define PRINT_HANDLER_H

#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define TRUE 1
#define SEM_COUNT 17 /*Semaphore count*/
#define COOK_FINISH 1
#define CONTINUE_TAKING_PLATE 0
#define READ 0
#define WRITE 1
#define CONTAINER_SIZE 1024

/*This variable stores the printed string and after that
    it print screen with write() adn STDOUT_FILENO variable.*/
extern char print_screen[CONTAINER_SIZE];

/*If something unexpected happens this
    variables sets and print screen to
    the error before exit.*/
extern char error_screen[CONTAINER_SIZE];


/*This variable will be mmaped. */
extern int *empty_table;

extern int tables;
extern int times;	
extern int students,graduate,undergraduate;

/*This pipe makes main process waits until all its childrens finished.*/
extern int waitpipe[2];

extern sem_t *sems; /*All the semaphores stores in this sem_t pointer so in case 
            of CTRL + C or something signal handler can reach this global 
            variable with ease. */

extern int cooks,counter_size,
        kitchen_size; /*Command Line parameters.*/

extern char *filePath;

/*This variables will be mmaped. */
extern int *kitchen_count, /*Stores elements in the kitchen.*/
    *counter_count; /*Stores elements in the counter and 
                        stores information related counter.*/



/*This function using only inside the main process,
    it cleans the memories and semaphores.*/
void clear_main();

/*This function using as printf if
    something unexpected happens.*/
void print_error (char *message);

/*In case of CTRL + C, this handler invoke for main and free all 
	the resources and exits gracefully.*/
void sigint_handler_main(int signal_number);

/*In case of CTRL + C, this handler invoke for childs and free all 
	the resources and exits gracefully.*/
void sigint_handler_childs(int signal_number);

/*This handler handles the SIGCHLD signal synchronously.*/
void sigchld_handler(int signal_number);


/*Takes command line arguments with getopt*/
int command_line(int argc, char* argv[]);

/*It unmaps and clears all the memories.*/
void clear_memories();

#endif
