/*Yusuf Can Kan */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include "kitchen.h"
#include "print_handler.h"

#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define TRUE 1
#define SEM_COUNT 17 /*Semaphore count*/
#define COOK_FINISH 1
#define CONTINUE_TAKING_PLATE 0
#define READ 0
#define WRITE 1
#define CONTAINER_SIZE 1024

	/*It takes one plate form kitchen.*/
int get_from_kitchen(int number,sem_t *plate,sem_t *kitchen,sem_t *lock,int *k_count,
                    sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock, sem_t *three_meal,
                    sem_t *soup_deliv,sem_t *main_deliv,sem_t *dessert_deliv,sem_t *print_mutex,int *c_count){
    int err=1,
        check_value, /*For checking semaphores value.*/
        chose_meal, /*This variable choses meal for delivering counter.*/
        three_meal_flag=-1;/*This variable holds how many 3 kind of meals in the counter.*/
    /*Takes plate from kitchen.*/
    if(sem_wait(plate)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
        print_error(error_screen);
        return RETURN_FAIL;
    } 

    /* It will put one plate to counter so this will decrease 
        one empty place from counter.*/
    if(sem_wait(counter_size)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
        print_error(error_screen);
        return RETURN_FAIL;

    }

    /*Locks the critical section for counter.*/
    if(sem_wait(c_lock)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
        print_error(error_screen);
        return RETURN_FAIL;

    } 

    /*In this part it choses the meal for deliver to counter. This part looks the counter
        and tries to take most least meal. If kitchen doesn't have that meal it waits
        until meal arrives.*/
    if(c_count[0] == 0) chose_meal=0;
    else if(c_count[1] == 0) chose_meal=1;
    else if(c_count[2] == 0) chose_meal=2;
    else{
        if(c_count[0] <= c_count[1]) chose_meal=0;
        else chose_meal=1;
        if(c_count[chose_meal] > c_count[2]) chose_meal=2;
    }
            
    /*Checks if meals are done.*/
    if(c_count[5]!=-1){

        /*Save print screen operation.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        if(chose_meal==0){
            /*Print screen.*/
            sprintf(print_screen,
                "Cook %d is going to the counter to deliver soup – counter items P:%d,C:%d,D:%d=%d\n",
                number,c_count[0],c_count[1],c_count[2],c_count[0]+c_count[1]+c_count[2]);
            
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }        
        }
        else if(chose_meal==1){
            /*Print screen.*/
            sprintf(print_screen,
                "Cook %d is going to the counter to deliver main course – counter items P:%d,C:%d,D:%d=%d\n",
                number,c_count[0],c_count[1],c_count[2],c_count[0]+c_count[1]+c_count[2]);
            
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }        
        }
        else{
            /*Print screen.*/
            sprintf(print_screen,
                "Cook %d is going to the counter to deliver dessert – counter items P:%d,C:%d,D:%d=%d\n",
                number,c_count[0],c_count[1],c_count[2],c_count[0]+c_count[1]+c_count[2]);
            
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }   
        }
    
        /*Unlock the print mutex.*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }     

    }

    /*Locks critical section.*/
    if(sem_wait(lock)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
        print_error(error_screen);
        return RETURN_FAIL;
    }  
    
    /*Check if meal exits in counter.*/
    if(k_count[chose_meal]==0){
        /*This part check if kitchen has the meal this cook needs.*/
        if(chose_meal==0){
            /*First it unlocks it so supplier can deliver.*/
            if(sem_post(lock)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
            /*Waits for soup to deliver.*/
            if(sem_wait(soup_deliv)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }  
        }
        else if(chose_meal==1){
            /*First it unlocks it so supplier can deliver.*/
            if(sem_post(lock)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
            /*waits for main course to deliver.*/
            if(sem_wait(main_deliv)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }  
        }
        else{
        /*First it unlocks it so supplier can deliver.*/
            if(sem_post(lock)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        /*Waits for dessert to deliver.*/
            if(sem_wait(dessert_deliv)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        } 

        /*Locks critical section again.*/
        if(sem_wait(lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }  
    }
    else{ /*if it the plate exits in kitchen*/
        if(chose_meal==0){
            /*Decreases 1 from soup semaphore.*/
            if(sem_wait(soup_deliv)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }  
        }
        else if(chose_meal==1){
             /*Decreases 1 from main course semaphore.*/
            if(sem_wait(main_deliv)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }  
        }
        else{
            /*Decreases 1 from dessert semaphore.*/
            if(sem_wait(dessert_deliv)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }  
        }
    }
    
    /*Check if other cooks delivered all plates or not.*/
    if(k_count[0]==0 && k_count[1]==0 && k_count[2]==0 && k_count[3]!=0){
        
               /*Save print screen operation.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        /*Print screen.*/
        sprintf(print_screen,
            "Cook %d finished serving - items at kitchen: %d – going home – GOODBYE!!! \n",
            number,k_count[0]+k_count[1]+k_count[2]);
            
        if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
            sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }    

        /*Unlock the print screen mutex.*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*This part opens all the barrier. It must do this because 
            if it don't do it this semaphores may cause deadlock.*/
            
        /*Unlocks critical section.*/
        if(sem_post(lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Unlocks plate barrier..*/
        if(sem_post(plate)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Unlocks supplier control barrier..*/
        if(sem_post(soup_deliv)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Unlocks plate barrier..*/
        if(sem_post(main_deliv)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }        
        
        /*Unlocks plate barrier..*/
        if(sem_post(dessert_deliv)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }   
        
        /*Unlocks the counter barrier.*/
        if(sem_post(c_lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }   
        
        /*Unlocks plate counter empty size barrier.*/
        if(sem_post(counter_size)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }   

        return COOK_FINISH;
    }
    

    /*Choses minimum element for increasing three meal.*/
    if(c_count[0] != c_count[1] || c_count[1] != c_count[2] || c_count[0] != c_count[2]){
        if(c_count[0] <= c_count[1]) three_meal_flag=0;
        else three_meal_flag=1;
        if(c_count[three_meal_flag] >= c_count[2]) three_meal_flag=2;

        if( (c_count[three_meal_flag] == c_count[(three_meal_flag+1)%3]) ||
            (c_count[three_meal_flag] == c_count[(three_meal_flag+2)%3]) ) three_meal_flag=-1;
    }   

    /*Take from kitchen.*/
    k_count[chose_meal]--;
    
    /*Put to counter.*/
    c_count[chose_meal]++;

    /*If there is  3 kind of meal in the counter, this 
        lets student takes the meals.*/
    if(three_meal_flag!=-1 && chose_meal==three_meal_flag){
        if(sem_post(three_meal)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
    }

        /*Save print screen operation.*/
    if(sem_wait(print_mutex)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
        print_error(error_screen);
        return RETURN_FAIL;
    }
    /*Print placed item to terminal.*/
    if(chose_meal==0){
        /*Print screen.*/
        sprintf(print_screen,
            "Cook %d placed soup on the counter - counter items P:%d,C:%d,D:%d=%d\n",
            number,c_count[0],c_count[1],c_count[2],c_count[0]+c_count[1]+c_count[2]);
            
        if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
            sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
    }
    else if(chose_meal==1){
        /*Print screen.*/
        sprintf(print_screen,
            "Cook %d placed main course on the counter - counter items P:%d,C:%d,D:%d=%d\n",
            number,c_count[0],c_count[1],c_count[2],c_count[0]+c_count[1]+c_count[2]);
            
        if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
            sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
    }
    else{
        /*Print screen.*/
        sprintf(print_screen,
            "Cook %d placed desert on the counter - counter items P:%d,C:%d,D:%d=%d\n",
            number,c_count[0],c_count[1],c_count[2],c_count[0]+c_count[1]+c_count[2]);
            
        if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
            sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
    }

       /*Unlock print mutex.*/
    if(sem_post(print_mutex)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
        print_error(error_screen);
        return RETURN_FAIL;
    }

    /*Check if plates are finished after the execution.*/
    if(k_count[0]==0 && k_count[1]==0 && k_count[2]==0 && k_count[3]!=0){

               /*Save print screen operation.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Print screen.*/
        sprintf(print_screen,
            "Cook %d finished serving - items at kitchen: %d – going home – GOODBYE!!! \n",
            number,k_count[0]+k_count[1]+k_count[2]);
            
        if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
            sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }        


        /*Unlock print mutex.*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        c_count[5]=-1;
        /*This part opens all the barrier. It must do this because 
            if it don't do it this semaphores may cause deadlock.*/
            
        /*Unlocks critical section.*/
        if(sem_post(lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Unlocks plate barrier..*/
        if(sem_post(plate)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Unlocks supplier control barrier..*/
        if(sem_post(soup_deliv)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Unlocks plate barrier..*/
        if(sem_post(main_deliv)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }        
        
        /*Unlocks plate barrier..*/
        if(sem_post(dessert_deliv)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }   
        
        /*Unlocks the counter barrier.*/
        if(sem_post(c_lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }   
        
        /*Unlocks plate counter empty size barrier.*/
        if(sem_post(counter_size)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Add one empty place to kitchen for last student.*/
        if(sem_post(counter_plate)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }   
      
        return COOK_FINISH;
    }

    /*Unlocks critical section.*/
    if(sem_post(lock)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
        print_error(error_screen);
        return RETURN_FAIL;
    }

    /*Unlocks critical section.*/
    if(sem_post(c_lock)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
        print_error(error_screen);
        return RETURN_FAIL;
    }

      /*Add one empty place to kitchen.*/
    if(sem_post(counter_plate)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
        print_error(error_screen);
        return RETURN_FAIL;
    }

    /*Add one empty place to kitchen.*/
    if(sem_post(kitchen)==ERROR_CONTROL){
        sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
        print_error(error_screen);
        return RETURN_FAIL;
    }
    
    return CONTINUE_TAKING_PLATE;
}

/*Takes plate and puts cook to counter.*/
int cook(int number,sem_t *plate,sem_t *kitchen,sem_t *lock,int *k_count,
        sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock, sem_t *three_meal,
        sem_t *soup_deliv,sem_t *main_deliv,sem_t *dessert_deliv,sem_t *print_mutex,int *c_count){
    char cook;
    int err=1,
        k_return=0,
        check_value;

    while(TRUE){
        /*Locks critical section.*/
        if(sem_wait(lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }  
        
        /*Check if plates are finished before the execution.*/
        if( (k_count[0]!=0 || k_count[1]!=0 || k_count[2]!=0 || k_count[3]==0 )){

            /*Save print screen operation.*/
            if(sem_wait(print_mutex)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
            
            /*Print screen.*/
            sprintf(print_screen,
                "Cook %d is going to the kitchen to wait for/get a plate - kitchen items P:%d,C:%d,D:%d=%d\n",
                number,k_count[0],k_count[1],k_count[2],k_count[0]+k_count[1]+k_count[2]);
            
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }        

            if(sem_post(print_mutex)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        }

        /*Unlocks critical section.*/
        if(sem_post(lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        k_return=get_from_kitchen(number,plate,kitchen,lock,k_count,counter_size,counter_plate,
                            c_lock,three_meal,soup_deliv,main_deliv,dessert_deliv,print_mutex,c_count);
        
        if(k_return == ERROR_CONTROL){
            return RETURN_FAIL;
        } 
        else if(k_return == COOK_FINISH){
            return RETURN_SUCCESS;
        }
    }
    return RETURN_SUCCESS;
}

/*Creates cooks and cooks takes plates from kitchen and send counter.*/
int create_cooks(int cooks,sem_t *plate,sem_t *kitchen,sem_t *lock,int *k_count,
                sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock, sem_t *three_meal,
                sem_t *soup_deliv,sem_t *main_deliv, sem_t *dessert_deliv,sem_t *print_mutex,int *c_count){
    
    struct sigaction sigint_action;
    int i;
    for(i=START;i<cooks;i++){
        switch (fork()){
            case -1:
                sprintf(error_screen,"\nError while fork operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            case 0:
                
                /*Sets the signal handler for SIGINT.*/
                memset(&sigint_action,0,sizeof(sigint_action));
                sigint_action.sa_handler = &sigint_handler_childs;
                if(sigaction(SIGINT,&sigint_action,NULL)==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while sigaction operation: %s",strerror (errno));                
                    print_error(error_screen);
                    clear_memories();
                    exit(EXIT_FAILURE);
                }


                /*It closes read side of the pipe.*/
                if(close(waitpipe[READ])==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                    print_error(error_screen);
                    clear_memories();
                    exit(EXIT_FAILURE);
                }

                if(cook(i,plate,kitchen,lock,k_count,counter_size,counter_plate,
                            c_lock,three_meal,soup_deliv,main_deliv,
                            dessert_deliv,print_mutex,c_count)==ERROR_CONTROL){     
                    clear_memories();
                    /*It closes write side of the pipe so main 
                        process can continue.*/
                    if(close(waitpipe[WRITE])==ERROR_CONTROL){
                        sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                        print_error(error_screen);
                        clear_memories();
                        exit(EXIT_FAILURE);
                    }
                    exit(EXIT_FAILURE);
                }

                /*If ctrl+c ignores cleaning process it may create problem so after here
                    process ignores ctrl+c.*/
                memset(&sigint_action,0,sizeof(sigint_action));
                sigint_action.sa_handler = SIG_IGN;
                if(sigaction(SIGINT,&sigint_action,NULL)==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while sigaction operation: %s",strerror (errno));                
                    print_error(error_screen);
                    clear_memories();
                    exit(EXIT_FAILURE);
                }

                clear_memories();

                /*It closes write side of the pipe so main 
                    process can continue.*/
                if(close(waitpipe[WRITE])==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                    print_error(error_screen);
                    exit(EXIT_FAILURE);
                }

                exit(EXIT_SUCCESS); 
            default:
                break;
        }    
    }
    return 0;
}

