/*Yusuf Can Kan */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include "student.h"
#include "print_handler.h"

#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define TRUE 1
#define SEM_COUNT 17 /*Semaphore count*/
#define COOK_FINISH 1
#define CONTINUE_TAKING_PLATE 0
#define READ 0
#define WRITE 1
#define CONTAINER_SIZE 1024


/*Below 3 function runs only graduate and undergraduate case.*/
/*This function only takes foods from counter.*/
int take(int number,int *c_count,sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock,
            sem_t *three_meal,char isGraduate,int round,sem_t* mutex_table,sem_t* print_mutex){
    
        /*If there is 3 kind of meal.*/
        if(sem_wait(three_meal)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        /*Since it will take 3 kind of food from counter it decrease 3 plates from counter.*/
        if(sem_wait(counter_plate)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
       
        if(sem_wait(counter_plate)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        if(sem_wait(counter_plate)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Locks critical section*/
        if(sem_wait(c_lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        c_count[0]--;
        c_count[1]--;
        c_count[2]--;
        c_count[3]--;

        /*Saves the table variable.*/
        if(sem_wait(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*This mutex saves the print operation.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        if(isGraduate=='g'){

            /*Print screen.*/
            sprintf(print_screen,
                "Graduate student %d got food and is going to get a table (round %d) - # of empty tables: %d\n",
                number,round,empty_table[tables]);
                
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }   

        }
        else{
            /*Print screen.*/
            sprintf(print_screen,
                "Undergraduate student %d got food and is going to get a table (round %d) - # of empty tables: %d\n",
                number,round,empty_table[tables]);
            
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        }

        /*This mutex saves the print operation.*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Saves the table variable.*/
        if(sem_post(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        /*Unlocks critical section.*/
        if(sem_post(c_lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        /*Since one student took 3 kind of plate increase empty place 3 for counter.*/
        if(sem_post(counter_size)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        if(sem_post(counter_size)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        if(sem_post(counter_size)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

    return RETURN_SUCCESS;
}

/*Graduate students*/
int graduate_f(int number,int *c_count,sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock,
            sem_t *three_meal,sem_t* try_take,sem_t* secure_take, sem_t *wait_take,
            sem_t *lock_graduate,sem_t *table,sem_t *mutex_table,sem_t* print_mutex, int *empty_table){
    int i,find_table=0;
    for(i=START;i<times;i++){

        /*This semaphore makes sure while undergraduate taking plate
            no graduate student comes to line.*/
        if(sem_wait(secure_take)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Saves the graduate counter variable which is c_count[4]*/
        if(sem_wait(lock_graduate)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Make save print.*/
        if(sem_wait(c_lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Adds one person to counter.*/
        c_count[4]++;
        c_count[3]++;

        /*This mutex saves the print operation.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Print screen.*/
        sprintf(print_screen,
            "Graduate student %d is going to the counter (round %d) - # of students at counter: %d and counter items P:%d,C:%d,D:%d=%d\n",
            number,i+1,c_count[3],c_count[0],c_count[1],c_count[2],c_count[0]+c_count[1]+c_count[2]);
            
        if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
            sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        } 

        /*This mutex saves the print operation.*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*If this is first gradute to come here.*/
        if(c_count[4]==1){
            /*Make undergraduate student wait until graduate takes its meal.*/
            if(sem_wait(wait_take)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        }

        if(sem_post(c_lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        if(sem_post(lock_graduate)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        if(sem_post(secure_take)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Takes plate.*/
        if(take(number,c_count,counter_size,counter_plate,c_lock,
                three_meal,'g',i+1,mutex_table,print_mutex)==RETURN_FAIL) return RETURN_FAIL;

        /*Saves the graduate counter variable which is c_count[4]*/
        if(sem_wait(lock_graduate)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Save c_count variable.*/
        if(sem_wait(c_lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        c_count[4]--;

        if(sem_post(c_lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        /*If this is last graduate*/
        if(c_count[4]==0){
            if(sem_post(wait_take)==ERROR_CONTROL){
                sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        }

        if(sem_post(lock_graduate)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        

        /*AFTER THIS PART STUDENT GOES TO THE TABLES.*/

        /*Decrease the empty place in table.*/
        if(sem_wait(table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        /*Saves the table variable.*/
        if(sem_wait(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        find_table=0;

        /*Find empty table.*/
        while( (empty_table[find_table]!=0) && find_table<tables) find_table++;
        
        if(find_table<tables)
            empty_table[find_table]=1;

        empty_table[tables]--;

        /*This mutex saves the print operation.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Print screen.*/
        sprintf(print_screen,
            "Graduate student %d  sat at table %d to eat (round %d) - empty tables:%d\n",
            number,find_table+1,i+1,empty_table[tables]);
            
        if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
            sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Unlock the print mutex.*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Open the table lock so while this student eating 
            other student can come.*/
        if(sem_post(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*This part prints when student lefts table.*/
        if(sem_wait(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        empty_table[find_table]=0;
        empty_table[tables]++;

        /*This mutex saves the print operation.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        if(i==times-1){
            
            /*Print screen.*/
            sprintf(print_screen,
                "Graduate student %d is done eating L=%d times - going home – GOODBYE!!!\n",
                number,times);
                
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }        

        }
        else{
            /*Print screen.*/
            sprintf(print_screen,
                "Graduate student %d left table %d to eat again (round %d) - empty tables:%d\n",
                number,find_table,i+1,empty_table[tables]);
                
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }   
        }

        /*Unlock print mutex.*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        if(sem_post(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Increace empty place semaphore 1.*/
        if(sem_post(table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
    }
    return RETURN_SUCCESS;
}

/*Undergraduate students*/
int undergraduate_f(int number,int *c_count,sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock,
            sem_t *three_meal,sem_t* try_take,sem_t* secure_take, sem_t *wait_take,
            sem_t *table,sem_t *mutex_table,sem_t* print_mutex, int *empty_table){
    
    int i,find_table=0;
    for(i=START;i<times;i++){

        if(sem_wait(c_lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Adds one person to counter.*/
        c_count[3]++;

        /*This mutex saves the print operation.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

                /*Print screen.*/
        sprintf(print_screen,
            "Undergraduate student %d is going to the counter (round %d) - # of students at counter: %d and counter items P:%d,C:%d,D:%d=%d\n",
            number,i+1,c_count[3],c_count[0],c_count[1],c_count[2],c_count[0]+c_count[1]+c_count[2]);
            
        if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
            sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*This mutex saves the print operation.*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Unlocks critical section.*/
        if(sem_post(c_lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*This semapores provides students taking plate 1 by 1.*/
        if(sem_wait(try_take)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*This semaphore makes sure while or before undergraduate taking plate
            no graduate student comes to line.*/
        if(sem_wait(secure_take)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*If graduate student comes, undergradute student waits here until
            it takes its plate.*/
        if(sem_wait(wait_take)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Takes plate.*/
        if(take(number,c_count,counter_size,counter_plate,c_lock,three_meal,'u',i+1,mutex_table,print_mutex)==RETURN_FAIL) return RETURN_FAIL;

        if(sem_post(wait_take)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        if(sem_post(secure_take)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
 
        if(sem_post(try_take)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*AFTER THIS PART STUDENT GOES TO THE TABLES.*/

        /*Decrease the empty place in table.*/
        if(sem_wait(table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        /*Saves the table variable.*/
        if(sem_wait(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        find_table=0;
        while( (empty_table[find_table]!=0) && find_table<tables) find_table++;
        
        if(find_table<tables)
            empty_table[find_table]=1;
        empty_table[tables]--;

        /*This mutex saves the print operation.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Print screen.*/
        sprintf(print_screen,
            "Undergraduate student %d  sat at table %d to eat (round %d) - empty tables:%d\n",
            number,find_table+1,i+1,empty_table[tables]);
            
        if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
            sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*This mutex saves the print operation.*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Open the table lock so while this student eating 
            other student can come.*/
        if(sem_post(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*This part prints when student lefts table.*/
        if(sem_wait(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        empty_table[find_table]=0;
        empty_table[tables]++;
        
        /*This mutex saves the print operation.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        if(i==times-1){
            /*Print screen.*/
            sprintf(print_screen,
                "Undergraduate student %d is done eating L=%d times - going home – GOODBYE!!!\n",
                number,times);
                
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        }
        else{
            /*Print screen.*/
            sprintf(print_screen,
                "Undergraduate student %d left table %d to eat again (round %d) - empty tables:%d\n",
                number,find_table,i+1,empty_table[tables]);
                
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        }


        /*This mutex saves the print operation.*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }


        if(sem_post(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Increace empty place semaphore 1.*/
        if(sem_post(table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
    }

    return RETURN_SUCCESS;
}

/*This function runs only if user provides -M from command line.*/
int students_f(int number,int *c_count,sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock,
            sem_t *three_meal,sem_t *table,sem_t *mutex_table,sem_t* print_mutex, int *empty_table){
    int i=0,find_table=0;
    for(i=ZERO;i<times;i++){

        if(sem_wait(c_lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        /*Adds one person to counter.*/
        c_count[3]++;

        /*This mutex saves the print operation.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Print screen.*/
        sprintf(print_screen,
            "Student %d is going to the counter (round %d) - # of students at counter: %d and counter items P:%d,C:%d,D:%d=%d\n",
            number,i+1,c_count[3],c_count[0],c_count[1],c_count[2],c_count[0]+c_count[1]+c_count[2]);
            
        if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
            sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*This mutex saves the print operation.*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }


        /*Unlocks critical section.*/
        if(sem_post(c_lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*If there is three kind of meal.*/
        if(sem_wait(three_meal)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_wait operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
   
        /*Since it will take 3 kind of food from counter it decrease 3 plates from counter.*/
        if(sem_wait(counter_plate)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
       
        if(sem_wait(counter_plate)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        if(sem_wait(counter_plate)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Locks critical section*/
        if(sem_wait(c_lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        c_count[0]--;
        c_count[1]--;
        c_count[2]--;
        c_count[3]--;

        /*Saves the table variable.*/
        if(sem_wait(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*This mutex saves the print operation.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Print screen.*/
        sprintf(print_screen,
            "Student %d got food and is going to get a table (round %d) - # of empty tables: %d\n",
            number,i+1,empty_table[tables]);
            
        if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
            sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }


        /*Unlock print mutex.*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Saves the table variable.*/
        if(sem_post(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Unlocks critical section.*/
        if(sem_post(c_lock)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Since one student took 3 kind of plate increase empty place 3 for counter.*/
        if(sem_post(counter_size)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        if(sem_post(counter_size)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        if(sem_post(counter_size)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*AFTER THIS PART STUDENT GOES TO THE TABLES.*/

        /*Decrease the empty place in table.*/
        if(sem_wait(table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        /*Saves the table variable.*/
        if(sem_wait(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
        
        find_table=0;
        while( (empty_table[find_table]!=0) && find_table<tables) find_table++;
        
        if(find_table<tables)
            empty_table[find_table]=1;
        empty_table[tables]--;

        /*This mutex saves the print operation.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Print screen.*/
        sprintf(print_screen,
            "Student %d  sat at table %d to eat (round %d) - empty tables:%d\n",
            number,find_table+1,i+1,empty_table[tables]);
            
        if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
            sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }


        /*This mutex saves the print operation.*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Open the table lock so while this student eating 
            other student can come.*/
        if(sem_post(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        

        /*This part prints when student lefts table.*/
        if(sem_wait(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        empty_table[find_table]=0;
        empty_table[tables]++;

        /*This mutex saves the print operation.*/
        if(sem_wait(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }


        if(i==times-1){
            /*Print screen.*/
            sprintf(print_screen,
                "Student %d is done eating L=%d times - going home – GOODBYE!!!\n",
                number,times);
                
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        }
        else{
            /*Print screen.*/
            sprintf(print_screen,
                "Student %d left table %d to eat again (round %d) - empty tables:%d\n",
                number,find_table,i+1,empty_table[tables]);
                
            if((write(STDOUT_FILENO,print_screen,strlen(print_screen)))==ERROR_CONTROL){
                sprintf(error_screen,"\nError while printing screen: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            }
        }


        /*This mutex saves the print operation.*/
        if(sem_post(print_mutex)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }


        if(sem_post(mutex_table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }

        /*Increace empty place semaphore 1.*/
        if(sem_post(table)==ERROR_CONTROL){
            sprintf(error_screen,"\nError while sem_post operation: %s",strerror (errno));                
            print_error(error_screen);
            return RETURN_FAIL;
        }
    }
    return RETURN_SUCCESS;
}

/*This function creates both graduate and undergraduate students.*/
int create_studets(int *c_count,sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock,
                    sem_t *three_meal,sem_t* try_take,sem_t* secure_take, sem_t *wait_take,sem_t *lock_graduate,
                    sem_t *table,sem_t *mutex_table,sem_t* print_mutex, int *empty_table){

    int i=0;
    struct sigaction sigint_action;
    /*If parameter give as graduate and undergraduate it creates this students seperately.*/
    if(graduate!=0){
        /*Create undergraduate students.*/
        for(i=START;i<undergraduate;i++){
            switch (fork()){
            case -1:
                sprintf(error_screen,"\nError while fork operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            case 0:

                /*Sets the signal handler for SIGINT.*/
                memset(&sigint_action,0,sizeof(sigint_action));
                sigint_action.sa_handler = &sigint_handler_childs;
                if(sigaction(SIGINT,&sigint_action,NULL)==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                    print_error(error_screen);
                    clear_memories();
                    exit(EXIT_FAILURE);
                }

                /*It closes read side of the pipe.*/
                if(close(waitpipe[READ])==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                    print_error(error_screen);
                    clear_memories();
                    exit(EXIT_FAILURE);
                }

                if(graduate_f(i,c_count,counter_size,counter_plate,c_lock,three_meal, try_take,secure_take,
                    wait_take,lock_graduate,table,mutex_table,print_mutex,empty_table)==ERROR_CONTROL){
                    clear_memories();
                    exit(EXIT_FAILURE);
                }

                /*If ctrl+c comes while cleaning process it may create problem so after here
                    process ignores ctrl+c.*/
                memset(&sigint_action,0,sizeof(sigint_action));
                sigint_action.sa_handler = SIG_IGN;
                if(sigaction(SIGINT,&sigint_action,NULL)==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                    print_error(error_screen);
                    clear_memories();
                    exit(EXIT_FAILURE);
                }
                    
                clear_memories();

                /*It closes write side of the pipe so main 
                    process can continue.*/
                if(close(waitpipe[WRITE])==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                    print_error(error_screen);
                    exit(EXIT_FAILURE);
                }

                exit(EXIT_SUCCESS);
            default:
                break;
            }
        }

        /*Create graduate students*/
        for(i=START;i<graduate;i++){
            switch (fork()){
            case -1:
                sprintf(error_screen,"\nError while fork operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            case 0:
               
                /*Sets the signal handler for SIGINT.*/
                memset(&sigint_action,0,sizeof(sigint_action));
                sigint_action.sa_handler = &sigint_handler_childs;
                if(sigaction(SIGINT,&sigint_action,NULL)==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                    print_error(error_screen);
                    clear_memories();
                    exit(EXIT_FAILURE);
                }
                /*It closes read side of the pipe.*/
                if(close(waitpipe[READ])==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                    print_error(error_screen);
                    return RETURN_FAIL;
                }

                if(undergraduate_f(i,c_count,counter_size,counter_plate,c_lock,three_meal,
                        try_take,secure_take,wait_take,table,mutex_table,print_mutex,empty_table)==ERROR_CONTROL){
                    exit(EXIT_SUCCESS);
                }

                /*If ctrl+c comes while cleaning process it may create problem so after here
                    process ignores ctrl+c.*/
                memset(&sigint_action,0,sizeof(sigint_action));
                sigint_action.sa_handler = SIG_IGN;
                if(sigaction(SIGINT,&sigint_action,NULL)==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                    print_error(error_screen);
                    clear_memories();
                    exit(EXIT_FAILURE);
                }

                clear_memories();

                /*It closes write side of the pipe so main 
                    process can continue.*/
                if(close(waitpipe[WRITE])==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                    print_error(error_screen);
                    return RETURN_FAIL;
                }

                exit(EXIT_SUCCESS);
            default:
                break;
            }
        }
    }
    else{
        for(i=START;i<students;i++){
            switch (fork()){
            case -1:
                sprintf(error_screen,"\nError while fork operation: %s",strerror (errno));                
                print_error(error_screen);
                return RETURN_FAIL;
            case 0:

                /*Sets the signal handler for SIGINT.*/
                memset(&sigint_action,0,sizeof(sigint_action));
                sigint_action.sa_handler = &sigint_handler_childs;
                if(sigaction(SIGINT,&sigint_action,NULL)==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                    print_error(error_screen);
                     clear_memories();
                    return RETURN_FAIL;
                }

                /*It closes read side of the pipe.*/
                if(close(waitpipe[READ])==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                    print_error(error_screen);
                    return RETURN_FAIL;
                }

                if(students_f(i,c_count,counter_size,counter_plate,c_lock,three_meal,
                                table,mutex_table,print_mutex,empty_table)==ERROR_CONTROL){
                    exit(EXIT_SUCCESS);
                }

                /*If ctrl+c comes while cleaning process it may create problem so after here
                    process ignores ctrl+c.*/
                memset(&sigint_action,0,sizeof(sigint_action));
                sigint_action.sa_handler = SIG_IGN;
                if(sigaction(SIGINT,&sigint_action,NULL)==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                    print_error(error_screen);
                     clear_memories();
                    return RETURN_FAIL;
                }

                clear_memories();
                
                /*It closes write side of the pipe so main 
                    process can continue.*/
                if(close(waitpipe[WRITE])==ERROR_CONTROL){
                    sprintf(error_screen,"\nError while closing pipe: %s",strerror (errno));                
                    print_error(error_screen);
                    return RETURN_FAIL;
                }

                exit(EXIT_SUCCESS);
            default:
                break;
            }
        }
    }
    return RETURN_SUCCESS;
}
