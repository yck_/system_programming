#ifndef KITCHEN_H
#define KITCHEN_H


/*It takes one plate form kitchen.*/
int get_from_kitchen(int number,sem_t *plate,sem_t *kitchen,sem_t *lock,int *k_count,
                    sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock, sem_t *three_meal
                    ,sem_t *soup_deliv,sem_t *main_deliv,sem_t *dessert_deliv,sem_t *print_mutex,int *c_count);

/*Takes plate and puts cook to counter.*/
int cook(int number,sem_t *plate,sem_t *kitchen,sem_t *lock,int *k_count,
        sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock, sem_t *three_meal,
        sem_t *soup_deliv,sem_t *main_deliv,sem_t *dessert_deliv,sem_t *print_mutex,int *c_count);


/*Creates cooks and cooks takes plates from kitchen and send counter.*/
int create_cooks(int cooks,sem_t *plate,sem_t *kitchen,sem_t *lock,int *k_count,
                sem_t *counter_size,sem_t *counter_plate,sem_t *c_lock,sem_t *three_meal
                ,sem_t *soup_deliv,sem_t *main_deliv,sem_t *dessert_deliv,sem_t *print_mutex,int *c_count);



#endif
