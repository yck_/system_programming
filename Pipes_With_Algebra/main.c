/*Yusuf Can Kan*/

/*

	This program takes 2 matrix, product them and find single values and prints on screen.
	It takes 2^n x 2^n character from each of given files and converts them into ascii equivalent 
	and builds two 2^n x 2^n square matrix. Program takes input files and n variable with using
	command line argument;

	./161044007 -i inputPathA -j inputPathB -n positiveNumber

	It prints all the positive values to screen.


	Note:
		Single value calcultion code taken from online.

*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/wait.h>
#include "pipes.h"
#include "svd.c" /*Include for singular value calculations.*/





/*Macros*/
#define ONE 1
#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define PLUSONE 1
#define TRUE 1
#define NUMBER_OF_CHILD 4
#define READ 0
#define WRITE 1
#define NOT_SUFFICIENT_CHARACTER 0
#define CONTAINTER_SIZE 1024



/*SIGNITURES*/

/*Parsing the command line arguments.*/
void io_files(int argc, char* argv[], char** file1, char** file2,int *n);


/*Rads the files and fills the matrix.
	This function using in create_matrix function.*/
void readfiles(unsigned char **matrixA,unsigned char **matrixB,int fd1,int fd2,int line_length);

/*It reads the files and fills the matrix with 2^n x 2^n form.*/
void create_matrix(unsigned char **matrixA,unsigned char **matrixB,int n,char* file1, char* file2);

/*Takes matrix1 and matrix2 calculates result matrix*/
void calculate_matrix(int **matrix1,int **matrix2,int ***result,int line_length);

/*It creates matrices for calculation with respect to given n.*/
void create_matrices(int ***matrix1,int ***matrix2,int ***result,int line_length);

/*This function reads the values from pipe and sets the matrices.*/
void read_matrices(int pipe_read,int ***matrix1,int ***matrix2,int line_length);

/*This function writes result matrix to the write pipe.*/
void write_result_matrix(int pipe_write,int **result,int line_length);

/*Function for forked child.*/
void fork_child(int pipes[2],int line_length,int blockpipe);





/*This handler handles the SIGCHLD signal synchronously.*/
void sigchld_handler(int signal_number);

/*In case of CTRL + C, this handler invoke and free all 
	the resources and exits gracefully.*/
void sigint_handler(int signal_number);

/*Prints the singular values to screen with using 
	STDOUT, write and sprintf functions.*/
void print_result(float *result);








/*In main, all the childs creating, block and sigaction opetations are 
	setting and parent block operation is happening.*/
int main(int argc,char *argv[]){
	char *file1;
	char *file2;
    int n, /*n and line length for calculations.*/
        i,j,
		dummy;			/*Dummy value for synchronization barrier.*/
	int pipes[4][2], /*Holds pipes for every child.*/
		block_pipe[2]; /*This pipe for synchronization barrier. */

	float **dummy1; /*This variables will be using while calculating singular values of result matrix.*/

	struct sigaction sigchld_action,sigint_action; /*For handling SIGCHLD.*/

	/*Sets the signal handler for SIGCHLD.*/
	memset(&sigchld_action,0,sizeof(sigchld_action));
	sigchld_action.sa_handler = &sigchld_handler;
	sigchld_action.sa_flags=SA_RESTART;
	sigaction(SIGCHLD,&sigchld_action,NULL);

	/*Sets the signal handler for SIGINT.*/
	memset(&sigint_action,0,sizeof(sigint_action));
	sigint_action.sa_handler = &sigint_handler;
	sigaction(SIGINT,&sigint_action,NULL);

    /*Parsing command line argument.*/
    io_files(argc,argv,&file1,&file2,&n);
    line_length=pow(2,n);

	/*Create matrices*/
    matrixA = (unsigned char**)malloc(line_length*sizeof(unsigned char*));
    matrixB = (unsigned char**)malloc(line_length*sizeof(unsigned char*));
	result = (int**)malloc(line_length*sizeof(int*));
	result_matrix = (float**)malloc(line_length*sizeof(float*));
	singular_values=(float*)malloc(line_length*sizeof(float));
    for(i=START;i<line_length;i++){
        matrixA[i]=(unsigned char*)malloc(line_length*sizeof(unsigned char));
        matrixB[i]=(unsigned char*)malloc(line_length*sizeof(unsigned char));
		result[i]=(int*)malloc(line_length*sizeof(int));
		result_matrix[i]=(float*)malloc(line_length*sizeof(float));
    }

	/*Read the file and write to matrices.*/
    create_matrix(matrixA,matrixB,n,file1,file2);

	/*Creates the block pipe for synchronization barrier.*/
	if(pipe(block_pipe)==ERROR_CONTROL){
		/*Free all memories.*/
		free_memories();		
		fprintf (stderr, "\nError while fork operation.");
		exit(EXIT_FAILURE);
	}
	
	/*Creating childs.*/
	for(i=START;i<NUMBER_OF_CHILD;i++){
		pipe(pipes[i]);
		switch (fork()){
		case -1:
			free_memories();
			fprintf (stderr, "\nError while fork operation.");
			exit(EXIT_FAILURE);
			break;
		case 0:
			if(close(block_pipe[READ])==ERROR_CONTROL){
				free_memories();
				fprintf (stderr, "\nError while close operation.");
				exit(EXIT_FAILURE);
			}
			/*Closes unnecessary pipes. */
			for(j=0;j<i;j++){
				if(close(pipes[j][READ])==ERROR_CONTROL){
					fprintf (stderr, "\nError while close operation.");
					_exit(EXIT_FAILURE);
				}
				if(close(pipes[j][WRITE])==ERROR_CONTROL){
					fprintf (stderr, "\nError while close operation.");
					_exit(EXIT_FAILURE);
				}
			}
			fork_child(pipes[i],line_length,block_pipe[WRITE]);
			
			if(close(pipes[i][WRITE])==ERROR_CONTROL){
				fprintf (stderr, "\nError while close operation.");
				_exit(EXIT_FAILURE);
			}
			if(close(pipes[i][READ])==-1){
				fprintf (stderr, "\nError while close operation.");
				_exit(EXIT_FAILURE);
			}
			_exit(EXIT_SUCCESS);
		default:
			break;
		}
	}
	/*Close unnessesary block pipe.*/
	if(close(block_pipe[WRITE])==ERROR_CONTROL){
		free_memories();
		exit(EXIT_FAILURE);
	}

	/*Sends the matrices using pipe.*/
	write_pipe(pipes,line_length,matrixA,matrixB);

	/*It blocks the parent until all childs finish calcucations.*/
	if((read(block_pipe[READ],&dummy,1)) != 0)
		fprintf (stderr, "\nRead didin't get eof.");

	/*Close the unused block pipe.*/
	if(close(block_pipe[READ])==ERROR_CONTROL){
		free_memories();
		exit(EXIT_FAILURE);
	}

	/*Reads the result of each child and form it.*/
	read_pipe(pipes,line_length,&result);

	/*Create matrices for singular value calculations.
		After calculations this matrices directly be free.*/
    dummy1 = (float**)malloc(line_length*sizeof(float*));
    for(i=START;i<line_length;i++)
        dummy1[i]=(float*)malloc(line_length*sizeof(float));

	for(i=START;i<line_length;i++)
		for(j=START;j<line_length;j++)
			result_matrix[i][j]=(float)(result[i][j]);

	/*Sinhular value calculations.*/
	dsvd(result_matrix,line_length,line_length, singular_values, dummy1);
	
	print_result(singular_values);

	for(i=START;i<line_length;i++)
		free(dummy1[i]);
	free(dummy1);

	/*Close all the pipes.*/
	for(i=0;i<4;i++){
		close(pipes[i][READ]);
		close(pipes[i][WRITE]);
	}

	/*Free all the memories.*/
	free_memories();
	
	return 0;
}

/*Prints the singular values to screen.*/
void print_result(float *result){
	int i,err;
	char writescreen[CONTAINTER_SIZE];

	err=write(STDOUT_FILENO,"Singular Values;\n",17);
	if(err==ERROR_CONTROL){
		fprintf (stderr, "\n1Failed to print screen.");
		exit(EXIT_FAILURE);
	}

	for(i=START;i<line_length;i++){
		sprintf(writescreen,"%.3f \n",result[i]);
		err=write(STDOUT_FILENO,writescreen,strlen(writescreen));
		if(err==ERROR_CONTROL){
			fprintf (stderr, "\nFailed to print screen.");
			exit(EXIT_FAILURE);
		}
	}

	if(write(STDOUT_FILENO,"\n",2)==ERROR_CONTROL){
		fprintf (stderr, "\nFailed to print screen.");
		exit(EXIT_FAILURE);
	}
}

/*Parsing the command line arguments.*/
void io_files(int argc, char* argv[], char** file1, char** file2,int *n){
	int opt,file_flag=0;
	
	while((opt = getopt(argc, argv, "i:j:n:")) !=-1 ){
		switch(opt){
			case 'i':
					*file1=optarg;
					file_flag++;
					break;
			case 'j':
					*file2=optarg;
					file_flag++;
					break;
            case 'n':
                    (*n)=atoi(optarg);
					if((*n)<=0){
						fprintf (stderr, "\nPlease provide positive integer for n.\n");
						exit(EXIT_FAILURE);
					}
                    break;

            default:
					fprintf (stderr, "\nPlease provide proper input: ./161044007 -i inputPath -j outputPath -n number\n");
					exit(EXIT_FAILURE);
		}
	}
	if(file_flag<2){
		fprintf (stderr, "\nPlease provide proper input: ./161044007 -i inputPath -j outputPath -n number\n");
		exit(EXIT_FAILURE);
	}
}

/*Rads the files and fills the matrix.
	This function using in create_matrix function.*/
void readfiles(unsigned char **matrixA,unsigned char **matrixB,int fd1,int fd2,int line_length){
	int i,
		bytes_read;

	for(i=START;i<line_length;i++){
		bytes_read=read(fd1,matrixA[i],line_length);

		if(bytes_read==ERROR_CONTROL){
			free_memories();
			close(fd1);
			close(fd2);
			fprintf (stderr, "\nReading error.\n");
			exit(EXIT_FAILURE);
		}
		else if(bytes_read==NOT_SUFFICIENT_CHARACTER){
			free_memories();
			close(fd1);
			close(fd2);
			fprintf (stderr, "\nNot enough character in inputPathA.\n");
			exit(EXIT_FAILURE);
		}

		bytes_read=read(fd2,matrixB[i],line_length);
		if(bytes_read==ERROR_CONTROL){
			free_memories();
			close(fd1);
			close(fd2);
			fprintf (stderr, "\nReading error.\n");
			exit(EXIT_FAILURE);
		}
		else if(bytes_read==NOT_SUFFICIENT_CHARACTER){
			free_memories();
			close(fd1);
			close(fd2);
			fprintf (stderr, "\nNot enough character in inputPathB.\n");
			exit(EXIT_FAILURE);
		}
	}	
}

/*It reads the files and fills the matrix with 2^n x 2^n form.*/
void create_matrix(unsigned char **matrixA,unsigned char **matrixB,int n,char* file1, char* file2){
	int line_length, /*For calculatin length of row for matrix.*/
		fd1,fd2; /*File descriptors for file1 and file2.*/
		
	line_length=pow(2,n);
	
	fd1=open(file1,O_RDONLY);
	if(fd1==ERROR_CONTROL){
		free_memories();
		fprintf (stderr,"\nFile couldn't open\nerrno = %d: %s \n",errno,strerror (errno));
		exit(EXIT_FAILURE);
	}
	
	fd2=open(file2,O_RDONLY);
	if(fd2==ERROR_CONTROL){
		close(fd1);
		free_memories();
		fprintf (stderr,"\nFile couldn't open\nerrno = %d: %s \n",errno,strerror (errno));
		exit(EXIT_FAILURE);
	}

	/*Reads the files and fills the matrixA and matrix B.*/
	readfiles(matrixA,matrixB,fd1,fd2,line_length);

	if(close(fd1)==ERROR_CONTROL){

		/*Free all the memories.*/
		free_memories();
		fprintf (stderr, "\nerrno = %d: %s \n",errno, strerror (errno));
		exit(EXIT_FAILURE);
	}
	if(close(fd2)==ERROR_CONTROL){
		/*Free all the memories.*/
		free_memories();

		fprintf (stderr, "\nerrno = %d: %s \n",errno, strerror (errno));
		exit(EXIT_FAILURE);
	}	

}

/*Takes matrix1 and matrix2 calculates result matrix*/
void calculate_matrix(int **matrix1,int **matrix2,int ***result,int line_length){
	int i,j,k;
	/*Matrix calculation.*/
	for(i=0;i<line_length/2;i++)
		for(j=0;j<line_length/2;j++)
			for(k=0;k<line_length;k++)
				(*result)[i][j]+=matrix1[i][k]*matrix2[k][j];
}

/*It creates matrices for calculation with respect to given n.*/
void create_matrices(int ***matrix1,int ***matrix2,int ***result,int line_length){
	int i;
	(*matrix1)=(int**)malloc( (line_length/2)*sizeof(int*) );
	for(i=0;i<line_length/2;i++)
		(*matrix1)[i]=(int*)malloc( (line_length)*sizeof(int) );

	(*matrix2)=(int**)malloc( (line_length)*sizeof(int*) );
	for(i=0;i<line_length;i++)
		(*matrix2)[i]=(int*)malloc( (line_length/2)*sizeof(int) );

	(*result)=(int**)malloc( (line_length/2)*sizeof(int*) );
	for(i=0;i<line_length/2;i++)
		(*result)[i]=(int*)malloc( (line_length/2)*sizeof(int) );	
}

/*This function reads the values from pipe and sets the matrices.*/
void read_matrices(int pipe_read,int ***matrix1,int ***matrix2,int line_length){
	int i,j,
		convert=ZERO,/*This variable for converting char charcter to int.*/
		error=ZERO;

	/*This loop reads the left matrix.*/
	for(i=START;i<line_length/2;i++){
		for(j=START;j<line_length;j++){
			error=read(pipe_read,&convert,1);
			if(error==ERROR_CONTROL){
				/*Free all allocated memory.*/
				for(i=0;i<line_length/2;i++)
						free((*matrix1)[i]);
				for(i=0;i<line_length;i++)
						free((*matrix2)[i]);
				free((*matrix1));
				free((*matrix2));
				close(pipe_read);

				fprintf (stderr,"\nFailded to read pipe!\n");	
				exit(EXIT_FAILURE);
			}
			(*matrix1)[i][j]=convert;
			convert=ZERO;
		}
	}
	convert=ZERO;
	/*This loop reads the right matrix.*/
	for(i=START;i<line_length;i++){
		for(j=START;j<line_length/2;j++){
			error=read(pipe_read,&convert,1);
			if(error==ERROR_CONTROL){
				/*Free all allocated memory.*/
				for(i=0;i<line_length/2;i++)
					free((*matrix1)[i]);
				for(i=0;i<line_length;i++)
					free((*matrix2)[i]);
				free((*matrix1));
				free((*matrix2));
				close(pipe_read);
				fprintf (stderr,"\nFailded to read pipe!\n");	
				exit(EXIT_FAILURE);
			}
			(*matrix2)[i][j]=convert;
			convert=ZERO;
		}
	}
}	

/*This function writes result matrix to the write pipe.*/
void write_result_matrix(int pipe_write,int **result,int line_length){
	int i,j,error;

	for(i=0;i<line_length/2;i++){
		for(j=0;j<line_length/2;j++){
			error=write(pipe_write,&(result[i][j]),sizeof(result[i][j]));
			if(error==ERROR_CONTROL){
				/*Free all allocated memory.*/
				for(i=0;i<line_length/2;i++)
						free((result)[i]);
				free(result);

				fprintf (stderr,"\nFailded to read pipe!\n");	
				exit(EXIT_FAILURE);
			}
		}
	}
}

/*Function for forked child.*/
void fork_child(int pipes[2],int line_length,int blockpipe){
	int **matrix1,**matrix2,**result,i;
	create_matrices(&matrix1,&matrix2,&result,line_length);
	read_matrices(pipes[READ],&matrix1,&matrix2,line_length);
	calculate_matrix(matrix1,matrix2,&result,line_length);

	/*It opens parent block.*/
	if(close(blockpipe)==ERROR_CONTROL){
		for(i=0;i<line_length/2;i++){
			free((matrix1)[i]);
			free((result)[i]);
		}
		for(i=0;i<line_length;i++)
			free((matrix2)[i]);
		free(matrix1);
		free(matrix2);
		free(result);

		fprintf (stderr, "\nError while closing write pipe.");
		exit(EXIT_FAILURE);
	}
	/*Send C matrix to parent via pipe.*/
	write_result_matrix(pipes[WRITE],result,line_length);

	for(i=0;i<line_length/2;i++){
		free((matrix1)[i]);
		free((result)[i]);
	}
	for(i=0;i<line_length;i++)
		free((matrix2)[i]);
	free(matrix1);
	free(matrix2);
	free(result);
	
}


/*This handler handles the SIGCHLD signal synchronously.*/
void sigchld_handler(int signal_number){
	int error;
	/*This loops executes wait to ecah child until there is no child exist. 
		In this way when main program recieves SIGCHILD first time whis process 
		will start. */
	while((error=waitpid(-1,NULL,WNOHANG))>0)
		if(error==ERROR_CONTROL)
			fprintf (stderr, "\nError on wait opetation.");
}

/*In case of CTRL + C, this handler invoke and free all 
	the resources and exits gracefully.*/
void sigint_handler(int signal_number){
	/*free memories.*/
	free_memories();
	fprintf (stderr,"Process %d: exited gracefully \n",getpid());
	exit(EXIT_SUCCESS);
}

