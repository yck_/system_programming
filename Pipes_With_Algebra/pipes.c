/*Yusuf Can Kan*/

/*This file includes variables and read-write pipe operations.*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/wait.h>

/*Macros*/
#define ONE 1
#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define PLUSONE 1
#define TRUE 1
#define NUMBER_OF_CHILD 4
#define READ 0
#define WRITE 1
#define NOT_SUFFICIENT_CHARACTER 0
#define CONTAINTER_SIZE 1024

/*Global variables.*/
unsigned char **matrixA, **matrixB;/*Matrix A and B*/
int  **result; /*Result matrix*/
float **result_matrix; /*In order to calculate singular values we need float matrix.
							 When we convert to result matrix we will store in this matrix.*/
int line_length; /*This is using for free allocating memories.*/
float *singular_values; /*For storing singular values.*/

/*This function, using for free operation for matrices.*/
void free_matrix(unsigned char **matrix,int line_length){
	int i=0;
	for(i=START;i<line_length;i++){
		free(matrix[i]);
	}
	free(matrix);
}

/*This function frees all memory before all exits.*/
void free_memories(){
	int i;
	free_matrix(matrixA,line_length);
	free_matrix(matrixB,line_length);

	for(i=START;i<line_length;i++){
		free(result[i]);
		free(result_matrix[i]);
	}
	free(result);
	free(result_matrix);
	free(singular_values);
}


/*This function send quarters of martixA and matrixB
	to relevant pipe for calculation. */
void write_pipe(int pipes[4][2],int line_length,unsigned char **matrixA,unsigned char **matrixB){
	int i,j,
		error;
	for(i=START;i<line_length/2;i++){
		for(j=START;j<line_length;j++){
			error=write(pipes[0][WRITE],&(matrixA[i][j]),1);
			/*Error check*/
			if(error == ERROR_CONTROL){
				free_matrix(matrixA,line_length); /*Free matrixA and matrix B*/
				free_matrix(matrixB,line_length);
			
				for(i=0;i<4;i++) 	/*Close the pipe.*/
					for(j=0;j<2;j++)
						close(pipes[i][j]);

				fprintf (stderr, "\nFailed to write file.");
				exit(EXIT_FAILURE);
			}

			error=write(pipes[1][WRITE],&(matrixA[i][j]),1);
			/*Error check*/
			if(error == ERROR_CONTROL){
				free_matrix(matrixA,line_length); /*Free matrixA and matrix B*/
				free_matrix(matrixB,line_length);
			
				for(i=0;i<4;i++) 	/*Close the pipe.*/
					for(j=0;j<2;j++)
						close(pipes[i][j]);
				fprintf (stderr, "\nFailed to write file.");
				exit(EXIT_FAILURE);
			}

			error=write(pipes[2][WRITE],&(matrixA[(line_length/2)+i][j]),1);
			/*Error check*/
			if(error == ERROR_CONTROL){
				free_matrix(matrixA,line_length); /*Free matrixA and matrix B*/
				free_matrix(matrixB,line_length);
			
				for(i=0;i<4;i++) 	/*Close the pipe.*/
					for(j=0;j<2;j++)
						close(pipes[i][j]);

				fprintf (stderr, "\nFailed to write file.");
				exit(EXIT_FAILURE);
			}

			error=write(pipes[3][WRITE],&(matrixA[(line_length/2)+i][j]),1);
			/*Error check*/
			if(error == ERROR_CONTROL){
				free_matrix(matrixA,line_length); /*Free matrixA and matrix B*/
				free_matrix(matrixB,line_length);
			
				for(i=0;i<4;i++) 	/*Close the pipe.*/
					for(j=0;j<2;j++)
						close(pipes[i][j]);

				fprintf (stderr, "\nFailed to write file.");
				exit(EXIT_FAILURE);
			}
		}
	}

	for(i=0;i<line_length;i++){
		for(j=0;j<line_length/2;j++){

			error=write(pipes[0][1],&(matrixB[i][j]),1);
			/*Error check*/
			if(error == ERROR_CONTROL){
				free_matrix(matrixA,line_length); /*Free matrixA and matrix B*/
				free_matrix(matrixB,line_length);
			
				for(i=0;i<4;i++) 	/*Close the pipe.*/
					for(j=0;j<2;j++)
						close(pipes[i][j]);

				fprintf (stderr, "\nFailed to write file.");
				exit(EXIT_FAILURE);
			}

			error=write(pipes[1][1],&(matrixB[i][(line_length/2)+j]),1);
			/*Error check*/
			if(error == ERROR_CONTROL){
				free_matrix(matrixA,line_length); /*Free matrixA and matrix B*/
				free_matrix(matrixB,line_length);
			
				for(i=0;i<4;i++) 	/*Close the pipe.*/
					for(j=0;j<2;j++)
						close(pipes[i][j]);

				fprintf (stderr, "\nFailed to write file.");
				exit(EXIT_FAILURE);
			}

			error=write(pipes[2][1],&(matrixB[i][j]),1);
			/*Error check*/
			if(error == ERROR_CONTROL){
				free_matrix(matrixA,line_length); /*Free matrixA and matrix B*/
				free_matrix(matrixB,line_length);
			
				for(i=0;i<4;i++) 	/*Close the pipe.*/
					for(j=0;j<2;j++)
						close(pipes[i][j]);

				fprintf (stderr, "\nFailed to write file.");
				exit(EXIT_FAILURE);
			}

			error=write(pipes[3][1],&(matrixB[i][(line_length/2)+j]),1);
			/*Error check*/
			if(error == ERROR_CONTROL){
				free_matrix(matrixA,line_length); /*Free matrixA and matrix B*/
				free_matrix(matrixB,line_length);
			
				for(i=0;i<4;i++) 	/*Close the pipe.*/
					for(j=0;j<2;j++)
						close(pipes[i][j]);

				fprintf (stderr, "\nFailed to write file.");
				exit(EXIT_FAILURE);
			}

		}

	}
}

/*Reads pipes and forms everything in result matrix.*/
void read_pipe(int pipes[4][2],int line_length,int ***result){
	int i,j
		,intvalues=0 /*Takes reference from read. Assignes value to result array.*/
		,error=0;

	for(i=0;i<line_length/2;i++){
		for(j=0;j<line_length/2;j++){
			
			error=read(pipes[0][0],&((*result)[i][j]),sizeof( ((*result)[i][j]) ) );
			if(error==ERROR_CONTROL){
				/*Free all allocated memory.*/
				free_memories();
	
				for(i=0;i<4;i++){
					close(pipes[i][0]);
					close(pipes[i][1]);
				}
				fprintf (stderr,"\nFailded to read pipe!\n");	
				exit(EXIT_FAILURE);
			}

			error=read(pipes[1][0],&((*result)[i][j+(line_length/2)]),
				sizeof(((*result)[i][j+(line_length/2)])));
			if(error==ERROR_CONTROL){
				/*Free all allocated memory.*/
				free_memories();
				for(i=0;i<4;i++){
					close(pipes[i][0]);
					close(pipes[i][1]);
				}
				fprintf (stderr,"\nFailded to read pipe!\n");	
				exit(EXIT_FAILURE);
			}

			error=read(pipes[2][0],&((*result)[i+(line_length/2)][j]),
				sizeof((*result)[i+(line_length/2)][j]));
			if(error==ERROR_CONTROL){
				/*Free all allocated memory.*/
				free_memories();
				for(i=0;i<4;i++){
					close(pipes[i][0]);
					close(pipes[i][1]);
				}
				fprintf (stderr,"\nFailded to read pipe!\n");	
				exit(EXIT_FAILURE);
			}
			
			error=read(pipes[3][0],&((*result)[i+(line_length/2)][j+(line_length/2)]),
				sizeof(((*result)[i+(line_length/2)][j+(line_length/2)])));
			if(error==ERROR_CONTROL){
				/*Free all allocated memory.*/
				free_memories();
				for(i=0;i<4;i++){
					close(pipes[i][0]);
					close(pipes[i][1]);
				}
				fprintf (stderr,"\nFailded to read pipe!\n");	
				exit(EXIT_FAILURE);
			}
		}
	}
}
