/*Yusuf Can Kan*/

/*This file includes variables and read-write pipe operations.*/

#ifndef PIPES_H
#define PIPES_H

/*Global variables.*/
extern unsigned char **matrixA, **matrixB;/*Matrix A and B*/
extern int  **result; /*Result matrix*/
extern float **result_matrix; /*In order to calculate singular values we need float matrix.
							 When we convert to result matrix we will store in this matrix.*/
extern int line_length; /*This is using for free allocating memories.*/
extern float *singular_values; /*For storing singular values.*/


/*This function, using for free operation for matrices.*/
void free_matrix(unsigned char **matrix,int line_length);

/*This function frees all memory before all exits.*/
void free_memories();

/*This function send quarters of martixA and matrixB
	to relevant pipe for calculation. */
void write_pipe(int pipes[4][2],int line_length,unsigned char **matrixA,unsigned char **matrixB);

/*Reads pipes and forms everything in result matrix.*/
void read_pipe(int pipes[4][2],int line_length,int ***result);

#endif
