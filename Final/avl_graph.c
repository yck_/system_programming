/*This file includes AVL tree
    for storing graphs.*/

#include<stdio.h> 
#include<stdlib.h>
#include "avl_graph.h"

struct LinkedList;
struct List_Node;
struct AVLNode;
struct AVLVisited;
struct Queue;

/*-----------------------FOR GRAPH AVLTREE-----------------------*/

int getMax(int x,int y){
    return(x>y)? x : y;
}

/*Returns height of AVL Tree.*/
int height(struct AVLNode *n){
    if(n==NULL) return 0;
    else 
        return n->height;
}

/*Creates new node and returns it.*/
struct AVLNode* createNode(int value,struct LinkedList *dest){
    struct AVLNode *newNode=(struct AVLNode*)malloc(sizeof(struct AVLNode));
    newNode->value=value;
    newNode->right=NULL;
    newNode->left=NULL;
    newNode->dest=dest;
    newNode->height=1;
    return newNode;
}

/*Right Rotate*/
struct AVLNode* lRotate(struct AVLNode* node){
    struct AVLNode* node1=node->right;
    struct AVLNode* node2=node1->left;

    node1->left=node;
    node->right=node2;

    /*Update height*/
    node->height=getMax(height(node->left),height(node->right))+1;
    node1->height=getMax(height(node1->left),height(node1->right))+1;

    return node1;
}

/*Left rotate.*/
struct AVLNode* rRotate(struct AVLNode* node){

    struct AVLNode* node1=node->left;
    struct AVLNode* node2=node1->right;

    node1->right=node;
    node->left=node2;

    node->height=getMax(height(node->left),height(node->right))+1;
    node1->height=getMax(height(node1->left),height(node1->right))+1;

    return node1;
}

/*returns balance value of node.*/
int balance(struct AVLNode* node){
    if(node==NULL) return 0;
    return (height(node->left) - height(node->right));
}

/*inserts given node and value to tree.*/
struct AVLNode* add(struct AVLNode* node,int value,struct LinkedList *dest){

    int balances=0;

    if(node==NULL)
        return(createNode(value,dest));

    /*Recursive part*/
    if(value<node->value)
        node->left=add(node->left,value,dest);
    else if(value>node->value){
        node->right=add(node->right,value,dest);
    }
    else return node;

    node->height=getMax(height(node->left),
                        height(node->right)) +1;

    balances=balance(node);
    /*Left left*/
    if(balances>1 && value<node->left->value){
        return rRotate(node);
    }
       
    
    /*Right right*/
    if(balances < -1 && value > node->right->value){
        return lRotate(node);
    }
        
    /*Left Right*/
    if(balances>1 && value > node->left->value){
        node->left=lRotate(node->left);
        return rRotate(node);
    }
    
    /*Right Left*/
    if(balances<-1 && value < node->right->value){
        node->right=rRotate(node->right);
        return lRotate(node);
    }

    return node;
}


struct AVLNode* binarySearch(struct AVLNode* node,int value){
    if(node==NULL) return NULL;
    
    if(node->value==value) return node;
    
    if(node->value > value) return binarySearch(node->left,value);
    if(node->value < value) return binarySearch(node->right,value);
}


void clearTree(struct AVLNode* node){
    if(node == NULL) return;

    clearTree(node->right);
    clearTree(node->left);

    clearLinkedList(node->dest);
    free(node);
}


/*------------------------------------------------------------------*/



/*--------------------AVL TREE FOR BREATH FIRST SEARCH--------------------*/


/*Returns height of AVL Tree.*/
int heightVisited(struct AVLVisited *n){
    if(n==NULL) return 0;
    else 
        return n->height;
}

/*Creates new node and returns it.*/
struct AVLVisited* createVisitedNode(int value1,int parent){
    struct AVLVisited *newNode=(struct AVLVisited*)malloc(sizeof(struct AVLVisited));
    newNode->value=value1;
    newNode->right=NULL;
    newNode->left=NULL;
    newNode->parent=parent;
    newNode->height=1;
    return newNode;
}

/*Right Rotate*/
struct AVLVisited* lRotateVisited(struct AVLVisited* node){
    struct AVLVisited* node1=node->right;
    struct AVLVisited* node2=node1->left;

    node1->left=node;
    node->right=node2;

    /*Update height*/
    node->height=getMax(heightVisited(node->left),heightVisited(node->right))+1;
    node1->height=getMax(heightVisited(node1->left),heightVisited(node1->right))+1;

    return node1;
}

/*Left rotate.*/
struct AVLVisited* rRotateVisited(struct AVLVisited* node){

    struct AVLVisited* node1=node->left;
    struct AVLVisited* node2=node1->right;

    node1->right=node;
    node->left=node2;

    node->height=getMax(heightVisited(node->left),heightVisited(node->right))+1;
    node1->height=getMax(heightVisited(node1->left),heightVisited(node1->right))+1;

    return node1;
}

/*returns balance value of node.*/
int balanceVisited(struct AVLVisited* node){
    if(node==NULL) return 0;
    return (heightVisited(node->left) - heightVisited(node->right));
}

/*inserts given node and value to tree.*/
struct AVLVisited* addVisited(struct AVLVisited* node,int value,int parent){
    int balances=0;

    if(node==NULL)
        return(createVisitedNode(value,parent));
    /*Recursive part*/
    if(value<node->value){
        node->left=addVisited(node->left,value,parent);
    }
    else if(value>node->value){
        node->right=addVisited(node->right,value,parent);
    }
    else{
        return node;
    } 

    node->height=getMax(heightVisited(node->left),
                        heightVisited(node->right)) +1;

    balances=balanceVisited(node);

    /*Left left*/
    if(balances>1 && value<node->left->value)
        return rRotateVisited(node);

    /*Right right*/
    if(balances < -1 && value > node->right->value)
        return lRotateVisited(node);

    /*Left Right*/
    if(balances>1 && value > node->left->value){
        node->left=lRotateVisited(node->left);
        return rRotateVisited(node);
    }

    /*Right Left*/
    if(balances<-1 && value < node->right->value){
        node->right=rRotateVisited(node->right);
        return lRotateVisited(node);
    }

    return node;
}


struct AVLVisited* binarySearchVisited(struct AVLVisited* node,int value){
    if(node==NULL) return NULL;
    if(node->value==value) return node;

    if(node->value > value) return binarySearchVisited(node->left,value);
    if(node->value < value) return binarySearchVisited(node->right,value);
}


void clearTreeVisited(struct AVLVisited* node){
    if(node == NULL) return;

    clearTreeVisited(node->right);
    clearTreeVisited(node->left);
    free(node);
}


/*----------------------------------------------------------------*/





/*--------------------FOR QUEUE--------------------*/

/*Create queue.*/
struct Queue* createQueue(){
    struct Queue* q = (struct Queue*)malloc(sizeof(struct Queue)); 
    q->front = q->rear = NULL; 
    return q; 
}

/*Adds element to rear.*/
void enQueue(struct Queue *q,int value){
    /*Create new node.*/
    struct List_Node *temp = (struct List_Node*)malloc(sizeof(struct List_Node));
    temp->value=value;
    temp->next=NULL;
    /*If queue is empty.*/
    if(q->rear==NULL){
        q->front = q->rear = temp;
        return;
    }

    q->rear->next=temp;
    q->rear=temp;
}

/*Removes element in the front.*/
int deQueue(struct Queue *q){
    int return_value=0;
    /*If queue is empty return fail.*/
    if(q->front == NULL)
        return -1;
    
    struct List_Node *temp = q->front;
    q->front = q->front->next;
    if(q->front == NULL){
        q->rear=NULL;
    }
    return_value=temp->value;
    free(temp);
    return return_value;
}

void clearQueue(struct Queue *q){
    struct List_Node *temp;
    if(q==NULL) return;
    if(q->front==NULL){
        free(q);
        return;
    }

    while(q->front!=NULL){
        temp=q->front;
        q->front=q->front->next;
        free(temp);
    }
    free(q);
}

/*------------------------------------------------*/





/*-----------------FOR LINKEDLIST-----------------*/

struct LinkedList *createLinkedList(){
    struct LinkedList *ll=(struct LinkedList *)malloc(sizeof(struct LinkedList));
    ll->head=NULL;
}

/*This function adds new node to head of linked list.*/
void addLinkedList(struct LinkedList *ll,int node){
    struct List_Node *new_node=(struct List_Node*)malloc(sizeof(struct List_Node));
    new_node->next=NULL;
    new_node->value=node;

    if(ll->head==NULL){
        ll->head=new_node;
    }
    else{
        new_node->next=ll->head;
        ll->head=new_node;
    }
}

/*This function adds new node to end of linked list.*/
void addLinkedListLast(struct LinkedList *ll,int node){
    struct List_Node *new_node=(struct List_Node*)malloc(sizeof(struct List_Node));
    struct List_Node *temp;
    new_node->next=NULL;
    new_node->value=node;
    if(ll->head==NULL){
        ll->head=new_node;
    }
    else{
        temp=ll->head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=new_node;
    }
}

/*This function searches the LinkedList.
    returns 0 is successfull -1 if not.*/
int searchLinkedList(struct LinkedList *ll,int node){
    struct List_Node *temp;

    if(ll==NULL) return -1;
    temp=ll->head;

    if(temp==NULL) return -1;
    
    while(temp!=NULL){
        if(temp->value==node) return 0;
        temp=temp->next;
    }

    return -1;
}


/*Free all allocated memory.*/
void clearLinkedList(struct LinkedList *linked){

    struct List_Node *temp;

    if(linked==NULL){
        return;
    }
    else{
        if(linked->head==NULL){
            free(linked);
        }
        else{
            while(linked->head!=NULL){
                temp=linked->head;
                linked->head=linked->head->next;
                free(temp);
            }
            free(linked);
        }
    }
}


/*-----------------------------------------------*/

/*Finds path between s(source) and d(destination)
    and returns as a linkedlist. Returns NULL in fail.*/
struct LinkedList* BFS(struct AVLNode* g,int s,int d){

    struct AVLVisited* visited=NULL; /*Stores visited element and their parents.*/
    struct AVLVisited* temp_visited=NULL;
    struct AVLNode* temp_avl_node=NULL;

    struct Queue *q=createQueue();
    struct List_Node *temp=NULL; /*For iterating linked list.*/
    struct LinkedList *path=(struct LinkedList*)malloc(sizeof(struct LinkedList));
    path->head=NULL;

    int value=0;
    int node=-1;

    visited=addVisited(visited,s,-1);
    enQueue(q,s);

    while(q->front!=NULL){
        value=deQueue(q); /*Stores element*/

        temp_avl_node=binarySearch(g,value);
        if(temp_avl_node!=NULL)
            temp=temp_avl_node->dest->head;
        else{
            temp=NULL;
        } 
        while(temp!=NULL){

            node=temp->value;
            temp_visited=binarySearchVisited(visited,node);
            if(temp_visited==NULL){
                visited=addVisited(visited,node,value); 
                if(node==d) break;
                enQueue(q,node);
            }
            if(node==d){
                temp_visited->parent=value;
                break;
            } 
            temp=temp->next;
        }
        if(node==d) break;
        
    }
    if(node!=d || node ==-1){
        clearTreeVisited(visited);
        clearQueue(q);
        free(path);
        return NULL;
    }

    if(s==d){
        addLinkedList(path,node);
        node=temp_visited->parent;
    }
    value=-1;
    while(node != s){
        temp_visited=binarySearchVisited(visited,node);
        if(temp_visited!=NULL){
            addLinkedList(path,temp_visited->value);
        }
        node=temp_visited->parent;
    }
    addLinkedList(path,node);
    clearTreeVisited(visited);
    clearQueue(q);
    return path;
}


