/*
    Yusuf Can Kan
*/


/*
    This file includes spesific data structures for
        cache that stores already calculated paths
        in the graph and includes its nessesary
        functions such as checking database for
        spesific path and adding with write prioritization
        mode.

    The paths will be stored in an AVL. AVL includes linked list
        that stores paths as an different avl.
*/


/*Stores paths.*/
struct LinkedPath{
    int destination;
    struct LinkedPath *next;
    struct LinkedList *head; /*Linked list that storing paths. You can 
                                find this data structure implementation 
                                in file.*/
};

struct AVLCache{
    int value;
    struct LinkedPath *path;/*This variable stores the reachable nodes.*/
    struct AVLCache *left;
    struct AVLCache *right;
    int height;
};





/*Creates LinkedPath struct and adds given linked list.*/
struct LinkedPath* createLinkedPath(struct LinkedList *head,int destination);

/*Adds new linked path to linked path list.*/
void addLinkedPath(struct LinkedPath* lp,struct LinkedList *ll,int destination);

/*Clears allocated memory.*/
void clearLinkedPath(struct LinkedPath *node);

/*Adds new path to cache.*/
void addPathToCache(struct AVLCache **cache,struct LinkedList *ll,int destination);

/*Looks for path.*/
struct LinkedList* searchCache(struct AVLCache *cache,int s,int d);





/*Returns height of AVL Tree.*/
int heightCache(struct AVLCache *n);

/*Creates new node and returns it.*/
struct AVLCache* createCacheNode(int value1,struct LinkedPath *lp);

/*Right Rotate*/
struct AVLCache* lRotateCache(struct AVLCache* node);

/*Left rotate.*/
struct AVLCache* rRotateCache(struct AVLCache* node);

/*returns balance value of node.*/
int balanceCache(struct AVLCache* node);

/*inserts given node and value to tree.*/
struct AVLCache* addCache(struct AVLCache* node,int value,struct LinkedPath *lp);

struct AVLCache* binarySearchCache(struct AVLCache* node,int value);

void clearTreeCache(struct AVLCache* node);



/*Read Cache Writer Prior.*/
struct LinkedList* readCacheWP(int fd,struct AVLCache *cache,int s,int d);

/*Write Cache Writer Prior.*/
void writeCacheWP(int fd,struct AVLCache **cache,struct LinkedList *ll,int d);



/*Read Cache Read Prior.*/
struct LinkedList* readCacheRP(int fd,struct AVLCache *cache,int s,int d);

/*Write Cache Read Prior.*/
void writeCacheRP(int fd,struct AVLCache **cache,struct LinkedList *ll,int d);



/*Equal priorities to readers and writers
    If there is current reading readers
    when new reader comes it directly reads 
    data.*/


/*Read Cache Equal Prior.*/
struct LinkedList* readCacheEP(int fd,struct AVLCache *cache,int s,int d);

/*Read Cache Equal Prior.*/
struct LinkedList* writeCacheEP(int fd,struct AVLCache **cache,struct LinkedList *ll,int d);



