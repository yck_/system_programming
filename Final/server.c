#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include <pthread.h>
#include <sys/socket.h> /* For socket, connect, socklen_t */
#include <arpa/inet.h> /* sockaddr_in, inet_pton */
#include <sys/time.h>
#include <time.h>
#include "avl_graph.h"
#include "cache_data_structure.h"
#include "thread_queue.h"


/*Macros*/
#define ONE 1
#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define PLUSONE 1
#define TRUE 1
#define NUMBER_OF_CHILD 4
#define READ 0
#define WRITE 1
#define CONTAINER_SIZE 512
#define LINE_SIZE 256
#define SIZE 2048
#define SEM_PERMS (mode_t)(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)
#define SEM_FLAGS (O_CREAT | O_WRONLY | O_TRUNC)
#define SOCKET_QUEUE_SIZE 200



/***************GLOBAL VARIABLES****************/



/*Mutex for synronization between system and system threads.*/
pthread_mutex_t thread_m = PTHREAD_MUTEX_INITIALIZER; 

/*Mutex for resizer thread.*/
pthread_mutex_t resizer_m = PTHREAD_MUTEX_INITIALIZER; 

/*Cond. variable for resizer thread.*/
pthread_cond_t resizer_cond=PTHREAD_COND_INITIALIZER;

/*Cond. variable for synronization between resizer thread and main func.*/
pthread_cond_t resizer_cond_main=PTHREAD_COND_INITIALIZER;

/*Cond. variable for when there is no available thread and thread poll
    is reached max, main funct. waits any thread to be available.*/
pthread_cond_t server_cond=PTHREAD_COND_INITIALIZER;

/*Conv. variable will be using for after creating all threads
    main function waits for all thread will be ready.*/
pthread_cond_t  star_cond=PTHREAD_COND_INITIALIZER;

/*This conditional variable will be using inside
    signal handler and provides handler waits until
    all threads became available for cancelling.*/
pthread_cond_t  sig_cond=PTHREAD_COND_INITIALIZER;

/*Saves print operation.*/
pthread_mutex_t print_m = PTHREAD_MUTEX_INITIALIZER; 


char *fileLog=NULL;/*Logfile.*/
int logfd=0;

/*
Priorities for writing cache.
    priorities=0 : reader prioritization
    priorities=1 : writer prioritization
    priorities=2 : equal priorities to readers and writers
*/
int priorities=1;


int resizeAlive=1; /*This variable using for controlling if resize 
                        thread is alive or not.*/

int busyThreads=0; /*Busy threads count.*/
int maxThreads=0;  /*Maximum thread limit for creating.*/
int threads=0;     /*Current thread count.*/
int serverWait=0; /*This */
int start=0; /*Variable for after creating threads providing synronization.*/
int isSIGINT=0; /*SIGINT flag for threads.*/
int serverfd=0; /*Serverfd*/
int lockfd;     /*For preventing 2 instance of process.*/

struct flock lock; /*For preventing 2 instance of process.*/

struct timeval startTime;
/*This variable stores the time at the beginning of the program
                            for time stamps.*/

struct AVLNode *graph=NULL; /*Graph data structure.*/
struct AVLCache *cache=NULL; /*Cache data structure.*/
struct Thread_Queue *threadPoll=NULL;    /*Thread polls.*/


pthread_t *resizer_t=NULL;


/*********************************************/





/***************FUNCTION SIGNITURES****************/

/*This function creates new file under "/tmp/" and put on a write lock.
	In this way when user tries to start second server process it
	faces an error.*/
void createLockFile(const char *pidFile);

/*This function wakes the resizer thread.*/
void wakeResizer();

/*This function using as printf if
    something unexpected happens.*/
void print_error(char *message);

/*This function using instead of printf.*/
void print_logfile(char *message);

/*Parses command line arg with using getopt.*/
int parse_arg(int argc, char* argv[],char **fileGraph,int *port);

/*This function reads and initializes graph.*/
int init_graph(struct AVLNode **g,char *file,int *nodes,int *edges);

/*Clean handler for server threads.*/
void cleanupHandlerThreads(void *arg);

/*Clean handler for resize thread.*/
void cleanupHandlerResize(void *arg);

/*Threads*/
void *socketThread(void *arg);

/*Resizer thread*/
void *resizerThread(void *arg);

/*SIGINT Handler.*/
void sigint_handler(int signal_number);

/*Takes difference of 2 times.*/
void timediff(char printTime[16],struct timeval tv,struct timeval tv1);

/*Become daemon.*/
int becomeDaemon();

/**************************************************/





int main(int argc, char*argv[]){
    char line[SIZE];
    int port=0;
    char *fileGraph;/*Char pointer for graph.*/
	char printTime[16]; /*For printing timestamps.*/
    int error=0;
    int error_arg=0;
    int conn_fd; /*Connection fd*/
    socklen_t cLen;
    struct sockaddr_in addr;
    struct sockaddr acceptAddr;
    int acceptAddrSize=0;
    int nodes=0,edges=0;
    double systemload=0;
    int i=0;
    struct thrArg *tempThrArg=NULL;
    struct timeval time1;
    struct timeval time2;
    struct sigaction sigint_action;

    /*For blocking SIGINT in critical sections.*/
    sigset_t mask;    
    sigemptyset(&mask);
    sigaddset(&mask,SIGINT);
    sigset_t oldmask;
    sigemptyset(&oldmask);


    /*Controls double instantiation.*/
    createLockFile("/tmp/161044007d.pid");
    unlink("/tmp/161044007d.pid");

    error_arg=parse_arg(argc,argv,&fileGraph,&port);
    if(error==-2){
        sprintf(line,"Please provide logfile with -o.\n");
        write(STDERR_FILENO,line,strlen(line));
    }
    
    logfd=open(fileLog,SEM_FLAGS,SEM_PERMS);
    if(logfd==ERROR_CONTROL){
        sprintf(line,"Error:Please provide proper logfile!!\n");
        write(STDERR_FILENO,line,strlen(line));
        exit(EXIT_FAILURE);
    }


    becomeDaemon();

    /*Controls double instantiation.*/
    createLockFile("/tmp/161044007d.pid");

    /*Set SIGINT handler for CTRL+C.*/
    memset(&sigint_action,0,sizeof(sigint_action));
	sigint_action.sa_handler = &sigint_handler;


	if(sigaction(SIGINT,&sigint_action,NULL)==ERROR_CONTROL){
        sprintf(line,"\nError while sigaction operation.\n");                
        print_error(line);
        exit(EXIT_FAILURE);
    }


    /********************ARGUMENT CONTROL********************/
    if(error_arg==ERROR_CONTROL){
        sprintf(line,"Error:Please provide proper input: ./server -i pathToFile -p PORT -o pathToLogFile -s 4 -x 24\n");
        print_logfile(line);
        sprintf(line,"or ./server -i pathToFile -p PORT -o pathToLogFile -s 4 -x 24 -r 0 \n");
        print_logfile(line);
        close(logfd);
        exit(EXIT_FAILURE);
    }
    if(threads<2){
        sprintf(line,"Error:s must be bigger than 1.\n");
        print_logfile(line);
        close(logfd);
        exit(EXIT_FAILURE);
    }
    if(threads>maxThreads){
        sprintf(line,"Error:-s must be smaller than or equal to -x!\n");
        print_logfile(line);
        close(logfd);
        exit(EXIT_FAILURE);
    }
    if(port==0){
        sprintf(line,"Error:Please provide proper port!\n");
        print_logfile(line);
        close(logfd);
        exit(EXIT_FAILURE);
    }
    if((priorities>2)||(priorities<0)){
        sprintf(line,"Error:Please provide -r between 0 and 2.!\n");
        print_logfile(line);
        close(logfd);
        exit(EXIT_FAILURE);
    }

    /********************************************************/

    sprintf(line,"Executing with parameters:\n");
    print_logfile(line);
    sprintf(line,"-i %s\n",fileGraph);
    print_logfile(line);
    sprintf(line,"-p %d\n",port);
    print_logfile(line);
    sprintf(line,"-o %s:\n",fileLog);
    print_logfile(line);
    sprintf(line,"-s %d\n",threads);
    print_logfile(line);
    sprintf(line,"-x %d\n",maxThreads);
    print_logfile(line);
    sprintf(line,"-r %d\n",priorities);
    print_logfile(line);


    memset(&addr, 0, sizeof (addr)); /*Reset structure.*/
    addr.sin_family = AF_INET;

    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    addr.sin_port = htons(port);/*Set server port. Using htons provide proper 
                                    byte order.*/

    /*Block SIGINT until graph is loaded and threads are created.*/
    error=sigprocmask(SIG_BLOCK,&mask,&oldmask);
    if(error==ERROR_CONTROL){
        sprintf(line,"Error while blocking SIGINT with sigprocmask.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }

        /*SOCKET*/
    serverfd=socket(AF_INET, SOCK_STREAM, 0);
    if(serverfd==ERROR_CONTROL){
        sprintf(line,"Error on socket.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }

    /*Bind created socket.*/
    if(bind(serverfd, (struct sockaddr*) &addr, sizeof (addr)) < ZERO){
        sprintf(line,"Error on bind:%s.\n",strerror(errno));
        print_error(line);
        exit(EXIT_FAILURE);
    }

    if(listen(serverfd,SOCKET_QUEUE_SIZE) < 0){
        sprintf(line,"Error on listen.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }

    sprintf(line,"Loading graph...\n");
    print_logfile(line);


    /*For timestamp.*/
    if(gettimeofday(&time1,NULL)==ERROR_CONTROL){
        sprintf(line,"Error on gettimeofday call!:%s",strerror(errno));
        print_error(line);
    }

    /*Initializes the graph.*/
    if(init_graph(&graph,fileGraph,&nodes,&edges)==ERROR_CONTROL){
        close(serverfd);
        exit(EXIT_FAILURE);
    }
    
    /*For timestamp.*/
    if(gettimeofday(&time2,NULL)==ERROR_CONTROL){
        sprintf(line,"Error on gettimeofday call!:%s",strerror(errno));
        print_error(line);
    }
    
    printTime[0]='\0';
    /*Calculate timestamp.*/
    timediff(printTime,time1,time2);

    sprintf(line,"Graph loaded in %s seconds with %d nodes and %d edges.\n",printTime,nodes,edges);
    print_logfile(line);

    start=threads;

    /*Create resizer thread.*/
    resizer_t=(pthread_t*)malloc(sizeof(pthread_t));
    error=pthread_create(resizer_t,NULL,resizerThread,NULL);
    if(error!=0){
        sprintf(line,"Thread couldn't create!.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }


    /*Initialize,add all threads and add to thread poll queue.*/
    threadPoll=createThreadQueue();

    for(i=0;i<threads;i++){
        tempThrArg=createThreadArg((i+1));

        error=pthread_create(tempThrArg->th,NULL,socketThread,tempThrArg);
        if(error!=0){
            sprintf(line,"Thread couldn't create!.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }
        
        Thread_enQueue(threadPoll,tempThrArg);
    }

        /*Lock thread mutex.*/
    error=pthread_mutex_lock(&thread_m);
    if(error!=0){
        sprintf(line,"Failed while locking mutex.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }

    /*Waits until all threads ready.*/
    while(start!=0){
        error=pthread_cond_wait(&star_cond,&thread_m);
        if(error!=0){
            sprintf(line, "Failed while waiting conditional variable.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }
    }

    sprintf(line,"A pool of %d threads has been created.\n",threads);
    print_logfile(line);

        /*Lock thread mutex.*/
    error=pthread_mutex_unlock(&thread_m);
    if(error!=0){
        sprintf(line,"Failed while locking mutex.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }
    
    /*Critical section ends unblock SIGINT.*/
    error=sigprocmask(SIG_SETMASK,&oldmask,&mask);
    if(error==ERROR_CONTROL){
        sprintf(line,"Error while unblocking SIGINT with sigprocmask.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }


    while(TRUE){
        cLen = sizeof (addr);
        
        acceptAddrSize=sizeof(acceptAddr);

        if((conn_fd = accept(serverfd,(struct sockaddr*) &addr,&cLen)) < ZERO) {
            sprintf(line,"Error on accept.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }

        /*Block SIGINT UNTIL thread lock is opened.*/
        error=sigprocmask(SIG_SETMASK,&mask,&oldmask);
        if(error==ERROR_CONTROL){
            sprintf(line,"Error while blocking SIGINT with sigprocmask.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }

        error=pthread_mutex_lock(&thread_m);
        if(error!=0){
            sprintf(line,"Failed while locking mutex.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }


        if(threadPoll->front==NULL){
            if(threads<4 && resizeAlive ==1 && maxThreads>threads){
                wakeResizer();/*Adds more threads to the pool.*/
            }
            else{
                serverWait=1;
                sprintf(line,"No thread is available! Waiting for one.\n");
                print_logfile(line);

                /*Waits for any one of threads will be available.*/
                error=pthread_cond_wait(&server_cond,&thread_m);
                if(error!=0){
                    sprintf (line, "Failed while waiting conditional variable.\n");
                    print_error(line);
                    exit(EXIT_FAILURE);
                }
                serverWait=0;
            }
        }
        

        tempThrArg=Thread_deQueue(threadPoll);
        busyThreads++;
        systemload=(100*(double)(busyThreads))/(double)(threads);

        sprintf(line,"A connection has been delegated to thread id #%d, system load %.1f%%\n",
                        tempThrArg->no,systemload);
        print_logfile(line);
        
        tempThrArg->fd=dup(conn_fd);

       
        /*Send signal to resizer for increasing thread pool.*/
        error=pthread_cond_signal((tempThrArg->thr_cond));
        if(error!=0){
            sprintf (line, "Error while signalling conditional variable.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }

        if(resizeAlive==1 && ((double)(busyThreads))>=((double)(0.75*threads))){
            wakeResizer();/*Adds more threads to the pool.*/
        }
            
        close(conn_fd);

        error=pthread_mutex_unlock(&thread_m);
        if(error!=0){
            sprintf(line,"Failed while unlocking mutex.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }

        /*Critical section ends unblock SIGINT.*/
        error=sigprocmask(SIG_SETMASK,&oldmask,&mask);
        if(error==ERROR_CONTROL){
            sprintf(line,"Error while unblocking SIGINT with sigprocmask.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }

    }
    return 0;
}

/*This function creates new file under "/tmp/" and put on a write lock.
	In this way when user tries to start second server process it
	faces an error.*/
void createLockFile(const char *pidFile){
    char buf[LINE_SIZE];
    char line[LINE_SIZE];
    	/*Open file*/
	lockfd = open(pidFile, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
	if(lockfd == ERROR_CONTROL){
        sprintf(line,"Could not open PID file %s\n", pidFile);
        write(logfd,line,strlen(line));
		exit(EXIT_FAILURE);
	}

    lock.l_type = F_WRLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 0;

    int a;
    if(a=fcntl(lockfd,F_SETLK,&lock)==ERROR_CONTROL){
        if (errno == EAGAIN || errno == EACCES){
            line[0]='\0';
            sprintf(line,"You cannot start more than 1 instance of this process!!\n");
            write(logfd,line,strlen(line));
            exit(EXIT_FAILURE);
        }
        else{
            line[0]='\0';
            sprintf(line,"Unable to lock PID file '%s'",pidFile);
            write(logfd,line,strlen(line));
            exit(EXIT_FAILURE);
        }
    }
    if(ftruncate(lockfd, 0) == -1){
        printf("Could not truncate PID file '%s'", pidFile);
        exit(EXIT_FAILURE);
    }

    snprintf(buf, LINE_SIZE, "%ld\n", (long)getpid());
    if(write(lockfd, buf, strlen(buf)) != strlen(buf)){
        printf("Writing to PID file '%s'", pidFile);
        exit(EXIT_FAILURE);
    }

}

/*This function wakes the resizer thread.*/
void wakeResizer(){
    int error=0;
    char line[LINE_SIZE];
    /*Lock resizer mutex.*/
    error=pthread_mutex_lock(&resizer_m);
    if(error!=0){
        sprintf(line,"Failed while locking mutex.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }
    /*Send signal to resizer for increasing thread pool.*/
    error=pthread_cond_signal(&(resizer_cond));
    if(error!=0){
        sprintf (line, "Error while signalling conditional variable.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }
    error=pthread_cond_wait(&resizer_cond_main,&resizer_m);
    if(error!=0){
        sprintf (line, "Failed while waiting conditional variable.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }

    error=pthread_mutex_unlock(&resizer_m);
    if(error!=0){
        sprintf(line,"Failed while unlocking mutex.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }
}

/*This function using as printf if
    something unexpected happens.*/
void print_error(char *message){  
    char line[SIZE]; 
    char *timestamp=NULL;
    time_t t;


    time(&t);
    timestamp=ctime(&t);
    timestamp[strlen(timestamp)-1]='\0';

    sprintf(line,"[%s]  %s\n",timestamp,message);

    write(logfd,line,strlen(line));
}

/*This function using instead of printf.*/
void print_logfile(char *message){
    int error=0;
    char error_line[SIZE];
    char *timestamp=NULL;
    time_t t;
        
    error=pthread_mutex_lock(&print_m);
    if(error!=0){
        sprintf(error_line,"Failed while locking mutex.\n");
        print_error(error_line);
        exit(EXIT_FAILURE);
    }

    time(&t);
    timestamp=ctime(&t);
    timestamp[strlen(timestamp)-1]='\0';

    sprintf(error_line,"[%s]    %s",timestamp,message);

    error=write (logfd, error_line, strlen(error_line));
    if(error==ERROR_CONTROL){
        sprintf(error_line,"Error while write operation:%s\n",
                strerror(error));
        print_error(error_line);
        exit(EXIT_FAILURE);
    }


    error=pthread_mutex_unlock(&print_m);
    if(error!=0){
        sprintf(error_line,"Failed while unlocking mutex.\n");
        print_error(error_line);
        exit(EXIT_FAILURE);
    }
}

/*Parses command line arg with using getopt.*/
int parse_arg(int argc, char* argv[],char** fileGraph,int *port){
	int opt;
    int flag=0;
    int error=0;
    char line[LINE_SIZE];
	while((opt = getopt(argc, argv, "i:p:o:s:x:r:"))!=ERROR_CONTROL){
		switch(opt){
			case 'i':
				(*fileGraph)=optarg;
				break;
			case 'p':
				(*port)=atoi(optarg);
				break;
			case 'o':
				fileLog=optarg;
                flag++;
				break;
			case 's':
				threads=atoi(optarg);
				break;
			case 'x':
				maxThreads=atoi(optarg);
				break;
            case 'r':
                priorities=atoi(optarg);
                break;
			default:
                error++;
		}
	}
    if(flag==0){
        return -2;
    }
	if(((argc!=11)&&(argc!=13))||(error!=0)){
       return RETURN_FAIL;
    }
    return 0;
}

/*This function reads and initializes graph.
    It takes total node count from end of file.*/
int init_graph(struct AVLNode **g,char *file,int *nodes,int *edges){
    struct AVLNode *temp=NULL;
    struct AVLVisited *total_nodes=NULL; /*Stores all nodes so in this way 
                                        we can find total node count.*/
    struct AVLVisited *temp_nodes=NULL;
    char line[LINE_SIZE];
    int fd;
    int node=0,edge=0;
    int error=0;
    char buffer;
    int node1=-1,node2=-1;
    fd=open(file,O_RDONLY);
	if(fd==ERROR_CONTROL){
        sprintf(line,"Graph file couldn't open.\n");                
        print_error(line);
        return RETURN_FAIL;
    }

    error=read(fd,&buffer,1);
    if(error==ERROR_CONTROL){
        sprintf (line,"Failed to graph read file.\n");
        print_error(line);
        return RETURN_FAIL;
    }

    if(error==0){
        sprintf(line,"File is empty. Please provide file with data!!\n");    
        print_error(line);
        return RETURN_FAIL;
    }

/*Skips comment lines.*/
    if(buffer=='#'){
        while(TRUE){
            error=read(fd,&buffer,1);
            if(error==ERROR_CONTROL){
                sprintf (line, "Failed to read file.\n");
                print_error(line);
                return RETURN_FAIL;
            }
            if(buffer=='\n'){
                error=read(fd,&buffer,1);
                if(error==ERROR_CONTROL){
                    sprintf (line, "Failed to read file.\n");
                    print_error(line);
                    return RETURN_FAIL;
                }
                if(buffer!='#'){
                    break;
                }
            }
        }
    }

    line[0]='\0';
    strncat(line,&buffer,1);

    while(error=read(fd,&buffer,1)!=0){
        
        if(buffer!=' ' && buffer!='\n' && buffer!='\t'){
            strncat(line,&buffer,1);
        }
        else if(buffer=='\t'){/*When it reads tab it means it 
                                        readed source node.*/
            node1=atoi(line);
            line[0]='\0';
        }
        else if(buffer=='\n'){/*When it reads new line it means it 
                                        readed destination node.*/
            node2=atoi(line);
            line[0]='\0';
            temp=binarySearch((*g),node1);
            if(temp==NULL){
                (*g)=add(*g,node1,NULL);
                temp=binarySearch((*g),node1);
            }
            if(temp->dest==NULL)
                temp->dest=createLinkedList();
            edge++;
            addLinkedList(temp->dest,node2);

            temp_nodes=binarySearchVisited(total_nodes,node1);
            if(temp_nodes==NULL){
                node++;
                total_nodes=addVisited(total_nodes,node1,0);
            }
            temp_nodes=binarySearchVisited(total_nodes,node2);
            if(temp_nodes==NULL){
                node++;
                total_nodes=addVisited(total_nodes,node2,0);
            }
            node1=-1;
            node2=-1;
        }
    }
    (*nodes)=node;
    (*edges)=edge;

    clearTreeVisited(total_nodes);

    close(fd);
    return 0;
}

/*Clean handler for resize thread.*/
void cleanupHandlerResize(void *arg){
    int error=0;
    char line[40];
    /*Lock thread mutex.*/
    error=pthread_mutex_unlock(&resizer_m);
    if(error!=0){
        sprintf(line,"Failed while locking mutex.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }
}

/*Clean handler for server threads.*/
void cleanupHandlerThreads(void *arg){
    int error=0;
    char line[CONTAINER_SIZE];

    error=pthread_mutex_unlock(&thread_m);
    if(error!=0){
        sprintf(line,"Failed while locking mutex.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }
 
}

/*Threads*/
void *socketThread(void *arg){
    int error;
    char line[SIZE];
    char path[SIZE];
    char buffer='\0';
    int destination=-1,source=-1;
    struct LinkedList* tempLL=NULL;
    struct List_Node* node=NULL;
    struct thrArg *myArg=((struct thrArg*)arg);
    int size=0;
    pthread_cleanup_push(cleanupHandlerThreads,NULL);
    /*Lock thread mutex.*/
    error=pthread_mutex_lock(&thread_m);
    if(error!=0){
        sprintf(line,"Failed while locking mutex.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }

    start--;

    sprintf(line,"Thread #%d: waiting for connection.\n",myArg->no);
    print_logfile(line);

    /*Signal start conditional variable for notifying main about
        another thread is started.*/

    error=pthread_cond_signal(&(star_cond));
    if(error!=0){
        sprintf (line, "Error while signalling conditional variable.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }
    
    while(TRUE){

        error=pthread_cond_wait((myArg->thr_cond),&thread_m);
        if(error!=0){
            sprintf (line, "Failed while waiting conditional variable.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }
    
        /*Unlock thread_m for other threads.*/
        error=pthread_mutex_unlock(&thread_m);
        if(error!=0){
            sprintf(line,"Failed while unlocking mutex.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }


        line[0]='\0';
        while((error=read(myArg->fd,&buffer,1))!=0){
            if(error==ERROR_CONTROL){
                sprintf(line,"Failed while locking mutex.\n");
                print_error(line);
                exit(EXIT_FAILURE);
            }
            if(buffer==' '){
                source=atoi(line);
                line[0]='\0';
            }
            if(buffer=='-') break;
            strncat(line,&buffer,1);
        }
        destination=atoi(line);

        line[0]='\0';
        sprintf(line,"Thread #%d: searching database for a path from node %d to node %d.\n",myArg->no,source,destination);
        print_logfile(line);

        if(priorities==0){
            /*Reader prioritization*/
            tempLL=readCacheRP(myArg->fd,cache,source,destination);
        }
        else if(priorities==1){
            /*Writer prioritization*/
            tempLL=readCacheWP(myArg->fd,cache,source,destination);
        }
        else{
            /*Equal prioritization*/
            tempLL=readCacheEP(myArg->fd,cache,source,destination);
        }

        if(tempLL!=NULL && tempLL->head!=NULL){
            sprintf(line,"Thread #%d: Path found in database from %d to %d.\n",
                            myArg->no,source,destination);
            print_logfile(line);
        }
            
        if(tempLL==NULL){

            sprintf(line,"Thread #%d: no path in database, calculating %d to %d.\n",
                                myArg->no,source,destination);
            print_logfile(line);

            tempLL=BFS(graph,source,destination);

            if(tempLL==NULL){
                tempLL=createLinkedList();
                addLinkedList(tempLL,source);
            }
            

            sprintf(line,"Thread #%d: path calculated from %d to %d adding database...\n",
                            myArg->no,source,destination);
            print_logfile(line);


            if(priorities==0){
                /*Reader prioritization*/
                writeCacheRP(myArg->fd,&cache,tempLL,destination);
            }
            else if(priorities==1){
                /*Writer prioritization*/
                writeCacheWP(myArg->fd,&cache,tempLL,destination);
            }
            else{
                /*Equal prioritization*/
                writeCacheEP(myArg->fd,&cache,tempLL,destination);
            }
        
        }

        line[0]='\0';
        path[0]='\0';
        if(tempLL!=NULL && tempLL->head!=NULL && tempLL->head->next!=NULL ){
            node=tempLL->head;
            while(node!=NULL){
                sprintf(line,"%d->",node->value);
                strncat(path,line,strlen(line));
                node=node->next;
            }

            size=strlen(path)-2;
            path[strlen(path)-2]='\0';

            sprintf(line,"Thread #%d: responding to client: %s.\n",myArg->no,path);
            print_logfile(line);

            path[size]='+';
            path[size+1]='\0';
            
            error=write(myArg->fd,path,strlen(path));
            if(error==ERROR_CONTROL){
                sprintf(line,"Failed while write operation.\n");
                print_error(line);
                exit(EXIT_FAILURE);
            }
        }
        else{
            sprintf(line,"Thread #%d: path not possible from node %d to %d\n",myArg->no,source,destination);
            print_logfile(line);

            line[0]='*';
            line[1]='+';
            line[2]='\0';
            error=write(myArg->fd,line,2);
            if(error==ERROR_CONTROL){
                sprintf(line,"Failed while write operation.\n");
                print_error(line);
                exit(EXIT_FAILURE);
            }
        
        }
        
        
        close(myArg->fd);
        
        error=pthread_mutex_lock(&thread_m);
        if(error!=0){
            sprintf(line,"Failed while locking mutex.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }
          
        Thread_enQueue(threadPoll,myArg);

        busyThreads--;

        if(isSIGINT==1){
            /*Send signal to SIGINT handler for notifying */
            error=pthread_cond_signal(&(sig_cond));
            if(error!=0){
                sprintf (line,"Error while signalling conditional variable.\n");
                print_error(line);
                exit(EXIT_FAILURE);
            }

            error=pthread_mutex_unlock(&thread_m);
            if(error!=0){
                sprintf(line,"Failed while unlocking mutex.\n");
                print_error(line);
                exit(EXIT_FAILURE);
            }

            pthread_exit(0);
        }
        if(serverWait==1){
            /*Send signal to main funct. for notifying 
                there is available thread.*/
            error=pthread_cond_signal(&(server_cond));
            if(error!=0){
                sprintf (line, "Error while signalling conditional variable.\n");
                print_error(line);
                exit(EXIT_FAILURE);
            }
        }
    }
    
    pthread_cleanup_pop(0);
}

/*Resizer thread*/
void *resizerThread(void *arg){
    char line[CONTAINER_SIZE];
    int error=0;
    int resize_count=0;
    int i=0;
    struct thrArg *tempThrArg=NULL;
    struct timeval myTime; /*For timestamps.*/
    char printTime[16]; /*For printing timestamps.*/
    int threadno;
    double systemload=0;

    pthread_cleanup_push(cleanupHandlerResize,NULL);

    error=pthread_mutex_lock(&resizer_m);
    if(error!=0){
        sprintf(line,"Failed while locking mutex.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }

    while(TRUE){
        /*Wait for main's inkove for extra thread.*/
        error=pthread_cond_wait(&resizer_cond,&resizer_m);
        if(error!=0){
            sprintf (line, "Failed while waiting conditional variable.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }
        if(maxThreads>threads){
            resize_count=((0.25)*(double)threads);
            if(resize_count==0) resize_count=1;

            if(resize_count+threads>maxThreads){
                resize_count=maxThreads-threads;
            }

            systemload=(100*(double)(busyThreads))/(double)(threads);
            
            start+=resize_count;

            sprintf(line,"System load %.1f %% , pool extended to %d threads\n",systemload,threads+resize_count);
            print_logfile(line);
            
            threadno=threads+1;
            for(i=0;i<resize_count;i++){
                tempThrArg=createThreadArg((threadno));

                error=pthread_create(tempThrArg->th,NULL,socketThread,tempThrArg);
                if(error!=0){
                    sprintf(line,"Thread couldn't create!.\n");
                    print_error(line);
                    exit(EXIT_FAILURE);
                }
                
                Thread_enQueue(threadPoll,tempThrArg);
                threadno++;
            }
            threads+=resize_count;

            /*Waits until all threads ready.*/
            while(start!=0){
                error=pthread_cond_wait(&star_cond,&thread_m);
                if(error!=0){
                    sprintf(line, "Failed while waiting conditional variable.\n");
                    print_error(line);
                    exit(EXIT_FAILURE);
                }
            }
        }
        /*If pool reaches maximum capacity thread exits automatically.*/
        if(threads==maxThreads){
            resizeAlive=0;

            /*Notify main about new threads.*/
            error=pthread_cond_signal(&(resizer_cond_main));
            if(error!=0){
                sprintf (line,"Error while signalling conditional variable.\n");
                print_error(line);
                exit(EXIT_FAILURE);
            }

            error=pthread_mutex_unlock(&resizer_m);
            if(error!=0){
                sprintf(line,"Failed while unlocking mutex.\n");
                print_error(line);
                exit(EXIT_FAILURE);
            }

            pthread_exit(0);

        }
        /*Notify main about new threads.*/
        error=pthread_cond_signal(&(resizer_cond_main));
        if(error!=0){
            sprintf (line,"Error while signalling conditional variable.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }

    }
        pthread_cleanup_pop(0);
}

/*SIGINT Handler.*/
void sigint_handler(int signal_number){

    int error=0;
    char line[CONTAINER_SIZE];
    struct thrArg *tempNode=NULL;

    isSIGINT++;

    sprintf(line,"Termination signal received, waiting for ongoing threads to complete..\n");
    print_logfile(line);

    error=pthread_mutex_lock(&resizer_m);
    if(error!=0){
        sprintf(line,"Failed while locking mutex.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }

    if(resizeAlive==1 && resizer_t!=NULL){
        pthread_cancel(*(resizer_t));

        error=pthread_mutex_unlock(&resizer_m);
        if(error!=0){
            sprintf(line,"Failed while unlocking mutex.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }

        pthread_join((*resizer_t),NULL);
    }
    if(resizer_t!=NULL){
        pthread_join((*resizer_t),NULL);
        free(resizer_t);
    }
    
    error=pthread_mutex_lock(&thread_m);
    if(error!=0){
        sprintf(line,"Failed while locking mutex.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }
    
    while(busyThreads!=0){
        error=pthread_cond_wait(&sig_cond,&thread_m);
        if(error!=0){
            sprintf (line, "Failed while waiting conditional variable.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }      
    }

    while(threadPoll->front!=NULL){
        tempNode=Thread_deQueue(threadPoll);
        pthread_cancel(*(tempNode->th));

        error=pthread_mutex_unlock(&thread_m);
        if(error!=0){
            sprintf(line,"Failed while unlocking mutex.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }

        pthread_join(*(tempNode->th),NULL);
        free(tempNode->th);

        if(pthread_cond_destroy(tempNode->thr_cond)!=0){
            sprintf (line, "Failed while destroying conditional variable.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }
        
        free(tempNode->thr_cond);
        free(tempNode);

        error=pthread_mutex_lock(&thread_m);
        if(error!=0){
            sprintf(line,"Failed while unlocking mutex.\n");
            print_error(line);
            exit(EXIT_FAILURE);
        }
    }

    error=pthread_mutex_unlock(&resizer_m);
    if(error!=0){
        sprintf(line,"Failed while locking mutex.\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }
    
    sprintf(line,"All threads have terminated, server shutting down.\n");
    print_logfile(line);

    if(close(logfd)<0){
        sprintf(line,"Error while closing log file.\n");
        print_logfile(line);
    }

    free(threadPoll);

    clearTreeCache(cache);
    clearTree(graph);

    if(close(serverfd)<0){
        sprintf(line,"Error while closing serverfd.\n");
        print_logfile(line);
    }

    unlink("/tmp/16104400d.pid");
    exit(EXIT_SUCCESS);
}

/*Takes difference of 2 times.*/
void timediff(char printTime[16],struct timeval tv,struct timeval tv1){	
	struct timeval diff;

	diff.tv_sec = tv1.tv_sec - tv.tv_sec;
	diff.tv_usec = tv1.tv_usec - tv.tv_usec;

	if(diff.tv_usec<0){
		diff.tv_sec--;
		diff.tv_usec+=1000000;
	}
	sprintf(printTime,"%ld.%ld",diff.tv_sec,(diff.tv_usec/1000));
}

/*Become deamon process.*/
int becomeDaemon(){
	
	int fd;

	switch(fork()){ /* Become background process */
		case -1: 
			return -1;
		case 0: 
			break;
		default: 
			exit(EXIT_SUCCESS); /*Terminate parent.*/
	}
	
	if(setsid()==-1)
		return -1; /* Become leader of new session */

	switch(fork()){	/* Ensures that it is not session leader */
		case -1:
			return -1;
		case 0:
			break;
		default:
			exit(EXIT_SUCCESS);
	}

	umask(0); /* Clear file mode creation mask */
	
	close(STDIN_FILENO); /* Reopen standard fd's to /dev/null */

	fd = open("/dev/null", O_RDWR);
	if (fd != STDIN_FILENO) /* 'fd' should be 0 */
		return -1;
	if (dup2(STDIN_FILENO, STDOUT_FILENO) != STDOUT_FILENO)
		return -1;
	if (dup2(STDIN_FILENO, STDERR_FILENO) != STDERR_FILENO)
		return -1;

}
