/*
    This file contains different queue data structure that spesific
        for thread structrures.
*/

#include <stdio.h> 
#include <stdlib.h>
#include <pthread.h>
#include "thread_queue.h"

struct thrArg;
struct Thread_Queue;
struct Thread_List_Node;


/*Create queue.*/
struct Thread_Queue* createThreadQueue(){
    struct Thread_Queue* q = (struct Thread_Queue*)malloc(sizeof(struct Thread_Queue)); 
    q->front = q->rear = NULL; 
    return q; 
}

/*Adds element to rear.*/
void Thread_enQueue(struct Thread_Queue *q,struct thrArg *value){
    /*Create new node.*/
    struct Thread_List_Node *temp = (struct Thread_List_Node*)malloc(sizeof(struct Thread_List_Node));
    temp->thrArgValue=value;
    temp->next=NULL;
    /*If queue is empty.*/
    if(q->rear==NULL){
        q->front = q->rear = temp;
        return;
    }

    q->rear->next=temp;
    q->rear=temp;
}

/*Removes element in the front.*/
struct thrArg* Thread_deQueue(struct Thread_Queue *q){
    struct thrArg *return_value=0;
    /*If queue is empty return fail.*/
    if(q->front == NULL)
        return NULL;
    
    struct Thread_List_Node *temp = q->front;
    q->front = q->front->next;
    if(q->front == NULL){
        q->rear=NULL;
    }
    return_value=temp->thrArgValue;
    free(temp);
    return return_value;
}


struct thrArg* createThreadArg(int id){
    struct thrArg *arg=NULL;
    int error=0;

    arg=(struct thrArg *)malloc(sizeof(struct thrArg));
    arg->fd=-1;
    arg->thr_cond=(pthread_cond_t *)malloc(sizeof(pthread_cond_t));
    error=pthread_cond_init(arg->thr_cond,NULL);
    if(error!=0){
        free(arg->thr_cond);
        return NULL;
    }
    arg->no=id;
    arg->th=(pthread_t*)malloc(sizeof(pthread_t));
    


    return arg;
}


