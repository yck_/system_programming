#include <stdio.h>
#include <fcntl.h> 
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h> /* For socket, connect, socklen_t */
#include <sys/types.h>
#include <arpa/inet.h> /* sockaddr_in, inet_pton */
#include <errno.h>
#include <sys/time.h>

/*Macros*/
#define ONE 1
#define ZERO 0
#define RETURN_FAIL -1
#define RETURN_SUCCESS 0
#define START 0
#define ERROR_CONTROL -1
#define PLUSONE 1
#define TRUE 1
#define NUMBER_OF_CHILD 4
#define READ 0
#define WRITE 1
#define CONTAINER_SIZE 512
#define LINE_SIZE 256
#define SIZE 2048




/*Parses command line arg with using getopt.*/
void parse_arg(int argc, char* argv[],char** ip,int *port,int *s,int *d);

/*Takes difference of 2 times.*/
void timediff(char printTime[16],struct timeval tv,struct timeval tv1);

/*This function using as printf if
    something unexpected happens.*/
void print_error(char *message);

/*This functions using instead of printf. */
void print_screen(char *message);




int main(int argc, char* argv[]){
    
    int clientfd=0; /*Stores socket fd.*/
    struct sockaddr_in addr;
    char *ip;
	int port=0,s=-1,d=-1;
    char line[SIZE];
    char linee[SIZE];
    int error=0;
    char buffer;
    int i=0;
    struct timeval time1;
    struct timeval time2;
    char printTime[16]; /*For printing timestamps.*/

    memset(&addr,0,sizeof(addr)); /*Reset structure.*/

    parse_arg(argc,argv,&ip,&port,&s,&d);

        
    /*Open socket.*/
    if((clientfd=socket(PF_INET, SOCK_STREAM,0))==ERROR_CONTROL){
        sprintf(line,"error while socket operation!\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }

    addr.sin_family=AF_INET;
    addr.sin_port=htons(port);/*Set server port. Using htons provide 
                                        proper byte order.*/ 
        

    /* convert command line argument to numeric IP */
    if (inet_pton(AF_INET,ip,&(addr.sin_addr))!=1) {
        sprintf(line,"Invalid IP address\n");
        print_error(line);
        exit(EXIT_FAILURE);
    }


    sprintf(line,"Client (%d) connecting to %s:%d\n",getpid(),ip,port);
    print_screen(line);

    if(error=connect(clientfd,(struct sockaddr*)(&addr),sizeof(addr))==ERROR_CONTROL){
            /* close socket*/
        if(close(clientfd)<0){
            perror("socket close error");
            exit(EXIT_FAILURE);
        }

        sprintf(line,"connect error:%s,%d!\n",strerror(errno),errno);
        print_error(line);
        exit(EXIT_FAILURE);
    }
    
    sprintf(line,"Client (%d) connected and requesting a path from node %d to %d\n",getpid(),s,d);
    print_screen(line);

    sprintf(line,"%d %d-",s,d);
    error=write(clientfd,line,strlen(line));
    if(error==ERROR_CONTROL){
        sprintf(line,"Error while write operation.!");
        print_error(line);
    }



    /*For timestamp.*/
    if(gettimeofday(&time1,NULL)==ERROR_CONTROL){
        sprintf(line,"Error on gettimeofday call!:%s",strerror(errno));
        print_error(line);
    }


    line[0]='\0';
    while(read(clientfd,&buffer,1)!=0){
        if(buffer=='+') break;
        strncat(line,&buffer,1);
        i++;
    }

    line[i]='\0';

        /*For timestamp.*/
    if(gettimeofday(&time2,NULL)==ERROR_CONTROL){
        sprintf(line,"Error on gettimeofday call!:%s",strerror(errno));
        print_error(line);
    }

    printTime[0]='\0';
    /*Calculate timestamp.*/
    timediff(printTime,time1,time2);


    if(line[0]=='*'){
        line[0]='\0';
        sprintf(line,"Server’s response (%d): NO PATH, arrived in %s seconds, shutting down\n",getpid(),printTime);
        print_screen(line);
    }
    else{
        if(line[0]!='\0'){
            linee[0]='\0';
            sprintf(linee,"Server’s response to (%d): %s, arrived in %s seconds.\n",getpid(),line,printTime);
            print_screen(linee);
        }
        else{
            sprintf(line,"(%d): No Response From Server!\n",getpid());
            print_screen(line);
        }
    }

    if(close(clientfd)<0){
        sprintf(line,"(%d): socket close error\n",getpid());
        print_error(line);
        exit(EXIT_FAILURE);
    }

    return 0;
}








/*Parses command line arg with using getopt.*/
void parse_arg(int argc, char* argv[],char** ip,int *port,int *s,int *d){
	int opt;
    int flag=0;
    int error=0;
    char line[LINE_SIZE];
	while((opt = getopt(argc, argv, "a:p:s:d:"))!=ERROR_CONTROL){
		switch(opt){
			case 'a':
				(*ip)=optarg;
				break;
			case 'p':
				(*port)=atoi(optarg);
				break;
			case 's':
				(*s)=atoi(optarg);
                flag++;
				break;
			case 'd':
				(*d)=atoi(optarg);
				break;
			default:
                sprintf(line,"\nPlease provide proper command line arguement!\n");
                print_error(line);
                sprintf(line,"./client -a 127.0.0.1 -p PORT -s source -d destination\n");
                print_error(line);
				exit(EXIT_FAILURE);
		}
	}
	if(argc!=9){
        sprintf(line,"\nPlease provide proper command line arguement!\n");
        print_error(line);
        sprintf(line,"./client -a 127.0.0.1 -p PORT -s source -d destination\n");                
        print_error(line);
		exit(EXIT_FAILURE);
    }
}



/*Takes difference of 2 times.*/
void timediff(char printTime[16],struct timeval tv,struct timeval tv1){	
	struct timeval diff;

	diff.tv_sec = tv1.tv_sec - tv.tv_sec;
	diff.tv_usec = tv1.tv_usec - tv.tv_usec;

	if(diff.tv_usec<0){
		diff.tv_sec--;
		diff.tv_usec+=1000000;
	}
	sprintf(printTime,"%ld.%ld",diff.tv_sec,(diff.tv_usec/1000));
}



    /*This function using as printf if
    something unexpected happens.*/
void print_error(char *message){  
    char line[SIZE]; 
    char *timestamp=NULL;
    time_t t;

    time(&t);
    timestamp=ctime(&t);
    timestamp[strlen(timestamp)-1]='\0';

    sprintf(line,"[%s]  %s\n",timestamp,message);

    write (STDERR_FILENO, line, strlen(line));
}

/*This function using instead of printf.*/
void print_screen(char *message){
    int error=0;
    char error_line[SIZE];
    char *timestamp=NULL;
    time_t t;
    
    time(&t);
    timestamp=ctime(&t);
    timestamp[strlen(timestamp)-1]='\0';

    sprintf(error_line,"[%s]    %s",timestamp,message);

    error=write (STDOUT_FILENO, error_line, strlen(error_line));
    if(error==ERROR_CONTROL){
        sprintf(error_line,"Error while write operation:%s\n",strerror(error));
        print_error(error_line);
        exit(EXIT_FAILURE);
    }

}
