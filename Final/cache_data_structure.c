/*
    Yusuf Can Kan
*/


/*
    This file includes spesific data structures for
        cache that stores already calculated paths
        in the graph and includes its nessesary
        functions such as checking database for
        spesific path and adding with write or other prioritization
        mode.

    The paths will be stored in an AVL. AVL includes linked list
        that stores paths as an different avl.
    
*/



#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include "avl_graph.h"
#include "cache_data_structure.h"



int AR=0; /*Active Reader.*/
int AW=0; /*Active Writer.*/
int WR=0; /*Waiting Readers.*/
int WW=0; /*Waiting Writers.*/

pthread_cond_t okToRead=PTHREAD_COND_INITIALIZER;
pthread_cond_t okToWrite=PTHREAD_COND_INITIALIZER;
pthread_cond_t okToReach=PTHREAD_COND_INITIALIZER;
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;



struct LinkedPath;
struct AVLCache;



/*Creates LinkedPath struct and adds given linked list.*/
struct LinkedPath* createLinkedPath(struct LinkedList *head,int destination){
    struct LinkedPath *temp=(struct LinkedPath*)malloc(sizeof(struct LinkedPath));
    temp->head=head;
    temp->next=NULL;
    temp->destination=destination;
    return temp;
}

/*Adds new linked path to linked path list.*/
void addLinkedPath(struct LinkedPath* lp,struct LinkedList *ll,int destination){
    struct LinkedPath* temp=NULL;

    temp=lp;

    while(temp->next!=NULL){
        temp=temp->next;
    }
    temp->next=createLinkedPath(ll,destination);
}

/*Clears allocated memory.*/
void clearLinkedPath(struct LinkedPath *node){
    struct LinkedPath *tempPath=NULL;
    struct LinkedList *tempList=NULL;


    while(node!=NULL){
        tempPath=node;

        tempList=tempPath->head;
        clearLinkedList(tempList);

        node=node->next;
        free(tempPath);
    }
}

/*Adds new path to cache.*/
void addPathToCache(struct AVLCache **cache,struct LinkedList *ll,int destination){

    struct AVLCache *temp=NULL;
    struct LinkedPath* lPath=NULL;
    if(ll==NULL) return;
    temp=binarySearchCache((*cache),ll->head->value);
    if(temp==NULL){
        lPath=createLinkedPath(ll,destination);
        (*cache)=addCache((*cache),ll->head->value,lPath);
        temp=binarySearchCache((*cache),ll->head->value);
    }
    else{
        addLinkedPath(temp->path,ll,destination);
    }
    
}

/*Looks for path.*/
struct LinkedList* searchCache(struct AVLCache *cache,int s,int d){
    
    struct AVLCache *temp=NULL;
    struct LinkedPath *tempPath=NULL;
    struct LinkedList *path=NULL;
    struct List_Node *tempNode=NULL;


    int search=0; 

    temp=binarySearchCache(cache,s);
    if(temp==NULL) return NULL;


    tempPath=temp->path;

    while(tempPath!=NULL){
        if(tempPath->destination==d) return tempPath->head;
        tempPath=tempPath->next;
    }

    return NULL;

}








/*--------------------FOR AVL--------------------------*/

/*Returns height of AVL Tree.*/
int heightCache(struct AVLCache *n){
    if(n==NULL) return 0;
    else return n->height;
}

/*Creates new node and returns it.*/
struct AVLCache* createCacheNode(int value1,struct LinkedPath *lp){
    struct AVLCache *newNode=(struct AVLCache*)malloc(sizeof(struct AVLCache));
    newNode->value=value1;
    newNode->right=NULL;
    newNode->left=NULL;
    newNode->path=lp;
    newNode->height=1;
    return newNode;
}

/*Right Rotate*/
struct AVLCache* lRotateCache(struct AVLCache* node){
    struct AVLCache* node1=node->right;
    struct AVLCache* node2=node1->left;

    node1->left=node;
    node->right=node2;

    /*Update height*/
    node->height=getMax(heightCache(node->left),heightCache(node->right))+1;
    node1->height=getMax(heightCache(node1->left),heightCache(node1->right))+1;

    return node1;
}

/*Left rotate.*/
struct AVLCache* rRotateCache(struct AVLCache* node){

    struct AVLCache* node1=node->left;
    struct AVLCache* node2=node1->right;

    node1->right=node;
    node->left=node2;

    node->height=getMax(heightCache(node->left),heightCache(node->right))+1;
    node1->height=getMax(heightCache(node1->left),heightCache(node1->right))+1;

    return node1;
}

/*returns balance value of node.*/
int balanceCache(struct AVLCache* node){
    if(node==NULL) return 0;
    return (heightCache(node->left) - heightCache(node->right));
}

/*inserts given node and value to tree.*/
struct AVLCache* addCache(struct AVLCache* node,int value,struct LinkedPath *lp){
    int balances=0;

    if(node==NULL)
        return(createCacheNode(value,lp));
    /*Recursive part*/
    if(value<node->value){
        node->left=addCache(node->left,value,lp);
    }
    else if(value>node->value){
        node->right=addCache(node->right,value,lp);
    }
    else{
        return node;
    } 

    node->height=getMax(heightCache(node->left),
                        heightCache(node->right)) +1;

    balances=balanceCache(node);

    /*Left left*/
    if(balances>1 && value<node->left->value)
        return rRotateCache(node);

    /*Right right*/
    if(balances < -1 && value > node->right->value)
        return lRotateCache(node);

    /*Left Right*/
    if(balances>1 && value > node->left->value){
        node->left=lRotateCache(node->left);
        return rRotateCache(node);
    }

    /*Right Left*/
    if(balances<-1 && value < node->right->value){
        node->right=rRotateCache(node->right);
        return lRotateCache(node);
    }

    return node;
}


struct AVLCache* binarySearchCache(struct AVLCache* node,int value){
    if(node==NULL) return NULL;
    if(node->value==value) return node;

    if(node->value > value) return binarySearchCache(node->left,value);
    if(node->value < value) return binarySearchCache(node->right,value);
}


void clearTreeCache(struct AVLCache* node){
    if(node == NULL) return;
    clearTreeCache(node->right);
    clearTreeCache(node->left);
    if(node->path!=NULL)
        clearLinkedPath(node->path);
    free(node);
}

/*----------------------------------------------------------*/



/*Read Cache Writer Prior.*/
struct LinkedList* readCacheWP(int fd,struct AVLCache *cache,int s,int d){
    int error=0;
    char line[64];
    struct LinkedList *return_value=NULL;

    error=pthread_mutex_lock(&m);
    if(error!=0){
        sprintf(line, "Failed while locking mutex.\n");
        write (fd, line, strlen(line));
		pthread_exit(NULL);
    }

    while((AW+WW)>0){
        WR++;
        error=pthread_cond_wait(&okToRead,&m);
        if(error!=0){
            sprintf(line,"Error while waiting cond variable.\n");
            write (fd, line, strlen(line));
            pthread_exit(NULL);
        }
        WR--;
    }
    AR++;
    error=pthread_mutex_unlock(&m);
    if(error!=0){
        sprintf(line, "Failed while unlocking mutex.\n");
        write(fd, line, strlen(line));
		pthread_exit(NULL);
    }

    /*Searches the cache.*/
    return_value=searchCache(cache,s,d);

    error=pthread_mutex_lock(&m);
    if(error!=0){
        sprintf(line, "Failed while locking mutex.\n");
        write(fd, line, strlen(line));
		pthread_exit(NULL);
    }
    AR--;

    if(AR==0 && WW>0){
        error=pthread_cond_signal(&okToWrite);
        if(error!=0){
            sprintf(line,"Error while signalling cond. variable.\n");
            write(fd, line, strlen(line));
            pthread_exit(NULL);
        }
    }

    error=pthread_mutex_unlock(&m);
    if(error!=0){
        sprintf(line, "Failed while unlocking mutex.\n");
        write (fd, line, strlen(line));
		pthread_exit(NULL);
    }

    return return_value;
}

/*Write Cache Writer Prior.*/
void writeCacheWP(int fd,struct AVLCache **cache,struct LinkedList *ll,int d){
    int error=0;
    char line[64];

    error=pthread_mutex_lock(&m);
    if(error!=0){
        sprintf(line, "Failed while locking mutex.\n");
        write(fd, line, strlen(line));
		pthread_exit(NULL);
    }

    while((AW + AR) >0){
        WW++;
        error=pthread_cond_wait(&okToWrite,&m);
        if(error!=0){
            sprintf(line,"Error while waiting cond variable.\n");
            write (fd, line, strlen(line));
            pthread_exit(NULL);
        }
        WW--;
    }
    AW++;

    error=pthread_mutex_unlock(&m);
    if(error!=0){
        sprintf(line, "Failed while unlocking mutex.\n");
        write (fd, line, strlen(line));
		pthread_exit(NULL);
    }

    addPathToCache(cache,ll,d);

    error=pthread_mutex_lock(&m);
    if(error!=0){
        sprintf(line, "Failed while locking mutex.\n");
        write (fd, line, strlen(line));
		pthread_exit(NULL);
    }

    AW--;

    if(WW>0){
        error=pthread_cond_signal(&okToWrite);
        if(error!=0){
            sprintf(line,"Error while signalling cond. variable.\n");
            write(fd,line,strlen(line));
            pthread_exit(NULL);
        }
    }
    else if (WR>0){
        error=pthread_cond_broadcast(&okToRead);
        if(error!=0){
            sprintf (line, "Failed while broadcasting cond. variable.\n");
            write(fd,line,strlen(line));
            exit(EXIT_FAILURE);
        }
    }
        
    error=pthread_mutex_unlock(&m);
    if(error!=0){
        sprintf(line, "Failed while unlocking mutex.\n");
        write (fd, line, strlen(line));
		pthread_exit(NULL);
    }

}



/*Read Cache Read Prior.*/
struct LinkedList* readCacheRP(int fd,struct AVLCache *cache,int s,int d){
    int error=0;
    char line[64];
    struct LinkedList *return_value=NULL;

    error=pthread_mutex_lock(&m);
    if(error!=0){
        sprintf(line, "Failed while locking mutex.\n");
        write (fd, line, strlen(line));
		pthread_exit(NULL);
    }
    while(AW>0){
        WR++;
        error=pthread_cond_wait(&okToRead,&m);
        if(error!=0){
            sprintf(line,"Error while waiting cond variable.\n");
            write (fd, line, strlen(line));
            pthread_exit(NULL);
        }
        WR--;
    }
    AR++;
    error=pthread_mutex_unlock(&m);
    if(error!=0){
        sprintf(line, "Failed while unlocking mutex.\n");
        write(fd, line, strlen(line));
		pthread_exit(NULL);
    }
    return_value=searchCache(cache,s,d);

    error=pthread_mutex_lock(&m);
    if(error!=0){
        sprintf(line, "Failed while locking mutex.\n");
        write(fd, line, strlen(line));
		pthread_exit(NULL);
    }

    AR--;

    if(WR>0){
        error=pthread_cond_signal(&okToRead);
        if(error!=0){
            sprintf(line,"Error while signalling cond. variable.\n");
            write(fd, line, strlen(line));
            pthread_exit(NULL);
        }
    }
    else if(AR==0 && WW>0){
        error=pthread_cond_signal(&okToWrite);
        if(error!=0){
            sprintf(line,"Error while signalling cond. variable.\n");
            write(fd, line, strlen(line));
            pthread_exit(NULL);
        }
    }

    error=pthread_mutex_unlock(&m);
    if(error!=0){
        sprintf(line, "Failed while unlocking mutex.\n");
        write (fd, line, strlen(line));
		pthread_exit(NULL);
    }
    return return_value;

}


/*Write Cache Read Prior.*/
void writeCacheRP(int fd,struct AVLCache **cache,struct LinkedList *ll,int d){
    int error=0;
    char line[64];

    error=pthread_mutex_lock(&m);
    if(error!=0){
        sprintf(line,"Failed while locking mutex.\n");
        write(fd, line, strlen(line));
		pthread_exit(NULL);
    }
    while((AW + AR + WR) >0){
        WW++;
        error=pthread_cond_wait(&okToWrite,&m);
        if(error!=0){
            sprintf(line,"Error while waiting cond variable.\n");
            write (fd, line, strlen(line));
            pthread_exit(NULL);
        }
        WW--;
    }
    AW++;
    error=pthread_mutex_unlock(&m);
    if(error!=0){
        sprintf(line,"Failed while unlocking mutex.\n");
        write (fd, line, strlen(line));
		pthread_exit(NULL);
    }

    addPathToCache(cache,ll,d);

    error=pthread_mutex_lock(&m);
    if(error!=0){
        sprintf(line,"Failed while locking mutex.\n");
        write (fd, line, strlen(line));
		pthread_exit(NULL);
    }

    AW--;
    
    if(AW==0 && WR>0){
        error=pthread_cond_signal(&okToRead);
        if(error!=0){
            sprintf(line,"Error while signalling cond. variable.\n");
            write(fd,line,strlen(line));
            pthread_exit(NULL);
        }
    }
    else if(WW>0){
        error=pthread_cond_signal(&okToWrite);
        if(error!=0){
            sprintf (line,"Failed while broadcasting cond. variable.\n");
            write(fd,line,strlen(line));
            exit(EXIT_FAILURE);
        }
    }

    error=pthread_mutex_unlock(&m);
    if(error!=0){
        sprintf(line,"Failed while locking mutex.\n");
        write (fd, line, strlen(line));
		pthread_exit(NULL);
    }
}




/*Equal priorities to readers and writers
    If there is current reading readers
    when new reader comes it directly reads 
    data.*/

/*Read Cache Equal Prior.*/
struct LinkedList* readCacheEP(int fd,struct AVLCache *cache,int s,int d){
    int error=0;
    char line[64];
    struct LinkedList *return_value=NULL;

    error=pthread_mutex_lock(&m);
    if(error!=0){
        sprintf(line, "Failed while locking mutex.\n");
        write(fd, line, strlen(line));
		pthread_exit(NULL);
    }

    while(AW>0){
        WR++;
        error=pthread_cond_wait(&okToReach,&m);
        if(error!=0){
            sprintf(line,"Error while waiting cond variable.\n");
            write (fd, line, strlen(line));
            pthread_exit(NULL);
        }
        WR--;
    }
    AR++;
    error=pthread_mutex_unlock(&m);
    if(error!=0){
        sprintf(line, "Failed while unlocking mutex.\n");
        write(fd, line, strlen(line));
		pthread_exit(NULL);
    }

    return_value=searchCache(cache,s,d);
    
    error=pthread_mutex_lock(&m);
    if(error!=0){
        sprintf(line, "Failed while locking mutex.\n");
        write(fd, line, strlen(line));
		pthread_exit(NULL);
    }
    AR--;

    error=pthread_cond_broadcast(&okToReach);
    if(error!=0){
        sprintf(line,"Error while signalling cond. variable.\n");
        write(fd, line, strlen(line));
        pthread_exit(NULL);
    }

    error=pthread_mutex_unlock(&m);
    if(error!=0){
        sprintf(line, "Failed while unlocking mutex.\n");
        write (fd, line, strlen(line));
		pthread_exit(NULL);
    }
    return return_value;
}

/*Read Cache Equal Prior.*/
struct LinkedList* writeCacheEP(int fd,struct AVLCache **cache,struct LinkedList *ll,int d){
    int error=0;
    char line[64];

    error=pthread_mutex_lock(&m);
    if(error!=0){
        sprintf(line, "Failed while locking mutex.\n");
        write(fd, line, strlen(line));
		pthread_exit(NULL);
    }
    
    while(AW+AR>0){
        WW++;
        error=pthread_cond_wait(&okToReach,&m);
        if(error!=0){
            sprintf(line,"Error while waiting cond variable.\n");
            write (fd, line, strlen(line));
            pthread_exit(NULL);
        }
        WW--;
    }
    AW++;

    error=pthread_mutex_unlock(&m);
    if(error!=0){
        sprintf(line, "Failed while unlocking mutex.\n");
        write (fd, line, strlen(line));
		pthread_exit(NULL);
    }


    addPathToCache(cache,ll,d);

    error=pthread_mutex_lock(&m);
    if(error!=0){
        sprintf(line, "Failed while locking mutex.\n");
        write (fd, line, strlen(line));
		pthread_exit(NULL);
    }

    AW--;

    error=pthread_cond_broadcast(&okToReach);
    if(error!=0){
        sprintf(line,"Error while signalling cond. variable.\n");
        write(fd,line,strlen(line));
        pthread_exit(NULL);
    }

    error=pthread_mutex_unlock(&m);
    if(error!=0){
        sprintf(line, "Failed while unlocking mutex.\n");
        write (fd, line, strlen(line));
		pthread_exit(NULL);
    }
}
