#ifndef AVL_GRAPH_H
#define AVL_GRAPH_H



struct LinkedList{
    struct List_Node *head;
};

struct List_Node{
    int value;
    struct List_Node *next;
};


struct AVLNode{
    int value;
    struct LinkedList *dest;/*This variable stores the reachable nodes.*/
    struct AVLNode *left;
    struct AVLNode *right;
    int height;
};

struct AVLVisited{
    int value;
    int parent; /*This variable stores the reachable nodes.*/
    struct AVLVisited *left;
    struct AVLVisited *right;
    int height;
};

/*Queue.*/
struct Queue{
    struct List_Node *front;
    struct List_Node *rear;
};




/*-----------------FOR GRAPH AVLTREE-----------------*/

int getMax(int x,int y);

/*Returns height of AVL Tree.*/
int height(struct AVLNode *n);

/*Creates new node and returns it.*/
struct AVLNode* createNode(int value,struct LinkedList *dest);

/*Right Rotate*/
struct AVLNode* rRotate(struct AVLNode* node);

/*Left rotate.*/
struct AVLNode* lRotate(struct AVLNode* node);

/*returns balance value of node.*/
int balance(struct AVLNode* node);

/*inserts given node and value to tree.*/
struct AVLNode* add(struct AVLNode* node,int value,struct LinkedList *dest);

struct AVLNode* binarySearch(struct AVLNode* node,int value);

void clearTree(struct AVLNode* node);

/*----------------------------------------------------------------------------*/




/*--------------------AVL TREE FOR BREATH FIRST SEARCH--------------------*/

/*Returns height of AVL Tree.*/
int heightVisited(struct AVLVisited *n);

/*Creates new node and returns it.*/
struct AVLVisited* createVisitedNode(int value,int parent);

/*Right Rotate*/
struct AVLVisited* rRotateVisited(struct AVLVisited* node);

/*Left rotate.*/
struct AVLVisited* lRotateVisited(struct AVLVisited* node);

/*returns balance value of node.*/
int balanceVisited(struct AVLVisited* node);

/*inserts given node and value to tree.*/
struct AVLVisited* addVisited(struct AVLVisited* node,int value,int parent);

struct AVLVisited* binarySearchVisited(struct AVLVisited* node,int value);

void clearTreeVisited(struct AVLVisited* node);

/*--------------------------------------------------------------*/





/*--------------------FOR QUEUE--------------------*/

/*Create queue.*/
struct Queue* createQueue();

/*Adds element to rear.*/
void enQueue(struct Queue *q,int value);

/*Removes element in the front.*/
int deQueue(struct Queue *q);

/*Clears queue*/
void clearQueue(struct Queue *q);
/*------------------------------------------------*/





/*-----------------FOR LINKEDLIST-----------------*/

struct LinkedList *createLinkedList();

/*This function adds new node to linked list.*/
void addLinkedList(struct LinkedList *ll,int node);

/*This function adds new node to end of linked list.*/
void addLinkedListLast(struct LinkedList *ll,int node);

/*This function searches the LinkedList.
    returns 0 is successfull -1 if not.*/
int searchLinkedList(struct LinkedList *ll,int node);

/*Free all allocated memory.*/
void clearLinkedList(struct LinkedList *linked);

/*------------------------------------------------*/


/*Finds path between s(source) and d(destination)
    and returns as a linkedlist. Returns NULL in fail.*/
struct LinkedList* BFS(struct AVLNode* g,int s,int d);

#endif
