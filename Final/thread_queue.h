/*
    This file contains different queue data structure that spesific
        for thread structrures.
*/

#ifndef THREAD_QUEUE_H
#define THREAD_QUEUE_H


/*Stores thread arguments for each thread..*/
/*Stores thread arguments for each thread..*/
struct thrArg{

    pthread_t *th;
    int fd; /*Socket fd for communicating client.*/
    pthread_cond_t *thr_cond; /*Threads waits with this contion until main delivers
                                new fd and wakes up thread.*/
    int no; /*Thread no.*/
};

/*Queue.*/
struct Thread_Queue{
    struct Thread_List_Node *front;
    struct Thread_List_Node *rear;
};

struct Thread_List_Node{
    struct thrArg *thrArgValue; /*Thread arguments.*/
    struct Thread_List_Node *next;
};



/*Create queue.*/
struct Thread_Queue* createThreadQueue();

/*Adds element to rear.*/
void Thread_enQueue(struct Thread_Queue *q,struct thrArg *value);

/*Removes element in the front.*/
struct thrArg* Thread_deQueue(struct Thread_Queue *q);

/*Clears queue*/
int ThreadClearQueue(struct Thread_Queue *q);

struct thrArg* createThreadArg(int id);

#endif